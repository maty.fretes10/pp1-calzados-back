
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
            productos: [],
            carrito:{
                "store_id":1,
                "products":[]
            },
            productosElegidos:[],
            verCarrito:false,
		},
		mounted: function(){
			fetch("/api/v1/products",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				
				this.productos = data;
			});

		},
		methods:{
			refrescarDatos(){
				fetch("/api/v1/products",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {

					this.productos = data;
				});
			},
			agregarItem( product, required_stock){
			    this.carrito.products.push(
                    {
                        "product_id": product.id,
                        "required_stock": required_stock,
                    }
			    );
			    product.required_stock=required_stock;
			    this.productosElegidos.push(product);
			},
			verCarrito(){
                this.verCarrito=true;
			}
		},
		computed: {

		},
	});