
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
			venta: {
                "cart_id": 0,
                "store_id": 1,
                "employee_id": 1,
                "total_amount": 0,
                "client_id": 1,
                "sale_type":"STORE",
                "transactions": [
                ],
                "discount": 0,
                "total_discount_amount": 0
            },
            tarjeta:{
              "transaction_type": "CREDIT_CARD",
              "amount": 0,
              "credit_card": {
                  "card_type": "",
                  "card_number": ""
              }
            },
            efectivo:{
              "transaction_type": "CASH",
              "amount": 0,
              "description": "Efectivo"
            },
            cuenta:{
                "transaction_type": "ACCOUNT",
                "amount": 0
            },
            saldo:{
            },
            clientes:[],
            mostrarDialogoDeVentaTarjeta:false,
            mostrarDialogoDeVentaEfectivo:false,
            mostrarDialogoDeVentaCuentaCorriente:false,
            modalShow: true,
            productos: [],
            carrito:{
                "store_id":1,
                "products":[ ],
            },
            productosElegidos:[],
            resumenProductosElegidos:[],
            verCarrito:false,
            errors: [],
            errores_tarjeta: [],
            errores_efectivo: [],
            errores_cuenta_corriente: [],
            total_pagado:0,
            cart_id: 1,
            balanceActual : {
                balance: 0
            },
		},
		mounted: function(){
          fetch("/api/v1/client/balance/1",
          {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "GET"})
          .then(response => response.json())
          .then(data => {this.balanceActual = data});

		fetch("/api/v1/products",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				
				this.productos = data;
			});

			fetch("/api/v1/clients",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            			method: "GET"})
            			.then(response => response.json())
            			.then(data => {this.clientes = data});
		},
		methods:{
		    validarVentas: function (e){

                this.errors = [];
                this.errores_cuenta_corriente = [];
                this.errores_efectivo = [];
                this.errores_tarjeta = [];
                this.total_pagado = 0;

                var campoPagoTarjeta = $( "#pagoTarjetaInput" );
                var mensajePagoTarjeta = $( "#pagoTarjetaDiv" );

                var campoNombreTarjeta = $( "#nombreTarjetaInput" );
                var mensajeNombreTarjeta = $( "#nombreTarjetaDiv" );

                var campoNumeroTarjeta = $( "#numeroTarjetaInput" );
                var mensajeNumeroTarjeta = $( "#numeroTarjetaDiv" );

                var campoEfectivo = $( "#efectivoInput" );
                var mensajeEfectivo = $( "#efectivoDiv" );

                var campoSaldoUtilizable = $( "#saldoUtilizableInput" );
                var mensajeSaldoUtilizable = $( "#saldoUtilizableDiv" );

                var allInputs = $( ":input" );
                var textInputs = $( ":input:text" );
                var botonDeEnviar = $( ".submitButton" );

                //Limpiar mensajes y validaciones
                allInputs.removeClass('is-valid');
                allInputs.removeClass('is-invalid');
                $( ".invalid-feedback" ).text('');

                //Validacion de todos los campos
                /*
                if(!this.insumo.name && !this.insumo.description && !this.insumo.stock && !this.insumo.stock_min && !this.insumo.price_buy){
                    this.errors.push("Todos los campos son obligatorios.");

                    var divs = $( ".invalid-feedback" ).text('Este campo es obligatorio');

                    textInputs.addClass('is-invalid');
                    campoStockInsumo.addClass('is-invalid');
                    campoStockMinInsumo.addClass('is-invalid');
                    campoPrecioCompraInsumo.addClass('is-invalid');

                    e.preventDefault();
                    return false;
                }
                */

                if(this.mostrarDialogoDeVentaTarjeta){

                    //Validaciones de dinero a pagar con la tarjeta
                    if (!this.tarjeta.amount) {
                      this.errors.push("El pago con tarjeta es obligatorio.");
                      this.errores_tarjeta.push("El pago con tarjeta es obligatorio.");

                      campoPagoTarjeta.removeClass('is-valid');
                      campoPagoTarjeta.addClass('is-invalid');
                      mensajePagoTarjeta.text('El pago con tarjeta es obligatorio');

                    }
                    else if (!this.validNumeros(this.tarjeta.amount)) {
                      this.errors.push('El pago con tarjeta debe tener solo numeros positivos.');
                      this.errores_tarjeta.push("El pago con tarjeta debe tener solo numeros positivos.");

                      campoPagoTarjeta.removeClass('is-valid');
                      campoPagoTarjeta.addClass('is-invalid');
                      mensajePagoTarjeta.text('El pago con tarjeta debe tener solo numeros positivos.');
                    }

                    //Validaciones de nombre de tarjeta
                    if (!this.tarjeta.credit_card.card_type) {
                      this.errors.push("El nombre de la tarjeta es obligatoria.");
                      this.errores_tarjeta.push("El nombre de la tarjeta es obligatoria.");

                      campoNombreTarjeta.removeClass('is-valid');
                      campoNombreTarjeta.addClass('is-invalid');
                      mensajeNombreTarjeta.text('El nombre de la tarjeta es obligatoria.');
                    }
                    else if (!this.validLetras(this.tarjeta.credit_card.card_type)) {
                      this.errors.push('El nombre de la tarjeta debe tener solo letras.');
                      this.errores_tarjeta.push("El nombre de la tarjeta debe tener solo letras.");

                      campoNombreTarjeta.removeClass('is-valid');
                      campoNombreTarjeta.addClass('is-invalid');
                      mensajeNombreTarjeta.text('El nombre de la tarjeta debe tener solo letras.');
                    }

                    //Validaciones de numero de tarjeta
                    if (!this.tarjeta.credit_card.card_number) {
                      this.errors.push("El numero de tarjeta es obligatorio.");
                       this.errores_tarjeta.push("El numero de tarjeta es obligatorio.");

                      campoNumeroTarjeta.removeClass('is-valid');
                      campoNumeroTarjeta.addClass('is-invalid');
                      mensajeNumeroTarjeta.text('El numero de tarjeta es obligatorio.');
                    }
                    else if (!this.validNumeros(this.tarjeta.credit_card.card_number)) {
                        this.errors.push('El numero de tarjeta debe tener solo numeros positivos.');
                        this.errores_tarjeta.push("El numero de tarjeta debe tener solo numeros positivos.");

                        campoNumeroTarjeta.removeClass('is-valid');
                        campoNumeroTarjeta.addClass('is-invalid');
                        mensajeNumeroTarjeta.text('El numero de tarjeta debe tener solo numeros positivos.');
                    }
                    else if (this.tarjeta.credit_card.card_number.length!=4) {
                      this.errors.push('El numero de tarjeta debe ser los ultimos 4 numeros de la tarjeta.');
                      this.errores_tarjeta.push("El numero de tarjeta debe ser los ultimos 4 numeros de la tarjeta.");

                      campoNumeroTarjeta.removeClass('is-valid');
                      campoNumeroTarjeta.addClass('is-invalid');
                      mensajeNumeroTarjeta.text('El numero de tarjeta debe ser los ultimos 4 numeros de la tarjeta.');
                    }
                    else{
                        this.total_pagado = parseInt(this.tarjeta.amount) + parseInt(this.total_pagado);
                    }
                }

                if(this.mostrarDialogoDeVentaEfectivo){
                    //Validaciones de efectivo
                    if (!this.efectivo.amount) {
                      this.errors.push("El efectivo es obligatorio.");
                      this.errores_efectivo.push("El efectivo es obligatorio.");

                      campoEfectivo.removeClass('is-valid');
                      campoEfectivo.addClass('is-invalid');
                      mensajeEfectivo.text('El efectivo es obligatorio.');
                    }
                    else if (!this.validNumeros(this.efectivo.amount)) {
                      this.errors.push('El efectivo debe tener solo numeros positivos.');
                      this.errores_efectivo.push("El efectivo debe tener solo numeros positivos.");

                      campoEfectivo.removeClass('is-valid');
                      campoEfectivo.addClass('is-invalid');
                      mensajeEfectivo.text('El efectivo debe tener solo numeros positivos.');
                    }
                    else{
                        this.total_pagado = parseInt(this.efectivo.amount) + parseInt(this.total_pagado);
                    }
                }

                if(this.mostrarDialogoDeVentaCuentaCorriente){
                    //Validaciones de saldo a utilizar
                    if (!this.cuenta.amount) {
                      this.errors.push("El saldo a utilizar es obligatorio.");
                      this.errores_cuenta_corriente.push("El saldo a utilizar es obligatorio.");

                      campoSaldoUtilizable.removeClass('is-valid');
                      campoSaldoUtilizable.addClass('is-invalid');
                      mensajeSaldoUtilizable.text('El saldo a utilizar es obligatorio.');
                    }
                    else if (!this.validNumeros(this.cuenta.amount)) {
                      this.errors.push('El saldo a utilizar debe tener solo numeros positivos.');
                      this.errores_cuenta_corriente.push("El saldo a utilizar debe tener solo numeros positivos.");

                      campoSaldoUtilizable.removeClass('is-valid');
                      campoSaldoUtilizable.addClass('is-invalid');
                      mensajeSaldoUtilizable.text('El saldo a utilizar debe tener solo numeros positivos.');
                    }
                    else{
                        this.total_pagado = parseInt(this.cuenta.amount) + parseInt(this.total_pagado);
                    }
                }

                //Hay errores o no
                if (!this.errors.length) {
                    if(this.total_pagado == this.total){
                        allInputs.removeClass('is-invalid');
                        allInputs.addClass('is-valid');
                        //$('#carritoModal').modal('hide');
                        //this.carrito.products = [];
                        //this.carrito.products.splice(0, this.carrito.products.length+1);
                        //$('#carritoModal').data('modal').$element.removeData();
                        botonDeEnviar.css('pointer-events','none');
                        botonDeEnviar.css('cursor','not-allowed');
                        return true;
                    }
                    else{
                        if(this.mostrarDialogoDeVentaTarjeta){
                            this.errors.push('El pago total debe coincidir con tu dinero total a pagar.');
                            this.errores_tarjeta.push("El pago total debe coincidir con tu dinero a pagar.");

                            campoPagoTarjeta.removeClass('is-valid');
                            campoPagoTarjeta.addClass('is-invalid');
                            mensajePagoTarjeta.text('El pago total debe coincidir con tu dinero a pagar.');
                        }
                        if(this.mostrarDialogoDeVentaEfectivo){
                            this.errors.push("El pago total debe coincidir con tu dinero total a pagar.");
                            this.errores_efectivo.push("El pago total debe coincidir con tu dinero total a pagar.");

                            campoEfectivo.removeClass('is-valid');
                            campoEfectivo.addClass('is-invalid');
                            mensajeEfectivo.text('El pago total debe coincidir con tu dinero total a pagar.');
                        }
                        if(this.mostrarDialogoDeVentaCuentaCorriente){
                            this.errors.push("El pago total debe coincidir con tu dinero total a pagar.");
                            this.errores_cuenta_corriente.push("El pago total debe coincidir con tu dinero total a pagar.");

                            campoSaldoUtilizable.removeClass('is-valid');
                            campoSaldoUtilizable.addClass('is-invalid');
                            mensajeSaldoUtilizable.text('El pago total debe coincidir con tu dinero total a pagar.');
                        }
                    }
                }
                e.preventDefault();
                return false;
            },
            validarNumeroTarjeta: function (numero_tar) {
                var re2 = /^(?:4\d([\- ])?\d{6}\1\d{5}|(?:4\d{3}|5[1-5]\d{2}|6011)([\- ])?\d{4}\2\d{4}\2\d{4})$/;
                return re2.test(numero_tar);
            },
            validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
            agregarMetodoDePago(metodo){
              if(metodo=='tarjeta'){
                this.mostrarDialogoDeVentaTarjeta = true;
                this.venta.transactions.push(this.tarjeta);
              }

              if(metodo=='efectivo'){
                this.mostrarDialogoDeVentaEfectivo = true;
                this.venta.transactions.push(this.efectivo);
              }

              if(metodo=='cuenta_corriente'){
                this.mostrarDialogoDeVentaCuentaCorriente = true;
                this.venta.transactions.push(this.cuenta);
              }

            },
			refrescarDatos(){
				fetch("/api/v1/products",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {

					this.productos = data;
				});
			},
			agregarItem( product, required_stock){
			    this.carrito.products.push(
                    {
                        "product_id": product.id,
                        "required_stock": required_stock,
                    }
			    );
			    product.required_stock=required_stock;


                    let found = false;
                      // Add the item or increase qty
                            let itemInCart = this.productosElegidos.filter(item => item.id===product.id);
                            let isItemInCart = itemInCart.length > 0;
                      if (isItemInCart === false) {
                        this.productosElegidos.push(Vue.util.extend({}, product));
                      }
                      else {
                                itemInCart[0].required_stock += product.required_stock;
                        }
                            product.required_stock = 1;
      },
      sacarItem(index) {
        itemElegidos = this.productosElegidos[index];
        itemCarrito = this.carrito.products.filter(item => item.product_id==itemElegidos.id);
        this.productosElegidos.splice(index, 1);
        for( var i = 0; i < this.carrito.products.length; i++){ 
                                   
          if ( this.carrito.products[i] === itemCarrito[0]) { 
            this.carrito.products.splice(i, itemCarrito.length); 
              i--;
          }
        }
      },
      comprar(){

        this.validarVentas();
        fetch("/api/v1/cart",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.carrito)
                })
                .then(response => response.json())
                .then(data => 
                  {
                    this.cart_id=data.cart_id;
                    this.venta.cart_id = data.cart_id;
                    this.venta.total_amount = this.total;
                    this.crearCompra();
                });

      },
      crearCompra(){
        /* agrego nueva compra*/

        console.log(this.venta.cart_id);
        fetch("/api/v1/sale/store",
          {
          credentials: "same-origin",
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(this.venta)
          }).then(async response =>{
            const data = await response.json();
            console.log(data)
            if (response.ok) {
              console.log('ok!');
              alert('Se envió la factura al mail');
              setTimeout(function(){
                  location.reload();
              },500);
            }else{
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
            }
          }).catch(error => {
             console.error('There was an error!', error);
             alert('No se pudo enviar la factura...: ' + error);
          });
      },
      borrarCarrito(){
        this.carrito.products=[];
        this.productosElegidos = [];
      },
      seleccionaCliente(index){
          fetch("/api/v1/client/balance/" + this.clientes[index].id,
          {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "GET"})
          .then(response => response.json())
          .then(data => {this.balanceActual = data});
      },
		},
		computed: {
            total: function () {
              var total = 0.0;
              this.productosElegidos.forEach( el => {
            	total = total + parseFloat(el.price_sale* el.required_stock);
              })
              return parseFloat(total.toFixed(2));
            },
		},
	});