

	var app = new Vue({
		el: '#app',
		data: {
        ordenes_produccion:[],
        modelos: [],
        colores:[],
        talles: [],
        historial: [],
        orden:{
            "store_id":1,
            "waist_production_id": 1,
            "colour_production_id": 1,
            "model_production_id": 1,
            "quantity": 1,

            "production_order_id":{
                "waistProduction" : {
                    "id" : 0
                },
                "colourProduction":{
                    "id" : 0
                },
                "quantity":0,
            }
        },
        ordenSeleccionada: 0,
        fases :[
            'CORTADO',
            'APARADO',
            'PREPARACION',
            'MONTADO',
            'PEGADO',
            'TERMINADO'
        ],
        siguiente: '',
        anterior:'',
        aux_state : false,
		},
		mounted: function(){
			fetch("/api/v1/po/state/list",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            method: "GET"})
            .then(response => response.json())
            .then(data => {
                this.ordenes_produccion = data;
            });
            
            
            fetch("/api/v1/products/colors",
            {headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"})
            .then(response => response.json())
            .then(data => {
                
                this.colores = data;
            });

            fetch("/api/v1/products/waist",
            {headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"})
            .then(response => response.json())
            .then(data => {
                
                this.talles = data;
            });

			fetch("/api/v1/production/model/list",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.modelos = data});
		},
		methods:{
            verDetalleOrden(index){
                this.ordenSeleccionada = index;
                

                if(this.ordenes_produccion[index].description == 'INICIADO'){
                    this.siguiente = 'CORTADO';
                    this.anterior = '';
                }else if (this.ordenes_produccion[index].description == 'CORTADO'){
                    this.siguiente = 'APARADO';
                    this.anterior = 'INICIADO';
                }else if (this.ordenes_produccion[index].description == 'APARADO'){
                    this.siguiente = 'PREPARACION';
                    this.anterior = 'CORTADO';
                }else if (this.ordenes_produccion[index].description == 'PREPARACION'){
                    this.siguiente = 'MONTADO';
                    this.anterior = 'APARADO';
                }else if (this.ordenes_produccion[index].description == 'MONTADO'){
                    this.siguiente = 'PEGADO';
                    this.anterior = 'PREPARACION';
                }else if (this.ordenes_produccion[index].description == 'PEGADO'){
                    this.siguiente = 'TERMINADO';
                    this.anterior = 'MONTADO';
                }
                else if (this.ordenes_produccion[index].description == 'TERMINADO'){
                    this.siguiente = '';
                    this.anterior = 'PEGADO';
                }

                fetch("/api/v1/po/history/list/"+ this.ordenes_produccion[index].production_order_id.id,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {
                    this.historial = data;
                });
                
            },
            crearOrdenProduccion(){
                fetch("/api/v1/production/order",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    "store_id":1,
                    "waist_production_id": this.orden.waist_production_id,
                    "colour_production_id": this.orden.colour_production_id,
                    "model_production_id": this.orden.model_production_id,
                    "quantity": this.orden.quantity
                })
                }).then(async response =>{
                    /* refresco tabla*/
                    this.refrescarDatos();
                });

                this.orden = {
                    "store_id":1,
                    "waist_production_id": 1,
                    "colour_production_id": 1,
                    "model_production_id": 1,
                    "quantity": 1,

                    "production_order_id":{
                        "waistProduction" : {
                            "id" : 0
                        },
                        "colourProduction":{
                            "id" : 0
                        },
                        "quantity":0,
                    }
                };
            },
            refrescarDatos(){
                fetch("/api/v1/po/state/list",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {
                    this.ordenes_produccion = data;
                });
            },
            refrescarHistoria(){
                if(this.ordenes_produccion[this.ordenSeleccionada].description == 'INICIADO'){
                    this.siguiente = 'CORTADO';
                    this.anterior = '';
                }else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'CORTADO'){
                    this.siguiente = 'APARADO';
                    this.anterior = 'INICIADO';
                }else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'APARADO'){
                    this.siguiente = 'PREPARACION';
                    this.anterior = 'CORTADO';
                }else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'PREPARACION'){
                    this.siguiente = 'MONTADO';
                    this.anterior = 'APARADO';
                }else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'MONTADO'){
                    this.siguiente = 'PEGADO';
                    this.anterior = 'PREPARACION';
                }else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'PEGADO'){
                    this.siguiente = 'TERMINADO';
                    this.anterior = 'MONTADO';
                }
                else if (this.ordenes_produccion[this.ordenSeleccionada].description == 'TERMINADO'){
                    this.siguiente = '';
                    this.anterior = 'PEGADO';
                }

                fetch("/api/v1/po/history/list/"+ this.ordenes_produccion[this.ordenSeleccionada].production_order_id.id,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {
                    this.historial = data;
                });
            },
            BorrarOrden(id){
                    fetch("/api/v1/po/state/edit",
                    {
                    credentials: "same-origin",
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({
                        "production_order_id": id,
                        "description": "CANCELADO"
                    }
                    )
                    }).then(async response =>{
                        /* refresco tabla*/
                        this.refrescarDatos();
                    });
            },
            cambiarFase(fase){
                if(fase == 'siguiente'){
                    fetch("/api/v1/po/state/edit",
                    {
                    credentials: "same-origin",
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({
                        "production_order_id": this.ordenes_produccion[this.ordenSeleccionada].production_order_id.id,
                        "description": this.siguiente,
                    }
                    )
                    }).then(async response =>{
                        /* refresco tabla*/
                        this.refrescarDatos();
                        this.refrescarHistoria();
                    });
                }else if (fase == 'anterior'){
                fetch("/api/v1/po/state/edit",
                    {
                    credentials: "same-origin",
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({
                        "production_order_id": this.ordenes_produccion[this.ordenSeleccionada].production_order_id.id,
                        "description": this.anterior ,
                    }
                    )
                    }).then(async response =>{
                        /* refresco tabla*/
                        this.refrescarDatos();
                        this.refrescarHistoria();
                    });
                }
            },
            modificarOrden(){
                
                fetch("/api/v1/production/order/edit",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    "production_order_id": this.ordenes_produccion[this.ordenSeleccionada].production_order_id.id,
                    "waist_production_id": this.orden.production_order_id.waistProduction.id,
                    "colour_production_id": this.orden.production_order_id.colourProduction.id,
                    "quantity": parseInt(this.orden.production_order_id.quantity)
                })
                }).then(async response =>{
                    /* refresco tabla*/
                    this.refrescarDatos();
                    this.refrescarHistoria();
                });
            },
            ordenarEstado(){
                this.aux_state = !this.aux_state;
                if(this.aux_state){
                  this.ordenes_produccion = this.ordenes_produccion.sort((a, b) => a.description.localeCompare(b.description))
                }else{
                  this.ordenes_produccion = this.ordenes_produccion.sort((a, b) => b.description.localeCompare( a.description))
                }
            },
        },
    
		computed: {
            modificar(){
                if(this.ordenes_produccion.length == 0 ){
                    return true;
                }else{
                    return this.ordenes_produccion[this.ordenSeleccionada].description !="CANCELADO" && this.ordenes_produccion[this.ordenSeleccionada].description !="TERMINADO";

                }
                
            }
		},
	});