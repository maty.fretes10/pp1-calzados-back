
var itemsNavegacion = [
    {
        'name': 'MANEJO DE SUCURSAL',
        'submodule':[
            {
                'target': '#Ventas',
                'control': 'Ventas',
                'name': 'Ventas',
                'icon': 'fas fa-dollar-sign',
                'subitems':[
                    {
                    'section':'Carrito de compras',
                    'url': '/carrito-compras',
                    },
                    {
                    'section':'Historial de ventas',
                    'url': '/ventas-realizadas',
                    },
                    {
                    'section':'Ventas web',
                    'url': '/ventas-web',
                    }
                ],

            },
            {
                'target': '#Stock',
                'control': 'Stock',
                'name': 'Stock',
                'icon': 'fas fa-boxes',
                'subitems':[
                    {
                    'section':'Gestión de Productos',
                    'url': '/productos',
                    },
                    {
                        'section':'Gestión de Modelos',
                        'url': '/modelo-productos',
                    }
                ],

            },
            {
                'target': '#Sucursales',
                'control': 'Sucursales',
                'name': 'Sucursales',
                'icon': 'fas fa-store',
                'subitems':[
                    {
                    'section':'Gestión de Sucursales',
                    'url': '/sucursales',
                    },
                    {
                    'section':'Gestión de Caja',
                    'url': '/gestion-caja',
                    }
                ],

            }
        ]
    },
    {
        'name': 'MÓDULO DE GESTIÓN',
        'submodule':[
            {
                'target': '#Clientes',
                'control': 'Clientes',
                'name': 'Clientes',
                'icon': 'fas fa-male',
                'subitems':[
                    {
                    'section':'Gestión de clientes',
                    'url': '/clientes',
                    }
                ],

            },
            {
                'target': '#Proveedores',
                'control': 'Proveedores',
                'name': 'Proveedores',
                'icon': 'fas fa-truck',
                'subitems':[
                    {
                    'section':'Gestión de Proveedores',
                    'url': '/proveedores',
                    },
                    {
                    'section':'Compra de Insumos',
                    'url': '/compra-proveedores',
                    }
                ],

            },
        ]
    },
    {
            'name': 'MÓDULO DE PRODUCCIÓN',
            'submodule':[
                {
                    'target': '#Produccion',
                    'control': 'Produccion',
                    'name': 'Produccion',
                    'icon': 'fas fa-industry',
                    'subitems':[
                        {
                        'section':'Gestión de insumos',
                        'url': '/insumos',
                        },
                        {
                        'section':'Ordenes de producción',
                        'url': '/ordenes-produccion',
                        },
                        {
                            'section':'Modelos de Produccion',
                            'url': '/modelo-ordenes-produccion',
                        },
                    ],

                },
            ]
        },

        {
            'name': 'CONFIGURACION',
            'submodule':[
                {
                    'target': '#Usuarios',
                    'control': 'Usuarios',
                    'name': 'Usuarios',
                    'icon': 'fas fa-user-edit',
                    'subitems':[
                        {
                        'section':'Gestion de Usuarios',
                        'url': '/userForm',
                        },

                    ],

                },
            ]
        },
        
];
