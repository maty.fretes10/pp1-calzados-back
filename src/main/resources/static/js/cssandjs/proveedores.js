



	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
			proveedor: {
                    "name":"",
                    "email":"",
                    "telephone":"",
                    "cuit":"",
                    "street":"",
                    "street_number":0,
                    "locality_id":1
            },
            proveedorSeleccionado:null,
            historial_cuentaCorriente:[],
            balanceActual: 0.0,
            saldo: {
                "balance": 0
            },
			proveedores: [],
			provincias: null,
			errors: [],
			localidades: [],
			tipo_categoria: null,
			errors: [],
			tipo_doc: null,
			provinciaSeleccionada: 1,
      ventana : false,
      editar: false,
    },
		mounted: function(){

			fetch("/api/v1/suppliers",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				
				this.proveedores = data;
			});

			fetch("/api/v1/provinces",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.provincias = data});

			fetch("/api/v1/localities/" + this.provinciaSeleccionada,
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.localidades = data});
		},
		methods:{
		validarProveedores: function (e){

                this.errors = [];

                var campoNombrePro = $( "#nombreProInput" );
                var mensajeNombrePro = $( "#nombreProDiv" );

                var campoEmailPro = $( "#emailProInput" );
                var mensajeEmailPro = $( "#emailProDiv" );

                var campoTelefonoPro = $( "#telefonoProInput" );
                var mensajeTelefonoPro = $( "#telefonoProDiv" );

                var campoCuit = $( "#cuitInput" );
                var mensajeCuit = $( "#cuitDiv" );

                var campoCallePro = $( "#calleProInput" );
                var mensajeCallePro = $( "#calleProDiv" );

                var campoNumCallePro = $( "#numCalleProInput" );
                var mensajeNumCallePro = $( "#numCalleProDiv" );

                var allInputs = $( ":input.rellenable" );

                //Limpiar mensajes y validaciones
                allInputs.removeClass('is-valid');
                allInputs.removeClass('is-invalid');
                $( ".invalid-feedback" ).text('');

                //Validacion de todos los campos
                if(!this.proveedor.name && !this.proveedor.email && !this.proveedor.telephone && !this.proveedor.cuit &&
                        !this.proveedor.street && !this.proveedor.street_number){
                    this.errors.push("Todos los campos son obligatorios.");

                    var divs = $( ".invalid-feedback" ).text('Todos los campos son obligatorios');

                    allInputs.addClass('is-invalid');
                    e.preventDefault();
                    return false;
                }

                //Validaciones de nombre
                if (!this.proveedor.name) {
                  this.errors.push("El nombre es obligatorio.");

                  campoNombrePro.removeClass('is-valid');
                  campoNombrePro.addClass('is-invalid');
                  mensajeNombrePro.text('El nombre es obligatorio.');
                }
                else if (!this.validLetras(this.proveedor.name)) {
                  this.errors.push('El nombre debe tener solo letras.');

                  campoNombrePro.removeClass('is-valid');
                  campoNombrePro.addClass('is-invalid');
                  mensajeNombrePro.text('El nombre debe tener solo letras.');
                }

                //Validaciones de email
                if (!this.proveedor.email) {
                  this.errors.push("El email es obligatorio.");

                  campoEmailPro.removeClass('is-valid');
                  campoEmailPro.addClass('is-invalid');
                  mensajeEmailPro.text('El email es obligatorio.');
                }
                else if (!this.validEmail(this.proveedor.email)) {
                  this.errors.push('El correo electrónico debe ser válido.');

                  campoEmailPro.removeClass('is-valid');
                  campoEmailPro.addClass('is-invalid');
                  mensajeEmailPro.text('El correo electrónico debe tener un formato valido');
                }

                //Validaciones de telefono
                if (!this.proveedor.telephone) {
                  this.errors.push("El stock es obligatorio.");

                  campoTelefonoPro.removeClass('is-valid');
                  campoTelefonoPro.addClass('is-invalid');
                  mensajeTelefonoPro.text('El telefono es obligatorio.');
                }

                //Validaciones de cuit
                if (!this.proveedor.cuit) {
                    this.errors.push("El cuit es obligatorio.");

                    campoCuit.removeClass('is-valid');
                    campoCuit.addClass('is-invalid');
                    mensajeCuit.text('El cuit es obligatorio.');
                }
                else if (!this.validCuit(this.proveedor.cuit)) {
                    this.errors.push('El cuit debe tener un formato valido de cuit');

                    campoCuit.removeClass('is-valid');
                    campoCuit.addClass('is-invalid');
                    mensajeCuit.text('El cuit debe tener un formato valido de cuit');
                }

                //Validaciones de calle
                if (!this.proveedor.street) {
                      this.errors.push('La calle es obligatoria.');

                      campoCallePro.removeClass('is-valid');
                      campoCallePro.addClass('is-invalid');
                      mensajeCallePro.text('La calle es obligatoria');
                }

                //Validaciones de numero de calle
                if (!this.proveedor.street_number) {
                  this.errors.push("El numero de calle es obligatorio.");

                  campoNumCallePro.removeClass('is-valid');
                  campoNumCallePro.addClass('is-invalid');
                  mensajeNumCallePro.text('El numero de calle es obligatorio.');
                }
                else if (!this.validNumeros(this.proveedor.street_number)) {
                  this.errors.push('El numero de calle debe ser solo numeros positivos.');

                  campoNumCallePro.removeClass('is-valid');
                  campoNumCallePro.addClass('is-invalid');
                  mensajeNumCallePro.text('El numero de calle debe ser solo numeros positivos.');
                }

                //Hay errores o no
                if (!this.errors.length) {
                  allInputs.removeClass('is-invalid');
                  allInputs.addClass('is-valid');
                  return true;
                }

                e.preventDefault();
                return false;
		    },
		    validEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
            validCuit: function (cadena_cuit) {
                  var re4 = /\b(30|33|34)(\D)?[0-9]{8}(\D)?[0-9]/;
                  return re4.test(cadena_cuit);
            },
		    validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
	    refrescarDatos(){
			fetch("/api/v1/suppliers",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				
				this.proveedores = data;
			});
		},
		crearProveedor(){
            this.validarProveedores();
            this.proveedor.street_number = parseInt(this.proveedor.street_number);

            if(this.editar){
                // modificar proveedor
                fetch("/api/v1/supplier/edit?supplierId=" + this.proveedorSeleccionado,
                {
                     credentials: "same-origin",
                     headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'application/json'
                     },
                     method: "POST",
                     body: JSON.stringify(this.proveedor)
                }).then(async response =>{
                      console.log(response);
                      this.refrescarDatos();
                      this.editar = false;
                });
            }
            else{

                /* crear proveedor*/
                fetch("/api/v1/supplier",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.proveedor)
                }).then(async response =>{
                    console.log(response);
                    this.refrescarDatos();
                    });
            }
				
            this.proveedor = {
                      "name":"",
                      "email":"",
                      "telephone":"",
                      "cuit":"",
                      "street":"",
                      "street_number":0,
                      "locality_id": 1
            };
		},
	    borrarDatos(){
                      this.proveedor = {
                                          "name":"",
                                          "email":"",
                                          "telephone":"",
                                          "cuit":"",
                                          "street":"",
                                          "street_number":0,
                                          "locality_id":1
                      };
                      this.provinciaSeleccionada = 1;
                      this.actualizarLocalidaddes();
                      var allInputs = $( ":input" );
                      allInputs.removeClass('is-invalid');
                      allInputs.removeClass('is-valid');
        },
		actualizarLocalidaddes(){
			fetch("/api/v1/localities/" + this.provinciaSeleccionada,
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.localidades = data});
		},
		inactivarProveedor(proveedor_id){
        fetch("/api/v1/supplier/inactive?supplierId="+proveedor_id,
        {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "DELETE"}).then(async response =>{
            this.refrescarDatos();
            const data = await response.json();
            console.log(data)
            console.log(response.status);
            if (response.status=200) {
            console.log('ok!');
            alert('El proveedor ya está desactivado');
            }else{
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
            }
        }).catch(error => {
            alert('El proveedor se desactivó');
        });

    },
    mostrarBalance(proveedorId){
        this.proveedorSeleccionado = proveedorId;
        fetch("/api/v1/supplier/balance/" + proveedorId,
        {headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "GET"})
        .then(response => response.json())
        .then(data => {this.balanceActual = data});

        fetch("/api/v1/supplier/account/history/" + proveedorId,
        {headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "GET"})
        .then(response => response.json())
        .then(data => {this.historial_cuentaCorriente = data
        console.log(this.historial_cuentaCorriente);});


      },
    cargarSaldo(){
      this.saldo.balance = parseFloat(this.saldo.balance);
      fetch("/api/v1/supplier/account/deposit/"+ this.proveedorSeleccionado,
          {
          credentials: "same-origin",
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(this.saldo)
          }).then(async response =>{
              this.refrescarDatos();
              this.mostrarBalance(this.proveedorSeleccionado);
          });
          this.saldo.balance = 0;
        },
    editarProveedor(proveedorId,index){
      this.proveedorSeleccionado = proveedorId;
      this.proveedor ={
            "name":this.proveedores[index].name,
            "email":this.proveedores[index].email,
            "telephone":this.proveedores[index].telephone,
            "cuit":this.proveedores[index].cuit,
            "street":this.proveedores[index].street,
            "street_number":this.proveedores[index].street_number,
                        "locality_id" : this.proveedores[index].locality.id,
      },
      //this.locality.province.id = this.proveedores[index].locality.province.id;
      this.provinciaSeleccionada = this.proveedores[index].locality.province.id;
      this.actualizarLocalidaddes();
      this.editar = true;
    }
	},
		computed: {

		},
	});

