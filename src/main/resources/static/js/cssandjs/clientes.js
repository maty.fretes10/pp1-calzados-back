
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
      clienteSeleccionado: null,
			cliente: {
			    "name":"",
			    "surname":"",
			    "email":"",
			    "telephone":"",
			    "document_type_id":2,
			    "document_number":"",
			    "category_afip_id":2,
			    "street":"",
			    "street_number":"",
			    "locality_id":1,
			    "credit_limit":0.0
			},
			clientes: [],
			historial_cuentaCorriente:[],
			balanceActual: 0.0,
			saldo: {
			    "balance": 0 
			},
			provincias: null,
			localidades: [],
			tipo_categoria: null,
			errors: [],
			tipo_doc: null,
			provinciaSeleccionada: 1,
			ventana: false,
			aux_name: false,
			aux_state: false,
			aux_email: false,
			aux_telephone: false,
			aux_credit_limit: false,
			editar: false

		},
		mounted: function(){
			fetch("/api/v1/clients",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.clientes = data});
			fetch("/api/v1/categories/afip",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.tipo_categoria = data});

			fetch("/api/v1/identification/types",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.tipo_doc = data});

			fetch("/api/v1/provinces",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.provincias = data});


			fetch("/api/v1/localities/" + this.provinciaSeleccionada,
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.localidades = data});
		},
		methods:{
      mostrarBalance(clienteId){
        this.clienteSeleccionado = clienteId;
        fetch("/api/v1/client/balance/" + clienteId,
        {headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "GET"})
        .then(response => response.json())
        .then(data => {this.balanceActual = data});

        fetch("/api/v1/client/account/history/" + clienteId,
        {headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "GET"})
        .then(response => response.json())
        .then(data => {this.historial_cuentaCorriente = data
        console.log(this.historial_cuentaCorriente);});
        

      },
      cargarSaldo(){
        this.saldo.balance = parseFloat(this.saldo.balance);
        fetch("/api/v1/client/account/deposit/"+ this.clienteSeleccionado,
            {
            credentials: "same-origin",
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(this.saldo)
            }).then(async response =>{
                this.refrescarDatos();
                this.mostrarBalance(this.clienteSeleccionado);
            });
            this.saldo.balance = 0;
          },

		    checkForm: function (e) {
                  this.errors = [];
                  //var form = document.querySelectorAll('.needs-validation')
//                  document.getElementById('submitbutton').disabled = true;

                  var campoNombre = $( ".nombreInput" );
                  var mensajeNombre = $( "#nombreDiv" );

                  var campoApellido = $( ".apellidoInput" );
                  var mensajeApellido = $( "#apellidoDiv" );

                  var campoEmail = $( ".emailInput" );
                  var mensajeEmail = $( "#emailDiv" );

                  var campoTelefono = $( ".telefonoInput" );
                  var mensajeTelefono = $( "#telefonoDiv" );

                  var campoDocumento = $( ".documentoInput" );
                  var mensajeDocumento = $( "#documentoDiv" );

                  var campoCalle = $( ".calleInput" );
                  var mensajeCalle = $( "#calleDiv" );

                  var campoNroCalle = $( ".nroCalleInput" );
                  var mensajeNroCalle = $( "#nroCalleDiv" );

                  var campoLimiteCredito = $( ".limiteCreditoInput" );
                  var mensajeLimiteCredito = $( "#limiteCreditoDiv" );

                  var allInputs = $( ":input.rellenable" );

                  //Limpiar mensajes y validaciones
                  allInputs.removeClass('is-valid');
                  allInputs.removeClass('is-invalid');
                  $( ".invalid-feedback" ).text('');

                  //Validaciones obligatorios de todos los campos
                  if (!this.cliente.name && !this.cliente.email && !this.cliente.surname && !this.cliente.telephone
                        && !this.cliente.document_number && !this.cliente.street && !this.cliente.street_number && !this.cliente.credit_limit) {
                            this.errors.push("Todos los campos son obligatorios.");

                            var divs = $( ".invalid-feedback" ).text('Este campo es obligatorio');

                            allInputs.addClass('is-invalid');
                            e.preventDefault();
                            return false;
                  }

                  //Validaciones de Nombre
                  if (!this.cliente.name) {
                    this.errors.push("El nombre es obligatorio.");

                    campoNombre.removeClass('is-valid');
                    campoNombre.addClass('is-invalid');
                    mensajeNombre.text('El nombre es obligatorio');

                  }
                  else if (!this.validLetras(this.cliente.name)) {
                    this.errors.push('El nombre deben ser solo letras.');

                    campoNombre.removeClass('is-valid');
                    campoNombre.addClass('is-invalid');
                    mensajeNombre.text('El nombre debe tener solo letras');
                  }

                  //Validaciones de Apellido
                  if (!this.cliente.surname) {
                    this.errors.push('El apellido es obligatorio.');

                    campoApellido.removeClass('is-valid');
                    campoApellido.addClass('is-invalid');
                    mensajeApellido.text('El apellido es obligatorio');
                  }
                  else if (!this.validLetras(this.cliente.surname)) {
                    this.errors.push('El apellido deben ser solo letras.');

                    campoApellido.removeClass('is-valid');
                    campoApellido.addClass('is-invalid');
                    mensajeApellido.text('El apellido debe tener solo letras');
                  }

                  //Validaciones de Email
                  if (!this.cliente.email) {
                    this.errors.push('El correo electrónico es obligatorio.');

                    campoEmail.removeClass('is-valid');
                    campoEmail.addClass('is-invalid');
                    mensajeEmail.text('El correo electrónico es obligatorio');
                  }
                  else if (!this.validEmail(this.cliente.email)) {
                    this.errors.push('El correo electrónico debe ser válido.');

                    campoEmail.removeClass('is-valid');
                    campoEmail.addClass('is-invalid');
                    mensajeEmail.text('El correo electrónico debe tener un formato valido');
                  }

                  //Validaciones de Telefono
                    if (!this.cliente.telephone) {
                      this.errors.push('El telefono es obligatorio.');

                      campoTelefono.removeClass('is-valid');
                      campoTelefono.addClass('is-invalid');
                      mensajeTelefono.text('El telefono es obligatorio');
                    }
                    else if (!this.validTelefono(this.cliente.telephone)) {
                        this.errors.push('El telefono debe tener un formato valido.');

                        campoTelefono.removeClass('is-valid');
                        campoTelefono.addClass('is-invalid');
                        mensajeTelefono.text('El telefono debe tener un formato valido.');
                    }

                  //Validaciones de Nro de Documento

                  if (!this.cliente.document_number) {
                      this.errors.push('El numero de documento es obligatorio.');

                      campoDocumento.removeClass('is-valid');
                      campoDocumento.addClass('is-invalid');
                      mensajeDocumento.text('El numero de documento es obligatorio');
                  }
                  else if(this.cliente.document_type_id==1){
                      if(!this.validDni(this.cliente.document_number)){
                           this.errors.push('El numero de documento debe ser un dni valido.');

                           campoDocumento.removeClass('is-valid');
                           campoDocumento.addClass('is-invalid');
                           mensajeDocumento.text('El numero de documento debe ser un dni valido.');
                      }
                  }
                  else if(this.cliente.document_type_id==2){
                      if(!this.validCuit(this.cliente.document_number)){
                           this.errors.push('El numero de documento debe ser un cuit valido.');

                           campoDocumento.removeClass('is-valid');
                           campoDocumento.addClass('is-invalid');
                           mensajeDocumento.text('El numero de documento debe ser un cuit valido.');
                      }
                  }
                  else if(this.cliente.document_type_id==3){
                      if(!this.validCuil(this.cliente.document_number)){
                           this.errors.push('El numero de documento debe ser un cuil valido.');

                           campoDocumento.removeClass('is-valid');
                           campoDocumento.addClass('is-invalid');
                           mensajeDocumento.text('El numero de documento debe ser un cuil valido.');
                      }
                  }

                  //Validaciones de Calle
                    if (!this.cliente.street) {
                      this.errors.push('La calle es obligatoria.');

                      campoCalle.removeClass('is-valid');
                      campoCalle.addClass('is-invalid');
                      mensajeCalle.text('La calle es obligatoria');
                    }

                  //Validaciones de Nro de Calle
                  if (!this.cliente.street_number) {
                    this.errors.push('El numero de calle es obligatorio.');

                    campoNroCalle.removeClass('is-valid');
                    campoNroCalle.addClass('is-invalid');
                    mensajeNroCalle.text('El numero de calle es obligatorio');
                  }
                  else if (!this.validNumeros(this.cliente.street_number)) {
                    this.errors.push('El numero de calle debe ser solo numeros.');

                    campoNroCalle.removeClass('is-valid');
                    campoNroCalle.addClass('is-invalid');
                    mensajeNroCalle.text('El numero de calle debe ser solo numeros');
                  }

                  //Validaciones de Limite de credito
                  if (!this.cliente.credit_limit) {
                        this.errors.push('El limite de credito es obligatorio.');

                        campoLimiteCredito.removeClass('is-valid');
                        campoLimiteCredito.addClass('is-invalid');
                        mensajeLimiteCredito.text('El limite de credito es obligatorio');
                  }
                  else if (!this.validNumeros(this.cliente.credit_limit)) {
                        this.errors.push('El limite de credito debe ser solo numeros.');

                        campoLimiteCredito.removeClass('is-valid');
                        campoLimiteCredito.addClass('is-invalid');
                        mensajeLimiteCredito.text('El limite de credito debe ser solo numeros');
                  }

                  //Hay errores o no
                  if (!this.errors.length) {
                    allInputs.removeClass('is-invalid');
                    allInputs.addClass('is-valid');
                    return true;
                  }

                  e.preventDefault();
                  return false;

            },
            validDni: function (dni) {
                  var re5 = /^[\d]{1,3}\.?[\d]{3,3}\.?[\d]{3,3}$/;
                  return re5.test(dni);
            },
            validCuit: function (cuit) {
                  var re6 = /\b(30|33|34)(\D)?[0-9]{8}(\D)?[0-9]/;
                  return re6.test(cuit);
            },
            validCuil: function (cuil) {
                  var re6 = /\b(20|23|24|27)(\D)?[0-9]{8}(\D)?[0-9]/;
                  return re6.test(cuil);
            },
            validEmail: function (email) {
                  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                  return re.test(email);
            },
            validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
            validTelefono: function (tel) {
                var re4 = /(\+54|0054|54)?[ -]*(11|351|379|370|221|380|261|299|343|376|280|362|2966|387|383|264|266|381|388|342|2954|385|2920|2901)[ -]*([0-9][ -]*){8}/;
                return re4.test(tel);
            },
            refrescarDatos(){
                fetch("/api/v1/clients",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {

                    this.clientes = data;
                });
            },
            inactivarCliente(cliente_id){
                fetch("/api/v1/client/inactive?clientId="+cliente_id,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE"}).then(async response =>{
                    this.refrescarDatos();
                    const data = await response.json();
                    console.log(data)
                    console.log(response.status);
                    if (response.status=200) {
                    console.log('ok!');
                    alert('El cliente ya está desactivado');
                    }else{
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                    }
                }).catch(error => {
                    alert('El cliente se desactivó');
                });

            },
            crearCliente(){
                this.checkForm();
                this.cliente.credit_limit = parseFloat(this.cliente.credit_limit);
                if(this.editar){

                        // modificar proveedor
                        fetch("/api/v1/client/edit?clientId=" + this.clienteSeleccionado,
                        {
                        credentials: "same-origin",
                        headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify(this.cliente)
                        }).then(async response =>{
                          console.log(response);
                          this.refrescarDatos();
                          this.editar = false;
                          });

                      }
                      else{
                /* agrego nuevo cliente*/
                fetch("/api/v1/client",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.cliente)
                }).then(async response =>{
                    /* refresco tabla*/
                    this.refrescarDatos();
                    });
                }

                this.cliente = {
                        "name":"",
                        "surname":"",
                        "email":"",
                        "telephone":"",
                        "document_type_id":2,
                        "document_number":"",
                        "category_afip_id":2,
                        "street":"",
                        "street_number":"",
                        "locality_id":1,
                        "credit_limit":0.0
                    };
            },
            borrarDatos(){
              this.cliente = {
                    "name":"",
                    "surname":"",
                    "email":"",
                    "telephone":"",
                    "document_type_id":2,
                    "document_number":"",
                    "category_afip_id":2,
                    "street":"",
                    "street_number":"",
                    "locality_id":1,
                    "credit_limit":0.0
                };
              this.provinciaSeleccionada = 1;
              this.actualizarLocalidaddes();
              var allInputs = $( ":input" );
              allInputs.removeClass('is-invalid');
              allInputs.removeClass('is-valid');
            },
            actualizarLocalidaddes(){
                fetch("/api/v1/localities/" + this.provinciaSeleccionada,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {this.localidades = data});
            },
            ordenarEstado(){
                this.aux_state = !this.aux_state;
                if(this.aux_state){
                  this.clientes = this.clientes.sort((a, b) => a.state.localeCompare(b.state))
                }else{
                  this.clientes = this.clientes.sort((a, b) => b.state.localeCompare( a.state))
                }
            },
            ordenarEmail(){
                this.aux_email = !this.aux_email;
                if(this.aux_email){
                  this.clientes = this.clientes.sort((a, b) => a.email.localeCompare(b.email))
                }else{
                  this.clientes = this.clientes.sort((a, b) => b.email.localeCompare( a.email))
                }
            },
            ordenarTelefono(){
                this.aux_telephone = !this.aux_telephone;
                if(this.aux_telephone){
                  this.clientes = this.clientes.sort((a, b) => a.telephone.localeCompare(b.telephone))
                }else{
                  this.clientes = this.clientes.sort((a, b) => b.telephone.localeCompare( a.telephone))
                }
            },
            ordenarNombre(){
                this.aux_name = !this.aux_name;
                if(this.aux_name){
                  this.clientes = this.clientes.sort((a, b) => a.name.localeCompare(b.name))
                }else{
                  this.clientes = this.clientes.sort((a, b) => b.name.localeCompare( a.name))
                }
            },
            ordenarBalance(){
                this.aux_credit_limit = !this.aux_credit_limit;
                if(this.aux_credit_limit){
                  this.clientes = this.clientes.sort((a, b) => a.credit_limit.localeCompare(b.credit_limit))
                }else{
                  this.clientes = this.clientes.sort((a, b) => b.credit_limit.localeCompare( a.credit_limit))
                }
            },
            editarCliente(clienteId,index){
                  this.clienteSeleccionado = clienteId;
                  this.cliente ={
                              "name":this.clientes[index].name,
                    			    "surname":this.clientes[index].surname,
                    			    "email":this.clientes[index].email,
                    			    "telephone":this.clientes[index].telephone,
                    			    "document_type_id":this.clientes[index].document_type.id,
                    			    "document_number":this.clientes[index].document_number,
                    			    "category_afip_id":this.clientes[index].category_afip.id,
                    			    "street":this.clientes[index].street,
                    			    "street_number":this.clientes[index].street_number,
                    			    "locality_id":this.clientes[index].locality.id,
                    			    "credit_limit":this.clientes[index].credit_limit
                  },
                  this.provinciaSeleccionada = this.clientes[index].locality.province.id;
                  this.actualizarLocalidaddes();
                  this.editar = true;
                }
        },
		computed: {

		},
	});

	/*
	$('form#id').submit(function(e){
        $(this).children('input[type=submit]').attr('disabled', 'disabled');
        // this is just for demonstration
        e.preventDefault();
        return false;
    });
    */
