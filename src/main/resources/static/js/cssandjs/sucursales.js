
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
      sucursalSeleccionado: null,
			sucursal: {
                          "name":"",
                          "sale_point":"",
                          "email":"",
                          "telephone":"",
                          "street":"",
                          "street_number":"",
                          "locality_id":1
                      },
			sucursales: [],
			historial_cuentaCorriente:[],
			balanceActual: 0.0,
			saldo: {
			    "balance": 0 
			},
			provincias: null,
			localidades: [],
			ventana: false,
			tipo_categoria: null,
			errors: [],
			provinciaSeleccionada: 1,
			aux_name: false,
			aux_state: false,
			aux_email: false,
			aux_telephone: false,
			aux_credit_limit: false,

		},
		mounted: function(){
			fetch("/api/v1/stores",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.sucursales = data});

			fetch("/api/v1/provinces",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.provincias = data});


			fetch("/api/v1/localities/" + this.provinciaSeleccionada,
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.localidades = data});
		},
		methods:{

		    validarSucursal: function (e) {
                  this.errors = [];
                  //var form = document.querySelectorAll('.needs-validation')
//                  document.getElementById('submitbutton').disabled = true;

                  var campoNombreSucursal = $( ".nombreSucursalInput" );
                  var mensajeNombreSucursal = $( "#nombreSucursalDiv" );

                  var campoPuntoVenta = $( ".puntoVentaInput" );
                  var mensajePuntoVenta = $( "#puntoVentaDiv" );

                  var campoEmailSucursal = $( ".emailSucursalInput" );
                  var mensajeEmailSucursal = $( "#emailSucursalDiv" );

                  var campoTelefonoSucursal = $( ".telefonoSucursalInput" );
                  var mensajeTelefonoSucursal = $( "#telefonoSucursalDiv" );

                  var campoCalleSucursal = $( ".calleSucursalInput" );
                  var mensajeCalleSucursal = $( "#calleSucursalDiv" );

                  var campoNroCalleSucursal = $( ".nroCalleSucursalInput" );
                  var mensajeNroCalleSucursal = $( "#nroCalleSucursalDiv" );

                  var allInputs = $( ":input.rellenable" );
                  var textInputs = $( ":input:text" );

                  //Limpiar mensajes y validaciones
                  allInputs.removeClass('is-valid');
                  allInputs.removeClass('is-invalid');
                  $( ".invalid-feedback" ).text('');

                  //Validaciones obligatorios de todos los campos
                  if (!this.sucursal.name && !this.sucursal.email && !this.sucursal.telephone
                        && !this.sucursal.sale_point && !this.sucursal.street && !this.sucursal.street_number) {
                            this.errors.push("Todos los campos son obligatorios.");

                            var divs = $( ".invalid-feedback" ).text('Este campo es obligatorio');

                            allInputs.addClass('is-invalid');
                            e.preventDefault();
                            return false;
                  }

                  //Validaciones de Nombre
                  if (!this.sucursal.name) {
                    this.errors.push("El nombre es obligatorio.");

                    campoNombreSucursal.removeClass('is-valid');
                    campoNombreSucursal.addClass('is-invalid');
                    mensajeNombreSucursal.text('El nombre es obligatorio');
                  }
                  else if (!this.validLetras(this.sucursal.name)) {
                    this.errors.push('El nombre deben ser solo letras.');

                    campoNombreSucursal.removeClass('is-valid');
                    campoNombreSucursal.addClass('is-invalid');
                    mensajeNombreSucursal.text('El nombre debe tener solo letras');
                  }

                  //Validaciones de Punto de venta
                  if (!this.sucursal.sale_point) {
                    this.errors.push('El punto de venta es obligatorio.');

                    campoPuntoVenta.removeClass('is-valid');
                    campoPuntoVenta.addClass('is-invalid');
                    mensajePuntoVenta.text('El punto de venta es obligatorio');
                  }

                  //Validaciones de Email
                  if (!this.sucursal.email) {
                    this.errors.push('El correo electrónico es obligatorio.');

                    campoEmailSucursal.removeClass('is-valid');
                    campoEmailSucursal.addClass('is-invalid');
                    mensajeEmailSucursal.text('El correo electrónico es obligatorio');
                  }
                  else if (!this.validEmail(this.sucursal.email)) {
                    this.errors.push('El correo electrónico debe ser válido.');

                    campoEmailSucursal.removeClass('is-valid');
                    campoEmailSucursal.addClass('is-invalid');
                    mensajeEmailSucursal.text('El correo electrónico debe tener un formato valido');
                  }

                  //Validaciones de Telefono
                    if (!this.sucursal.telephone) {
                      this.errors.push('El telefono es obligatorio.');

                      campoTelefonoSucursal.removeClass('is-valid');
                      campoTelefonoSucursal.addClass('is-invalid');
                      mensajeTelefonoSucursal.text('El telefono es obligatorio');
                    }
                    else if (!this.validTelefono(this.sucursal.telephone)) {
                        this.errors.push('El telefono debe tener un formato valido.');

                        campoTelefonoSucursal.removeClass('is-valid');
                        campoTelefonoSucursal.addClass('is-invalid');
                        mensajeTelefonoSucursal.text('El telefono debe tener un formato valido.');
                    }

                  //Validaciones de Calle
                    if (!this.sucursal.street) {
                      this.errors.push('La calle es obligatoria.');

                      campoCalleSucursal.removeClass('is-valid');
                      campoCalleSucursal.addClass('is-invalid');
                      mensajeCalleSucursal.text('La calle es obligatoria');
                    }

                  //Validaciones de Nro de Calle
                  if (!this.sucursal.street_number) {
                    this.errors.push('El numero de calle es obligatorio.');

                    campoNroCalleSucursal.removeClass('is-valid');
                    campoNroCalleSucursal.addClass('is-invalid');
                    mensajeNroCalleSucursal.text('El numero de calle es obligatorio');
                  }
                  else if (!this.validNumeros(this.sucursal.street_number)) {
                    this.errors.push('El numero de calle debe ser solo numeros.');

                    campoNroCalleSucursal.removeClass('is-valid');
                    campoNroCalleSucursal.addClass('is-invalid');
                    mensajeNroCalleSucursal.text('El numero de calle debe ser solo numeros');
                  }

                  //Hay errores o no
                  if (!this.errors.length) {
                    allInputs.removeClass('is-invalid');
                    allInputs.addClass('is-valid');
                    return true;
                  }

                  e.preventDefault();
                  return false;

            },
            validEmail: function (email) {
                  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                  return re.test(email);
            },
            validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
            validTelefono: function (tel) {
                var re4 = /(\+54|0054|54)?[ -]*(11|351|379|370|221|380|261|299|343|376|280|362|2966|387|383|264|266|381|388|342|2954|385|2920|2901)[ -]*([0-9][ -]*){8}/;
                return re4.test(tel);
            },
            refrescarDatos(){
                fetch("/api/v1/stores",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {

                    this.sucursales = data;
                });
            },
            inactivarSucursal(sucursal_id){
                fetch("/api/v1/store/inactive?storeId="+sucursal_id,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE"}).then(async response =>{
                    this.refrescarDatos();
                    const data = await response.json();
                    console.log(data)
                    console.log(response.status);
                    if (response.status=200) {
                    console.log('ok!');
                    alert('La sucursal ya está desactivada');
                    }else{
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                    }
                }).catch(error => {
                    alert('La sucursal se desactivó');
                });

            },
            crearSucursal(){

                this.validarSucursal();

                /* agrego nueva sucursal*/
                fetch("/api/v1/store",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.sucursal)
                }).then(async response =>{
                    /* refresco tabla*/
                    this.refrescarDatos();
                    });


                    sucursal = {
                          "name":"",
                          "sale_point":"",
                          "email":"",
                          "telephone":"",
                          "street":"",
                          "street_number":"",
                          "locality_id":1
                    };
            },
            actualizarLocalidaddes(){
                fetch("/api/v1/localities/" + this.provinciaSeleccionada,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {this.localidades = data});
            },
            ordenarEstado(){
                this.aux_state = !this.aux_state;
                if(this.aux_state){
                  this.sucursales = this.sucursales.sort((a, b) => a.state.localeCompare(b.state))
                }else{
                  this.sucursales = this.sucursales.sort((a, b) => b.state.localeCompare( a.state))
                }
            },
            ordenarEmail(){
                this.aux_email = !this.aux_email;
                if(this.aux_email){
                  this.sucursales = this.sucursales.sort((a, b) => a.email.localeCompare(b.email))
                }else{
                  this.sucursales = this.sucursales.sort((a, b) => b.email.localeCompare( a.email))
                }
            },
            ordenarTelefono(){
                this.aux_telephone = !this.aux_telephone;
                if(this.aux_telephone){
                  this.sucursales = this.sucursales.sort((a, b) => a.telephone.localeCompare(b.telephone))
                }else{
                  this.sucursales = this.sucursales.sort((a, b) => b.telephone.localeCompare( a.telephone))
                }
            },
            ordenarNombre(){
                this.aux_name = !this.aux_name;
                if(this.aux_name){
                  this.sucursales = this.sucursales.sort((a, b) => a.name.localeCompare(b.name))
                }else{
                  this.sucursales = this.sucursales.sort((a, b) => b.name.localeCompare( a.name))
                }
            },
        },
		computed: {

		},
	});
