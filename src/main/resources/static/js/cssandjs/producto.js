
	var app = new Vue({
		el: '#app',
		data: {
      producto:{
      "description":"",
      "stock": 0,
      "year_fabrication":null,
      "price_sale": 0,
      "price_buy": 0,
      "waist_id":"",
      "colour_id":"",
      "product_status_id":1,
      "product_model_id":1,
      "stock_security": 0,
      "stock_minimum": 0
            },
            producto_seleccionado: {
               "description":"",
               "stock": 0,
               "year_fabrication":null,
               "price_sale": 0,
               "price_buy": 0,
               "waist_id":1,
               "colour_id":1,
               "product_status_id":1,
               "product_model_id":2,
               "stock_security": 0,
               "stock_minimum": 0
            },
            productos: [],
            colores: [],
            estados:[],
            talles: [],
			anios: [],
			errors: [],
			modelos: [],
			proveedores: [],
			producto_proveedor_fav:{
               "product": {
                   "id": 0,
                   "description": "",
                   "product_model": {
                       "id": 0,
                       "name": "",
                       "mark": ""
                   },
                   "stock_security": 0,
                   "stock_minimum": 0,
                   "price_history": null
               },
               "supplier": {
                   "id": 0,
                   "name": "",
               },
           },
           producto_proveedor_enviar:{
               "id_product": 0,
               "id_supplier": 0,
           },
		},
		mounted: function(){
            var minAnio = 2000;
            var maxAnio = 2030;
            for(i=minAnio;i < maxAnio;i++ ){
                this.anios.push(i);
            }

			fetch("/api/v1/products",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				
				this.productos = data;
			});

			fetch("/api/v1/suppliers",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            			method: "GET"})
            			.then(response => response.json())
            			.then(data => {
            				this.proveedores = data;
            			});

            fetch("/api/v1/products/model",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.modelos = data;
				});

                fetch("/api/v1/products/colors",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.colores = data;
				});

                fetch("/api/v1/products/waist",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.talles = data;
				});

                fetch("/api/v1/products/status",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.estados = data;
				});
		},
		methods:{
		    validarProductos: function (e){

		        this.errors = [];

                var campoDescripcion = $( ".descripcionInput" );
                var mensajeDescripcion = $( "#descripcionDiv" );

                var campoAnio = $( "#anioInput" );
                var mensajeAnio = $( "#anioDiv" );

                var campoStock = $( "#stockInput" );
                var mensajeStock = $( "#stockDiv" );

                var campoStockSeguridad = $( "#stockSeguridadInput" );
                var mensajeStockSeguridad = $( "#stockSeguridadDiv" );

                var campoStockMin = $( "#stockMinInput" );
                var mensajeStockMin = $( "#stockMinDiv" );

                var campoPrecioCompra = $( "#precioCompraInput" );
                var mensajePrecioCompra = $( "#precioCompraDiv" );

                var campoPrecioVenta = $( "#precioVentaInput" );
                var mensajePrecioVenta = $( "#precioVentaDiv" );

                var allInputs = $( ":input" );
                var textInputs = $( ":input:text" );

                //Limpiar mensajes y validaciones
                allInputs.removeClass('is-valid');
                allInputs.removeClass('is-invalid');
                $( ".invalid-feedback" ).text('');

                //Validacion de todos los campos
                if(!this.producto.description && !this.producto.year_fabrication && !this.producto.stock && !this.producto.stock_minimum &&
                        !this.producto.stock_security && !this.producto.price_sale && !this.producto.price_buy){
                    this.errors.push("Todos los campos son obligatorios.");

                    var divs = $( ".invalid-feedback" ).text('Este campo es obligatorio');

                    textInputs.addClass('is-invalid');
                    campoAnio.addClass('is-invalid');
                    campoStock.addClass('is-invalid');
                    campoStockMin.addClass('is-invalid');
                    campoStockSeguridad.addClass('is-invalid');
                    campoPrecioVenta.addClass('is-invalid');
                    campoPrecioCompra.addClass('is-invalid');

                    e.preventDefault();
                    return false;
                }

                //Validaciones de Descripcion
                if (!this.producto.description) {
                  this.errors.push("La descripcion es obligatoria.");

                  campoDescripcion.removeClass('is-valid');
                  campoDescripcion.addClass('is-invalid');
                  mensajeDescripcion.text('La descripcion es obligatoria.');
                }
                else if (!this.validLetras(this.producto.description)) {
                  this.errors.push('La descripcion debe tener solo letras.');

                  campoDescripcion.removeClass('is-valid');
                  campoDescripcion.addClass('is-invalid');
                  mensajeDescripcion.text('La descripcion debe tener solo letras.');
                }

                //Validaciones de anio de fabricacion
                if (!this.producto.year_fabrication) {
                  this.errors.push("El anio de fabricacion es obligatorio.");

                  campoAnio.removeClass('is-valid');
                  campoAnio.addClass('is-invalid');
                  mensajeAnio.text('El anio de fabricacion es obligatorio.');
                }

                //Validaciones de stock
                if (!this.producto.stock) {
                  this.errors.push("El stock es obligatorio.");

                  campoStock.removeClass('is-valid');
                  campoStock.addClass('is-invalid');
                  mensajeStock.text('El stock es obligatorio.');
                }
                else if (!this.validNumeros(this.producto.stock)) {
                  this.errors.push('El stock debe tener solo numeros positivos.');

                  campoStock.removeClass('is-valid');
                  campoStock.addClass('is-invalid');
                  mensajeStock.text('El stock debe tener solo numeros positivos.');
                }

                //Validaciones de stock minimo
                if (!this.producto.stock_minimum) {
                  this.errors.push("El stock minimo es obligatorio.");

                  campoStockMin.removeClass('is-valid');
                  campoStockMin.addClass('is-invalid');
                  mensajeStockMin.text('El stock minimo es obligatorio.');
                }
                else if (!this.validNumeros(this.producto.stock_minimum)) {
                  this.errors.push('El stock minimo debe tener solo numeros positivos.');

                  campoStockMin.removeClass('is-valid');
                  campoStockMin.addClass('is-invalid');
                  mensajeStockMin.text('El stock minimo debe tener solo numeros positivos.');
                }

                //Validaciones de stock de seguridad
                if (!this.producto.stock_security) {
                  this.errors.push("El stock de seguridad es obligatorio.");

                  campoStockSeguridad.removeClass('is-valid');
                  campoStockSeguridad.addClass('is-invalid');
                  mensajeStockSeguridad.text('El stock de seguridad es obligatorio.');
                }
                else if (!this.validNumeros(this.producto.stock_security)) {
                  this.errors.push('El stock de seguridad debe tener solo numeros positivos.');

                  campoStockSeguridad.removeClass('is-valid');
                  campoStockSeguridad.addClass('is-invalid');
                  mensajeStockSeguridad.text('El stock de seguridad debe tener solo numeros positivos.');
                }

                //Validaciones de precio de compra
                if (!this.producto.price_buy) {
                  this.errors.push("El precio de compra es obligatorio.");

                  campoPrecioCompra.removeClass('is-valid');
                  campoPrecioCompra.addClass('is-invalid');
                  mensajePrecioCompra.text('El precio de compra es obligatorio.');
                }
                else if (!this.validNumeros(this.producto.price_buy)) {
                  this.errors.push('El precio de compra debe tener solo numeros positivos.');

                  campoPrecioCompra.removeClass('is-valid');
                  campoPrecioCompra.addClass('is-invalid');
                  mensajePrecioCompra.text('El precio de compra debe tener solo numeros positivos.');
                }

                //Validaciones de precio de venta
                if (!this.producto.price_sale) {
                  this.errors.push("El precio de venta es obligatorio.");

                  campoPrecioVenta.removeClass('is-valid');
                  campoPrecioVenta.addClass('is-invalid');
                  mensajePrecioVenta.text('El precio de venta es obligatorio.');
                }
                else if (!this.validNumeros(this.producto.price_sale)) {
                  this.errors.push('El precio de venta debe tener solo numeros positivos.');

                  campoPrecioVenta.removeClass('is-valid');
                  campoPrecioVenta.addClass('is-invalid');
                  mensajePrecioVenta.text('El precio de venta debe tener solo numeros positivos.');
                }

                //Hay errores o no
                if (!this.errors.length) {
                  allInputs.removeClass('is-invalid');
                  allInputs.addClass('is-valid');
                  return true;
                }

                e.preventDefault();
                return false;
		    },
		    validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
			refrescarDatos(){
				fetch("/api/v1/products",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.productos = data;
				});
			},
      refrescarColores(){
        fetch("/api/v1/products/colors",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.colores = data;
				});
      },
      agregarColor(color){
        fetch("/api/v1/color/add",
        {
        credentials: "same-origin",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({'name': color}) 
        }).then(async response =>{
        this.refrescarColores();
        var colorElegido = this.colores.filter(item => item.name==color.toUpperCase());
        this.producto.colour_id = colorElegido[0].id;
        });

      },
      refrescarTalles(){
        fetch("/api/v1/products/waist",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.talles = data;
				});
      },
      agregarTalle(talle){
        fetch("/api/v1/waist/add",
        {
        credentials: "same-origin",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({'name': talle}) 
        }).then(async response =>{
        this.refrescarTalles();
        var talleElegido = this.talles.filter(item => item.name==talle);
        this.producto.waist_id = talleElegido[0].id;
        });
        
      },
			crearProducto(){
				/* agrego nuevo producto*/

				this.validarProductos();

				this.producto.stock_minimum = parseFloat(this.producto.stock_minimum);
				this.producto.stock = parseFloat(this.producto.stock);
				this.producto.stock_security = parseFloat(this.producto.stock_security);
				this.producto.price_sale = parseFloat(this.producto.price_sale );
				this.producto.price_buy = parseFloat(this.producto.price_buy );

        var talle = this.talles.filter(item => item.name==this.producto.waist_id);
        var color = this.colores.filter(item => item.name==this.producto.colour_id);
        if(color.length !== 0 ){
          this.producto.colour_id = color[0].id;
        }else{
          this.agregarColor(this.producto.colour_id);
        }

        if(talle.length == 0){
          this.agregarTalle(this.producto.waist_id);
        }else{
          this.producto.waist_id = talle[0].id;
        }

				fetch("/api/v1/product",
          {
          credentials: "same-origin",
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(this.producto) 
          }).then(async response =>{
              this.refrescarDatos();
          });

				/* refresco tabla*/
				

				this.producto = {
					"description":"",
					"stock": 0,
					"year_fabrication":null,
					"price_sale": 0,
					"price_buy": 0,
					"waist_id":"",
					"colour_id":"",
					"product_status_id":1,
					"product_model_id":2,
					"stock_security": 0,
					"stock_minimum": 0
				};
			},
			crearFavorito(){
                this.producto_proveedor_enviar.id_product = this.producto_seleccionado.id;
                fetch("/api/v1/favs/supplier/product",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.producto_proveedor_enviar)
                }).then(async response =>{
                    alert('el favorito se guardó exitosamente');
                    this.refrescarDatos();
                  }).catch(error => {
                          alert('error al reemplazar el favorito');
                    });
            },
            editarFavorito(){
                this.producto_proveedor_enviar.id_product = this.producto_seleccionado.id;
                fetch("/api/v1/favs/supplier/product/edit",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.producto_proveedor_enviar)
                }).then(async response =>{
                    alert('el favorito se guardó exitosamente');
                    this.refrescarDatos();
                  }).catch(error => {
                      alert('error al reemplazar el favorito');
                });
            },
            eliminarFavorito(){
                this.producto_proveedor_enviar.id_product = this.producto_seleccionado.id;
                this.producto_proveedor_enviar.id_supplier = this.producto_proveedor_fav.supplier.id;
                fetch("/api/v1/favs/supplier/product/inactivate?productId="+this.producto_proveedor_enviar.id_product+"&supplierId="+this.producto_proveedor_enviar.id_supplier,
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                    method: "DELETE"}).then(async response =>{
                    alert('el favorito se eliminó exitosamente');
                    this.refrescarDatos();
                  }).catch(error => {
                      alert('error al eliminar el favorito');
                });
            },
            mostrarProveedorFav(producto){
                fetch("/api/v1/favs/supplier/product/id?id=" + producto.id,
                            {headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            method: "GET"})
                            .then(response => response.json())
                            .then(data => {
                                let arreglo=data;
                                this.producto_proveedor_fav = arreglo[0];
                            }).catch(error => {
                                  alert('el producto no tiene favorito');
                                  this.producto_proveedor_fav = {
                                       "product": {
                                           "id": 0,
                                           "description": "",
                                           "product_model": {
                                               "id": 0,
                                               "name": "",
                                               "mark": ""
                                           },
                                           "stock_security": 0,
                                           "stock_minimum": 0,
                                           "price_history": null
                                       },
                                       "supplier": {
                                           "id": 0,
                                           "name": "",
                                       },
                                  };
                            });
            },
            seleccionarProducto(producto){
                this.producto_seleccionado = producto;
                this.mostrarProveedorFav(producto);
            },
		},
		computed: {

		},
	});