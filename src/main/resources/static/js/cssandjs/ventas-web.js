
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
			venta_recibida:{},
			venta_enviar:{
              "client_id": 0,
              "store_id": 1,
              "total_amount": 0,
              "transactions": [
                {
                  "transaction_type": "CASH",
                  "amount": 0,
                  "description": "Venta por Mercado Libre"
                }
              ],
              "products": [
                {
                  "product_id": 0,
                  "required_stock": 0
                },
                {
                  "product_id": 0,
                  "required_stock": 0
                }
              ]
            },
            products:[{
                "product_desk": '',
                "required_stock": 0
              },
            ],
            clientes:[],
            productos:[],
		},
		mounted: function(){
		    fetch("/api/v1/clients",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            			method: "GET"})
            			.then(response => response.json())
            			.then(data => {
            			this.clientes = data
            			});

            fetch("/api/v1/products",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            			method: "GET"})
            			.then(response => response.json())
            			.then(data => {
            				this.productos = data;
            			});

			document.getElementById('import').onclick = () => {
              const files = document.getElementById('selectFiles').files;
              if (files.length <= 0) {
                alert('No se cargó ningun archivo');
                return false;
              }

              const fr = new FileReader();

              fr.onload = e => {
                const result = JSON.parse(e.target.result);
                this.venta_recibida=result;
                this.completarVentaEnviar();
                const formatted = JSON.stringify(result, null, 2);
                document.getElementById('result').innerHTML = formatted;
              }
              fr.readAsText(files.item(0));
            };
		},
		methods:{
            completarVentaEnviar(){
            this.venta_enviar.client_id=this.venta_recibida.client_id;
            this.venta_enviar.total_amount=this.venta_recibida.total_amount;
            this.venta_enviar.transactions[0].amount=this.venta_recibida.total_amount;
            this.venta_enviar.products=this.venta_recibida.products;
            console.log(this.venta_recibida);
            console.log(this.venta_enviar);
            },
            confirmarVenta(){
                fetch("/api/v1/sale/web",
                    {
                    credentials: "same-origin",
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify(this.venta_enviar)
                    }).then(async response =>{
                        const data = await response.json();
                        console.log(data);
                        if (response.ok) {
                          console.log('ok!');
                          alert('Se confirmó la venta');
                          setTimeout(function(){
                              location.reload();
                          },500);
                        }else{
                          const error = (data && data.message) || response.status;
                          return Promise.reject(error);
                        }
                      }).catch(error => {
                         console.error('There was an error!', error);
                         alert('No se pudo confirmar la venta... ' + error);
                      });
            },
            borrarCarrito(){
                this.venta_recibida={};
                this.venta_enviar={};
                document.getElementById('formArchivo').reset();
            }
        },
		computed: {

		},
	});