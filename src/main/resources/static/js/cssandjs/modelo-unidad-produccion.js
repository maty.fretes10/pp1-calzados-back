

	var app = new Vue({
		el: '#app',
		data: {
		usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
        modelo:{
            "model_id":1,
            "materials":[
                /*{
                    "material_id":1,
                    "required_stock":1,
                    "phase":"CORTADO"
                },*/

            ]
        },
        modelos: [],
        materiales : [],
        modelos_productos : [],
        errors: [],
        fases :[
            'CORTADO',
            'APARADO',
            'PREPARACION',
            'MONTADO',
            'PEGADO',
            'OTRO',
        ],
        //materialesAux :
        
		},
		mounted: function(){
            // get lista de modelos de productos
			fetch("/api/v1/products/model",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.modelos_productos = data});

            // get lista de modelos
			fetch("/api/v1/production/model/list",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.modelos = data});


            // get lista de materiales
            fetch("/api/v1/materials",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {this.materiales = data});

		},
		methods:{
            BorrarMaterial(index){
                this.modelo.materials.splice(index,1);
            },
            agregarMaterial(){
                this.modelo.materials.push({
                    "material_id":1,
                    "required_stock":1,
                    "phase":""
                });
            },
            refrescarDatos(){
                fetch("/api/v1/production/model/list",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {this.modelos = data});
            },
            crearModelo(){
                /* agrego nuevo modelo*/
                fetch("/api/v1/production/model",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.modelo)
                }).then(async response =>{
                    /* refresco tabla*/
                    this.refrescarDatos();
                    });


                this.modelo = {
                    "model_id":1,
                    "materials":[
                        /*
                        {
                            "material_id":1,
                            "required_stock":1,
                            "phase":"CORTADO"
                        },*/
        
                    ]
                };

            },

        },
		computed: {

		},
	});

