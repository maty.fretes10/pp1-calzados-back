
let componenteNav = Vue.component("menu-navegacion", {
    template: 
    `
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/index">
            <div class="sidebar-brand-icon">
                <i class="fab fa-shopify"></i>
            </div>
                <div class="sidebar-brand-text mx-3">ZappApp</div>
            </a>


            <template v-for='module in itemsNavegacion' >
                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading" style="font-size: 15px;">
                    {{ module.name}}
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item" v-for='item in module.submodule'>
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" v-bind:data-target="item.target"
                        aria-expanded="true" v-bind:aria-controls="item.control">
                        <i v-bind:class="item.icon"></i>
                        <span>{{ item.name }}</span>
                    </a>
                    <div v-bind:id="item.name" class="collapse" v-bind:aria-labelledby="item.name" data-parent="#accordionSidebar" >
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a v-for='subitem in item.subitems' class="collapse-item" v-bind:href="subitem.url">{{ subitem.section }}</a>
                        </div>
                    </div>
                </li>


            </template>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
    `,
    data: function () {
        return {
            itemsNavegacion: itemsNavegacion,
        }
      },
  });