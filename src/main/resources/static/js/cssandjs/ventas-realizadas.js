
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
			ventas_realizadas: [],
		},
		mounted: function(){
			fetch("/api/v1/sale",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            method: "GET"})
            .then(response => response.json())
            .then(data => {this.ventas_realizadas = data;
            console.log(this.ventas_realizadas.toString())});
		},
		methods:{

        },
		computed: {
            total: function () {
              var total = 0.0;
              this.productosElegidos.forEach( el => {
            	total = total + parseFloat(el.price_sale* el.required_stock);
              })
              return parseFloat(total.toFixed(2));
            },
		},
	});