

	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
            modelo:{
                "name": "",
                "mark": ""
            },
			errors: [],
			modelos: [],
			
		},
		mounted: function(){

            fetch("/api/v1/products/model",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.modelos = data;
				});
		},
		methods:{
			refrescarDatos(){
				fetch("/api/v1/products/model",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.modelos = data;
				});
			},
			crearModelo(){
				/* agrego nuevo producto*/
                fetch("/api/v1/product/model",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.modelo) 
                }).then(async response =>{
					this.refrescarDatos();
				  });

				/* refresco tabla*/
				

			},


		},
		computed: {

		},
	});