
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
            insumo:{
               "name":"",
               "description":"",
               "stock": 0,
               "stock_min":0,
               "price_buy": 0
            },
            insumo_seleccionado:{
               "name":"",
               "description":"",
               "stock": 0,
               "stock_min":0,
               "price_buy": 0
            },
            insumo_proveedor_fav:{
                 "material": {
                     "id": 0,
                     "name": "",
                     "description": "",
                 },
                 "supplier": {
                    "id": 0,
                    "name": "",
                 },
             },
            insumo_proveedor_enviar:{
               "id_material": 0,
               "id_supplier": 0
            },
            proveedores: [],
            insumos: [],
            errors: [],
            estados:[]
		},
		mounted: function(){
			fetch("/api/v1/materials",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
				this.insumos = data;
			});

			fetch("/api/v1/suppliers",
            {headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"})
            .then(response => response.json())
            .then(data => {
                this.proveedores = data;
            });


		},
		methods:{
		    validarInsumos: function (e){

                this.errors = [];

                var campoNombreInsumo = $( ".nombreInsumoInput" );
                var mensajeNombreInsumo = $( "#nombreInsumoDiv" );

                var campoDescripcionInsumo = $( ".descripcionInsumoInput" );
                var mensajeDescripcionInsumo = $( "#descripcionInsumoDiv" );

                var campoStockInsumo = $( "#stockInsumoInput" );
                var mensajeStockInsumo = $( "#stockInsumoDiv" );

                var campoStockMinInsumo = $( "#stockMinInsumoInput" );
                var mensajeStockMinInsumo = $( "#stockMinInsumoDiv" );

                var campoPrecioCompraInsumo = $( "#precioCompraInsumoInput" );
                var mensajePrecioCompraInsumo = $( "#precioCompraInsumoDiv" );

                var allInputs = $( ":input" );
                var textInputs = $( ":input:text" );

                //Limpiar mensajes y validaciones
                allInputs.removeClass('is-valid');
                allInputs.removeClass('is-invalid');
                $( ".invalid-feedback" ).text('');

                //Validacion de todos los campos
                if(!this.insumo.name && !this.insumo.description && !this.insumo.stock && !this.insumo.stock_min && !this.insumo.price_buy){
                    this.errors.push("Todos los campos son obligatorios.");

                    var divs = $( ".invalid-feedback" ).text('Este campo es obligatorio');

                    textInputs.addClass('is-invalid');
                    campoStockInsumo.addClass('is-invalid');
                    campoStockMinInsumo.addClass('is-invalid');
                    campoPrecioCompraInsumo.addClass('is-invalid');

                    e.preventDefault();
                    return false;
                }

                //Validaciones de Nombre
                if (!this.insumo.name) {
                  this.errors.push("El nombre es obligatorio.");

                  campoNombreInsumo.removeClass('is-valid');
                  campoNombreInsumo.addClass('is-invalid');
                  mensajeNombreInsumo.text('El nombre es obligatorio');

                }
                else if (!this.validLetras(this.insumo.name)) {
                  this.errors.push('El nombre deben ser solo letras.');

                  campoNombreInsumo.removeClass('is-valid');
                  campoNombreInsumo.addClass('is-invalid');
                  mensajeNombreInsumo.text('El nombre debe tener solo letras');
                }

                //Validaciones de Descripcion
                if (!this.insumo.description) {
                  this.errors.push("La descripcion es obligatoria.");

                  campoDescripcionInsumo.removeClass('is-valid');
                  campoDescripcionInsumo.addClass('is-invalid');
                  mensajeDescripcionInsumo.text('La descripcion es obligatoria.');
                }
                else if (!this.validLetras(this.insumo.description)) {
                  this.errors.push('La descripcion debe tener solo letras.');

                  campoDescripcionInsumo.removeClass('is-valid');
                  campoDescripcionInsumo.addClass('is-invalid');
                  mensajeDescripcionInsumo.text('La descripcion debe tener solo letras.');
                }

                //Validaciones de stock
                if (!this.insumo.stock) {
                  this.errors.push("El stock es obligatorio.");

                  campoStockInsumo.removeClass('is-valid');
                  campoStockInsumo.addClass('is-invalid');
                  mensajeStockInsumo.text('El stock es obligatorio.');
                }
                else if (!this.validNumeros(this.insumo.stock)) {
                  this.errors.push('El stock debe tener solo numeros positivos.');

                  campoStockInsumo.removeClass('is-valid');
                  campoStockInsumo.addClass('is-invalid');
                  mensajeStockInsumo.text('El stock debe tener solo numeros positivos.');
                }

                //Validaciones de stock minimo
                if (!this.insumo.stock_min) {
                  this.errors.push("El stock minimo es obligatorio.");

                  campoStockMinInsumo.removeClass('is-valid');
                  campoStockMinInsumo.addClass('is-invalid');
                  mensajeStockMinInsumo.text('El stock minimo es obligatorio.');
                }
                else if (!this.validNumeros(this.insumo.stock_min)) {
                  this.errors.push('El stock minimo debe tener solo numeros positivos.');

                  campoStockMinInsumo.removeClass('is-valid');
                  campoStockMinInsumo.addClass('is-invalid');
                  mensajeStockMinInsumo.text('El stock minimo debe tener solo numeros positivos.');
                }

                //Validaciones de precio de compra
                if (!this.insumo.price_buy) {
                  this.errors.push("El precio de compra es obligatorio.");

                  campoPrecioCompraInsumo.removeClass('is-valid');
                  campoPrecioCompraInsumo.addClass('is-invalid');
                  mensajePrecioCompraInsumo.text('El precio de compra es obligatorio.');
                }
                else if (!this.validNumeros(this.insumo.price_buy)) {
                  this.errors.push('El precio de compra debe tener solo numeros positivos.');

                  campoPrecioCompraInsumo.removeClass('is-valid');
                  campoPrecioCompraInsumo.addClass('is-invalid');
                  mensajePrecioCompraInsumo.text('El precio de compra debe tener solo numeros positivos.');
                }

                //Hay errores o no
                if (!this.errors.length) {
                  allInputs.removeClass('is-invalid');
                  allInputs.addClass('is-valid');
                  return true;
                }

                e.preventDefault();
                return false;
            },
            validLetras: function (cadena) {
                  var re2 = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
                  return re2.test(cadena);
            },
            validNumeros: function (numbers) {
                  var re3 = /^[0-9]+$/;
                  return re3.test(numbers);
            },
			refrescarDatos(){
				fetch("/api/v1/materials",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
					
					this.insumos = data;
				});
			},
			crearInsumo(){
				/* agrego nuevo producto*/

                this.validarInsumos();

				this.insumo.stock_min = parseFloat(this.insumo.stock_min);
				this.insumo.stock = parseFloat(this.insumo.stock);
				this.insumo.price_buy = parseFloat(this.insumo.price_buy );
				fetch("/api/v1/material",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.insumo)
                }).then(async response =>{
					this.refrescarDatos();
				  });

				/* refresco tabla*/

				this.insumo = {
                    "name":"",
                    "description":"",
                    "stock": 0,
                    "stock_min":0,
                    "price_buy": 0
                };
			},
			inactivarInsumo(insumo_id){
                            fetch("/api/v1/materials/inactive?materialId="+insumo_id,
                            {headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            method: "DELETE"}).then(async response =>{
                                this.refrescarDatos();
                                const data = await response.json();
                                console.log(data)
                                console.log(response.status);
                                if (response.status=200) {
                                console.log('ok!');
                                alert('El insumo ya está desactivado');
                                }else{
                                const error = (data && data.message) || response.status;
                                return Promise.reject(error);
                                }
                            }).catch(error => {
                                alert('El insumo se desactivó');
                            });

                        },

        crearFavorito(){
            this.insumo_proveedor_enviar.id_material = this.insumo_seleccionado.id;
            fetch("/api/v1/favs/supplier/material",
            {
            credentials: "same-origin",
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(this.insumo_proveedor_enviar)
            }).then(async response =>{
                alert('el favorito se guardó exitosamente');
                this.refrescarDatos();
              }).catch(error => {
                      alert('error al reemplazar el favorito');
                });
        },
        editarFavorito(){
            this.insumo_proveedor_enviar.id_material = this.insumo_seleccionado.id;
            fetch("/api/v1/favs/supplier/material/edit",
            {
            credentials: "same-origin",
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(this.insumo_proveedor_enviar)
            }).then(async response =>{
                alert('el favorito se guardó exitosamente');
                this.refrescarDatos();
              }).catch(error => {
                  alert('error al reemplazar el favorito');
            });
        },
        eliminarFavorito(){
            this.insumo_proveedor_enviar.id_material = this.insumo_seleccionado.id;
            this.insumo_proveedor_enviar.id_supplier = this.insumo_proveedor_fav.supplier.id;
            fetch("/api/v1/favs/supplier/material/inactivate?materialId="+this.insumo_proveedor_enviar.id_material+"&supplierId="+this.insumo_proveedor_enviar.id_supplier,
            {headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
                method: "DELETE"}).then(async response =>{
                alert('el favorito se eliminó exitosamente');
                this.refrescarDatos();
              }).catch(error => {
                  alert('error al eliminar el favorito');
            });
        },
        mostrarProveedorFav(insumo){
            fetch("/api/v1/favs/supplier/material/id?id=" + insumo.id,
                        {headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "GET"})
                        .then(response => response.json())
                        .then(data => {
                            let arreglo=data;
                            this.insumo_proveedor_fav = arreglo[0];
                        }).catch(error => {
                              alert('el insumo no tiene favorito');
                              this.insumo_proveedor_fav = {
                                   "material": {
                                       "id": 0,
                                       "name": "",
                                       "description": "",
                                   },
                                   "supplier": {
                                      "id": 0,
                                      "name": "",
                                   },
                              };
                        });
        },
        seleccionarInsumo(insumo){
            this.insumo_seleccionado = insumo;
            this.mostrarProveedorFav(insumo);
        },
		},
		computed: {

		},
	});