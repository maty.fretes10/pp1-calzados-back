
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
//            movimientos_a:[
//                {
//                    "id": 4,
//                    "description": "Cordones",
//                    "movement_type": "OUTPUT",
//                    "amount": 16000.0,
//                    "date": "2021-11-11T20:47:31.123"
//                },
//                {
//                    "id": 5,
//                    "description": "Air Max talle 40",
//                    "movement_type": "INPUT",
//                    "amount": 25000.0,
//                    "date": "2021-11-11T20:47:31.19"
//                },
//                {
//                    "id": 6,
//                    "description": "ADIDAS allStar",
//                    "movement_type": "INPUT",
//                    "amount": 15000.0,
//                    "date": "2021-11-11T20:47:31.194"
//                }
//            ],
//            movimientos_b:[
//                {
//                    "id": 7,
//                    "description": "Suelas",
//                    "movement_type": "OUTPUT",
//                    "amount": 32000.0,
//                    "date": "2021-11-11T20:47:31.123"
//                },
//                {
//                    "id": 8,
//                    "description": "Pegamento La gotita x100",
//                    "movement_type": "OUTPUT",
//                    "amount": 4000.0,
//                    "date": "2021-11-11T20:47:31.19"
//                },
//            ],
            movimientos:[],
            sucursal:{
                "name": '',
                "sale_point": '',
            },
            sucursales:[],
            caja:{
            "id": 0,
            "balance": 0,
            "state": "",
            "date": "",
                "cash_register": {
                    "id": 0,
                    "balance": 0,
                    "store": {
                        "id": 0,
                        "name": "",
                        "email": "",
                        "telephone": "",
                        "cuit": "",
                        "salePoint": "",
                        "street": "",
                        "streetNumber": "",
                        "locality": {
                            "id": 0,
                            "name": "",
                            "zipCode": "",
                            "province": {
                                "id": 0,
                                "name": ""
                            }
                        }
                    }
                }
            }
		},
		mounted: function(){
            fetch("/api/v1/stores",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            method: "GET"})
            .then(response => response.json())
            .then(data => {
            this.sucursales=data;
            });
		},
		methods:{
            seleccionarSucursal(){
              this.obtenerMovimientosCaja(this.sucursal.id);
              this.obtenerCaja(this.sucursal.id);
              console.log(this.sucursal);
              console.log(this.movimientos);
              console.log(this.caja);
            },
            obtenerMovimientosCaja(id){
                fetch("/api/v1/store/daily/items?sucursalId="+id,
                                {headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                            method: "GET"})
                            .then(response => response.json())
                            .then(data => {
                               console.log(data);
                               this.movimientos=data;
                            });
            },
		    cerrarCaja(sucursal_id){
                 fetch("/api/v1/store/daily/close?sucursalId="+sucursal_id,
                         {headers: {
                             'Accept': 'application/json',
                             'Content-Type': 'application/json'
                         },
                         method: "DELETE"}).then(async response =>{
                             this.refrescarDatos();
                             const data = await response.json();
                             console.log(data)
                             console.log(response.status);
                             if (response.status=200) {
                             console.log('ok!');
                             alert('No se pudo cerrar la caja');
                             }else{
                             const error = (data && data.message) || response.status;
                             return Promise.reject(error);
                             }
                         }).catch(error => {
                             alert('La caja se cerró correctamente');
                         });
            },
            obtenerCaja(id){
                fetch("/api/v1/store/dailies?sucursalId="+id,
                                {headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                            method: "GET"})
                            .then(response => response.json())
                            .then(data => {
                               console.log(data);
                               this.caja=data[0];
                            });
            },
            abrirCaja(sucursal_id){
                fetch("/api/v1/store/daily?sucursalId="+sucursal_id,
                     {headers: {
                         'Accept': 'application/json',
                         'Content-Type': 'application/json'
                     },
                     method: "POST"}).then(async response =>{
                         this.refrescarDatos();
                         const data = await response.json();
                         console.log(data)
                         console.log(response.status);
                         if (response.status=200) {
                         console.log('ok!');
                         alert('No se pudo abrir la caja');
                         }else{
                         const error = (data && data.message) || response.status;
                         return Promise.reject(error);
                         }
                     }).catch(error => {
                         alert('La caja se abrió correctamente');
                     });
            },
        },
		computed: {
		},
	});