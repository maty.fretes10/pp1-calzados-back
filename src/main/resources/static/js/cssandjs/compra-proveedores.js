
	var app = new Vue({
		el: '#app',
		data: {
			usuario: {
				"nombre": 'Ignacio Stefanini',
				"username": 'ignacio12',
			},
            proveedores:[],
            modalShow: true,
            productos_insumos: [],
            carrito:{
                "store_id":1,
                "supplier_id":1,
                "items":[ ],
            },
            productosElegidos:[],
            resumenProductosElegidos:[],
            verCarrito:false,
            errors: [],
            errores_tarjeta: [],
            errores_efectivo: [],
            errores_cuenta_corriente: [],
            total_pagado:0,
            cart_id: 1,
            balanceActual : {
                balance: 0
            },
		},
		mounted: function(){
            /*
		    fetch("/api/v1/products",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
                this.productos_insumos = data;
			});*/
            fetch("/api/v1/materials",
			{headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "GET"})
			.then(response => response.json())
			.then(data => {
                
                this.productos_insumos= data;

			});

			fetch("/api/v1/suppliers",
            			{headers: {
            				'Accept': 'application/json',
            				'Content-Type': 'application/json'
            			},
            			method: "GET"})
            			.then(response => response.json())
            			.then(data => {this.proveedores = data});
		},
		methods:{
			refrescarDatos(){
                /*
				fetch("/api/v1/products",
				{headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "GET"})
				.then(response => response.json())
				.then(data => {
                    
                    this.productos_insumos = data;
					
				});*/
                fetch("/api/v1/materials",
                {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"})
                .then(response => response.json())
                .then(data => {
                    
                    this.productos_insumos= data;
                });

               
			},
			agregarItem( insumo, required){
			    this.carrito.items.push(
                    {
                        "detail": insumo.name,
                        "quantity": required,
                        "unit_price": insumo.price_buy
                    }
			    );
			    insumo.quantity=required;


                    let found = false;
                      // Add the item or increase qty
                            let itemInCart = this.productosElegidos.filter(item => item.description===item.description);
                            let isItemInCart = itemInCart.length > 0;
                      if (isItemInCart === false) {
                        this.productosElegidos.push(Vue.util.extend({}, insumo));
                      }
                      else {
                                itemInCart[0].quantity += insumo.quantity;
                        }
                        insumo.quantity = 1;
      },
      sacarItem(index) {
        itemElegidos = this.productosElegidos[index];
        itemCarrito = this.carrito.items.filter(item => item.description==itemElegidos.description);
        this.productosElegidos.splice(index, 1);
        for( var i = 0; i < this.carrito.items.length; i++){ 
                                   
          if ( this.carrito.items[i] === itemCarrito[0]) { 
            this.carrito.items.splice(i, itemCarrito.length); 
              i--;
          }
        }
      },
      comprar(){
        fetch("/api/v1/purchase/order",
                {
                credentials: "same-origin",
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.carrito)
                }).then(async response =>{
                    // const data = await response.json();
                    // console.log(data)
                    if (response.ok) {
                      console.log('ok!');
                      alert('Se envió la orden de compra por mail.');
                      setTimeout(function(){
                          location.reload();
                      },500);
                    }
                  })

      },
      borrarCarrito(){
        this.carrito.items=[];
        this.productosElegidos = [];
      },
      seleccionarProveedor(index){
          fetch("/api/v1/client/balance/" + this.proveedores[index].id,
          {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "GET"})
          .then(response => response.json())
          .then(data => {this.balanceActual = data});
      },
		},
		computed: {
            total: function () {
              var total = 0.0;
              this.productosElegidos.forEach( el => {
            	total = total + parseFloat(el.price_buy* el.quantity);
              })
              return parseFloat(total.toFixed(2));
            },
		},
	});