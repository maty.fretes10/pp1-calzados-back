var desplegarVentas = false;
var desplegarStock = false;
var desplegarClientes = false;
var desplegarProveedores = false;
var desplegarProduccion = false;
var desplegarMenu = false;

function desplegar(id){
  if(id == '#Ventas'){
    desplegarVentas= !desplegarVentas;
    if(desplegarVentas){
      $(id).addClass('show');
    }else{
      $(id).removeClass('show');
    }
    desplegarStock = false;
    $('#Stock').removeClass('show');
    desplegarClientes = false;
    $('#Clientes').removeClass('show');
    desplegarProveedores = false;
    $('#Proveedores').removeClass('show');
    desplegarProduccion = false;
    $('#Produccion').removeClass('show');
  }else if(id == '#Stock'){
    desplegarStock= !desplegarStock;
    if(desplegarStock){
      $(id).addClass('show');
    }else{
      $(id).removeClass('show');
    }
    desplegarVentas = false;
    desplegarClientes = false;
    desplegarProveedores = false;
    desplegarProduccion = false;
    $('#Ventas').removeClass('show');
    $('#Clientes').removeClass('show');
    $('#Proveedores').removeClass('show');
    $('#Produccion').removeClass('show');
  }else if(id == '#Clientes'){
    desplegarClientes= !desplegarClientes;
    if(desplegarClientes){
      $(id).addClass('show');
    }else{
      $(id).removeClass('show');
    }
    desplegarStock = false;
    desplegarVentas = false;
    desplegarProveedores = false;
    desplegarProduccion = false;
    $('#Ventas').removeClass('show');
    $('#Stock').removeClass('show');
    $('#Proveedores').removeClass('show');
    $('#Produccion').removeClass('show');
  }else if(id == '#Proveedores'){
    desplegarProveedores= !desplegarProveedores;
    if(desplegarProveedores){
      $(id).addClass('show');
    }else{
      $(id).removeClass('show');
    }
    desplegarStock = false;
    desplegarVentas = false;
    desplegarClientes = false;
    desplegarProduccion = false;
    $('#Ventas').removeClass('show');
    $('#Stock').removeClass('show');
    $('#Clientes').removeClass('show');
    $('#Produccion').removeClass('show');
  }else if(id == '#Produccion'){
    desplegarProduccion= !desplegarProduccion;
    if(desplegarProduccion){
      $(id).addClass('show');
    }else{
      $(id).removeClass('show');
    }
    desplegarStock = false;
    desplegarVentas = false;
    desplegarClientes = false;
    desplegarProveedores = false;
    $('#Ventas').removeClass('show');
    $('#Stock').removeClass('show');
    $('#Clientes').removeClass('show');
    $('#Proveedores').removeClass('show');
  }else if(id == '#accordionSidebar'){
    desplegarMenu= !desplegarMenu;
    if(desplegarMenu){
      $(id).addClass('toggled');
    }else{
      $(id).removeClass('toggled');
    }
    desplegarVentas = false;
    desplegarStock = false;
    desplegarClientes = false;
    desplegarProveedores = false;
    desplegarProduccion = false;
    $('#Ventas').removeClass('show');
    $('#Stock').removeClass('show');
    $('#Produccion').removeClass('show');
    $('#Clientes').removeClass('show');
    $('#Proveedores').removeClass('show');
  }

}



hotkeys('1,2,3,4,5,m', function (event, handler){
    switch (handler.key) {
      case '1': desplegar('#Ventas');
        break;
      case '2': desplegar('#Stock');
        break;
      case '3': desplegar('#Clientes');
        break;
      case '4': desplegar('#Proveedores');
        break;
      case '5': desplegar('#Produccion');
        break;
      case 'm': desplegar('#accordionSidebar');
        break;
      default: console.log(event);
    }
});