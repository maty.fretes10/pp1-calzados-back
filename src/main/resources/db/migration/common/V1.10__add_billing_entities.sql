create table bill (
        id serial primary key,
        type_bill char not null,
        client_id integer not null,
        client_name varchar(50) not null,
        client_address varchar(70) not null,
        client_identification varchar(50) not null,
        client_category_afip varchar(50) not null,
        store_id integer not null,
        store_address varchar(70) not null,
        subtotal float(2) not null default 0,
        iva float(2) not null default 0,
        iva_percentage float(2) not null default 0,
        discount float(2) not null default 0,
        discount_name varchar(50) not null,
        total float (2) not null default 0
);

create table bill_item (
        id serial primary key,
        bill_id integer not null,
        product_id integer not null,
        detail varchar(100) not null,
        unit_price float(2) not null,
        quantity integer not null,
        total_price float(2) not null,

        constraint fk_bill_item foreign key (bill_id) references bill(id)
);

create table bill_pay (
        id serial primary key,
        bill_id integer not null,
        payment_method varchar(100) not null,
        amount float(2) not null,

        constraint fk_bill_item foreign key (bill_id) references bill(id)
);