CREATE TABLE categories_afip (
	id serial PRIMARY KEY,
	name VARCHAR (50) NOT NULL
);

INSERT INTO categories_afip (id, name) VALUES
(1, 'Responsable Inscripto'),
(2, 'Consumidor Final'),
(3, 'Monotributo'),
(4, 'Exento'),
(5, 'Extranjero');