CREATE TABLE sale_transaction (
        id serial primary key,
        client INTEGER not null,
        amount Float(2),
        transaction_state varchar(20) not null,
        creation_date timestamp not null,
        transaction_type varchar(30)
);

CREATE TABLE credit_card_transaction (
        id serial primary key,
        transaction_id integer,
        card_number varchar(4) not null,
        card_type varchar(4) not null,

        constraint fk_credit_card_transaction_sale_transaction FOREIGN KEY (transaction_id) REFERENCES sale_transaction(ID)
);

CREATE TABLE checking_account_transactions (
        id serial PRIMARY KEY,
        transaction_id integer,
        --checking_account_id INTEGER NOT NULL,
        movement_type varchar(20) not null

        --constraint fk_transaction_movement_checking_account FOREIGN KEY (checking_account_id) REFERENCES checking_accounts(id)
);

CREATE TABLE cash_transactions (
    id SERIAL PRIMARY KEY,
    description varchar(30),
    transaction_id integer,
    constraint fk_credit_card_transaction_sale_transaction FOREIGN KEY (transaction_id) REFERENCES sale_transaction(ID)
);

CREATE TABLE transactions_sales (
	transaction_id INTEGER NOT NULL,
    sale_id INTEGER NOT NULL,
    PRIMARY KEY (transaction_id, sale_id),

    CONSTRAINT fk_transactions_sales_transactions FOREIGN KEY(transaction_id) REFERENCES sale_transaction(id),
    CONSTRAINT fk_transactions_sales_sales FOREIGN KEY(sale_id) REFERENCES sales(id)
);