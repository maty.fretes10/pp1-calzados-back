create table production_model (
    id serial primary key,
    product_model_id INTEGER NOT NULL,
    state VARCHAR (30) DEFAULT 'ACTIVE',

    constraint fk_production_model foreign key (product_model_id) references product_model(id)
);


create table item_production (
    id serial primary key,
    id_material INTEGER NOT NULL,
    production_model_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    phase VARCHAR (30) DEFAULT 'CORTADO',

    constraint fk_production_model foreign key (production_model_id) references production_model(id)
);

create table production_order (
    id serial primary key,
    modification_date DATE NOT NULL,
	waist_production_id INTEGER NOT NULL,
	colour_production_id INTEGER NOT NULL,
    production_model_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    store_id integer not null,

    CONSTRAINT fk_production_waist FOREIGN KEY(waist_production_id) REFERENCES waist(id),
    CONSTRAINT fk_production_colours FOREIGN KEY(colour_production_id) REFERENCES colours(id),
    CONSTRAINT fk_production_model FOREIGN KEY(production_model_id) REFERENCES product_model(id)
);

create table production_order_state (
    id serial primary key,
    description VARCHAR(100) NOT NULL,
    production_order_id INTEGER NOT NULL,

    constraint fk_production_order_state foreign key (production_order_id) references production_order(id)
);

create table production_order_history (
    id serial primary key,
    modification_date DATE NOT NULL,
    description VARCHAR(100) NOT NULL,
    production_order_id INTEGER NOT NULL,

    constraint fk_production_order_history foreign key (production_order_id) references production_order(id)
);

CREATE TABLE stock_factory (
	id serial PRIMARY KEY,
	material_id INTEGER,
	total_stock INTEGER,
	store_id INTEGER NOT NULL,

    CONSTRAINT fk_stock_stores_materials FOREIGN KEY(material_id) REFERENCES materials(id),
    CONSTRAINT fk_stock_stores_stores FOREIGN KEY(store_id) REFERENCES stores(id)
);