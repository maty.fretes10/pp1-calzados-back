CREATE TABLE stores (
	id serial PRIMARY KEY,
	name VARCHAR(60) NOT NULL,
	email VARCHAR(60) NOT NULL UNIQUE,
	telephone VARCHAR(20) NOT NULL,
	cuit VARCHAR(20) NOT NULL,
	sale_point VARCHAR(5) NOT NULL UNIQUE,
	street VARCHAR (150) NOT NULL,
	street_number VARCHAR (10) NOT NULL,
	locality_id INTEGER NOT NULL,
	state VARCHAR (30) DEFAULT 'ACTIVE',

	CONSTRAINT fk_stores_localities FOREIGN KEY(locality_id) REFERENCES localities(id)
);

CREATE TABLE cash_register(
    id serial PRIMARY KEY,
    balance float(2) NOT NULL,
    store_id INTEGER NOT NULL,

    CONSTRAINT fk_store FOREIGN KEY(store_id) REFERENCES stores(id)
);

CREATE TABLE daily_register(
    id serial PRIMARY KEY,
    state VARCHAR (30) DEFAULT 'OPEN',
    date DATE NOT NULL,
    balance float(2) NOT NULL,
    cash_register_id INTEGER NOT NULL,

    CONSTRAINT fk_cash_register FOREIGN KEY (cash_register_id) REFERENCES cash_register(id)

);

CREATE TABLE register_items(
    id serial PRIMARY KEY,
    description VARCHAR(150),
    movement_type VARCHAR(30),
    amount float(2) NOT NULL,
    daily_register_id INTEGER NOT NULL,
    date TIMESTAMP NOT NULL,

    CONSTRAINT fk_daily FOREIGN KEY (daily_register_id) REFERENCES daily_register(id)
);

CREATE TABLE stock_stores (
	id serial PRIMARY KEY,
	product_id INTEGER,
	total_stock INTEGER,
	store_id INTEGER NOT NULL,

    CONSTRAINT fk_stock_stores_products FOREIGN KEY(product_id) REFERENCES products(id),
    CONSTRAINT fk_stock_stores_stores FOREIGN KEY(store_id) REFERENCES stores(id)
);
