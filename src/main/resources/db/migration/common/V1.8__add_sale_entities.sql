create table sales (
        id SERIAL PRIMARY KEY,
        cart_id integer not null,
        update_date timestamp not null,
        state varchar(20) not null,
        store_id integer not null,
        total_amount FLOAT(2),
        total_discount_amount float(2),
        discount FLOAT(2),
        sale_type VARCHAR(30),

        constraint fk_cart_sale foreign key (cart_id) references carts(id)
);

create table store_sale (
        id serial primary key,
        sale_id integer not null,
        employee varchar(30),
--        employee_id integer not null,

        constraint fk_sale_store_sale foreign key (sale_id) references sales(id)
 --       constraint fk_employ_store_sale foreign key (employee_id) references employees(id)
);

create table web_sales (
        id serial primary key,
        sale_id integer not null,

        constraint fk_sale_web_sale foreign key (sale_id) references sales(id)
);
