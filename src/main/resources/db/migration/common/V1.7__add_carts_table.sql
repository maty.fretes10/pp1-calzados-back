CREATE TABLE cart_items (
	id serial PRIMARY KEY,
	product_id INTEGER,
	item_count INTEGER,
	subtotal FLOAT(2),
	cart_id INTEGER NOT NULL,

    CONSTRAINT fk_cart_items_products FOREIGN KEY(product_id) REFERENCES products(id)
);

CREATE TABLE carts (
	id serial PRIMARY KEY,
	state VARCHAR(30)
);
