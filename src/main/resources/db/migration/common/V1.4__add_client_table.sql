create TABLE checking_accounts (
	id serial PRIMARY KEY,
	balance FLOAT(2)
);

create TABLE account_history (
    id serial PRIMARY KEY,
	checking_account_id INTEGER NOT NULL,
	movement_date DATE NOT NULL,
	type VARCHAR (30),
	money FLOAT(2),

	CONSTRAINT fk_checking_accounts_history FOREIGN KEY(checking_account_id) REFERENCES checking_accounts(id)
);

create TABLE clients (
	id serial PRIMARY KEY,
	name VARCHAR (100) NOT NULL,
	surname VARCHAR (100) NOT NULL,
	email VARCHAR (100) NOT NULL UNIQUE,
	telephone VARCHAR (30) NOT NULL,
	document_type_id INTEGER NOT NULL,
	document_number VARCHAR(40) NOT NULL,
	category_afip_id INTEGER NOT NULL,
	street VARCHAR (150) NOT NULL,
	street_number VARCHAR (10) NOT NULL,
	locality_id INTEGER NOT NULL,
	creation_date DATE NOT NULL,
	credit_limit FLOAT(2) NOT NULL,
	checking_account_id INTEGER NOT NULL,
	state VARCHAR (30) DEFAULT 'ACTIVE',

    CONSTRAINT fk_clients_categories_afip FOREIGN KEY(category_afip_id) REFERENCES categories_afip(id),
	CONSTRAINT fk_clients_localities FOREIGN KEY(locality_id) REFERENCES localities(id),
	CONSTRAINT fk_clients_document_types FOREIGN KEY(document_type_id) REFERENCES document_types(id),
	CONSTRAINT fk_clients_checking_accounts FOREIGN KEY(checking_account_id) REFERENCES checking_accounts(id)
);