CREATE TABLE document_types (
	id serial PRIMARY KEY,
	name VARCHAR (50) NOT NULL
);

INSERT INTO document_types (id, name) VALUES
(1, 'DNI'),
(2, 'CUIT'),
(3, 'CUIL');