create TABLE materials (
        id serial PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        description VARCHAR(100) NOT NULL,
        price_buy FLOAT(2),
        creation_date DATE NOT NULL,
        stock_min INTEGER NOT NULL,
        stock INTEGER NOT NULL,
        state VARCHAR (30) DEFAULT 'ACTIVE'

);

create TABLE mats_suppliers_favs (

                       id serial PRIMARY KEY,
                       id_material INTEGER NOT NULL,
                       id_proveedor INTEGER NOT NULL,

                       CONSTRAINT fk_id_material FOREIGN KEY(id_material) REFERENCES materials(id),
                       CONSTRAINT fk_id_proveedor FOREIGN KEY(id_proveedor) REFERENCES suppliers(id)
                       );


