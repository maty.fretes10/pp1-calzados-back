create TABLE colours (
    id serial PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    state VARCHAR (30) DEFAULT 'ACTIVE'
);

create TABLE waist (
    id serial PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    state VARCHAR (30) DEFAULT 'ACTIVE'
);

create TABLE product_status (
    id serial PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

create TABLE product_model (
    id serial PRIMARY KEY,
    state VARCHAR (30) DEFAULT 'ACTIVE',
    name VARCHAR(100) NOT NULL,
    mark VARCHAR(100) NOT NULL
);

create TABLE products (
	id serial PRIMARY KEY,
	description VARCHAR (100) NOT NULL,
	creation_date DATE NOT NULL,
	year_fabrication INTEGER NOT NULL,
	price_sale FLOAT(2),
	price_buy FLOAT(2),
	waist_product_id INTEGER NOT NULL,
	colour_product_id INTEGER NOT NULL,
    product_status_id INTEGER NOT NULL,
    product_model_id INTEGER NOT NULL,
    stock_security INTEGER NOT NULL,
    stock_minimum INTEGER NOT NULL,
    stock_consolidated INTEGER NOT NULL,
    state VARCHAR (30) DEFAULT 'ACTIVE',

    CONSTRAINT fk_products_waist FOREIGN KEY(waist_product_id) REFERENCES waist(id),
    CONSTRAINT fk_products_colours FOREIGN KEY(colour_product_id) REFERENCES colours(id),
    CONSTRAINT fk_products_product_status FOREIGN KEY(product_status_id) REFERENCES product_status(id),
    CONSTRAINT fk_products_product_model FOREIGN KEY(product_model_id) REFERENCES product_model(id)

);

create TABLE price_history (
    id serial PRIMARY KEY,
    product_id INTEGER NOT NULL,
	creation_date DATE NOT NULL,
	price FLOAT(2),

	CONSTRAINT fk_product_price_history FOREIGN KEY(product_id) REFERENCES products(id)
);

INSERT INTO product_status (id, name) VALUES
(1, 'FABRICADO'),
(2, 'COMPRADO');
