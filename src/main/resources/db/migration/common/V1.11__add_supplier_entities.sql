create TABLE suppliers (
        id serial PRIMARY KEY,
        name VARCHAR (100) NOT NULL,
        email VARCHAR (100) NOT NULL UNIQUE,
        state VARCHAR (30) DEFAULT 'ACTIVE',
        street VARCHAR (100) NOT NULL,
        street_number INTEGER NOT NULL,
        telephone VARCHAR (100) NOT NULL,
        cuit VARCHAR (100) NOT NULL,
        checking_account_id INTEGER NOT NULL,
        locality_id INTEGER NOT NULL,
        CONSTRAINT fk_locality_id FOREIGN KEY (locality_id) REFERENCES localities(id),
        CONSTRAINT fk_suppliers_checking_account FOREIGN KEY (checking_account_id) REFERENCES checking_accounts(id)

);

create TABLE prod_suppliers_favs (
    id serial PRIMARY KEY,
    id_product INTEGER NOT NULL,
    id_supplier INTEGER NOT NULL,

    CONSTRAINT fk_id_product FOREIGN KEY(id_product) REFERENCES products(id),
    CONSTRAINT fk_id_supplier FOREIGN KEY(id_supplier) REFERENCES suppliers(id)
);
