create table purchase_order (
        id serial primary key,
        state VARCHAR (30) DEFAULT 'ACTIVE',
        supplier_name varchar(50) not null,
        supplier_address varchar(70) not null,
        supplier_identification varchar(50) not null,
        store_id integer not null,
        store_address varchar(70) not null,
        total float (2) not null default 0
);

create table purchase_order_item (
        id serial primary key,
        purchase_order_id integer not null,
        detail varchar(100) not null,
        unit_price float(2) not null,
        quantity integer not null,
        total_price float(2) not null,

        constraint fk_purchase_order_item foreign key (purchase_order_id) references purchase_order(id)
);

