create table usuario (
        id serial primary key,
        email VARCHAR (255) not null,
        first_name varchar(50) not null,
        last_name varchar(70) not null,
        password varchar(255) not null,
        username varchar(255) not null
);

create table role (
        id serial primary key,
        description VARCHAR (255) not null,
        name varchar(100) not null
);

create table user_roles (
        user_id INTEGER NOT NULL,
        role_id INTEGER NOT NULL
);

INSERT INTO usuario (email, first_name, last_name, password, username) VALUES ('admin@admin.com', 'admin', 'admin', '$2a$04$n6WIRDQlIByVFi.5rtQwEOTAzpzLPzIIG/O6quaxRKY2LlIHG8uty', 'admin');
INSERT INTO role (description, name) VALUES ('ROLE_ADMIN', 'ADMIN');
INSERT INTO role (description, name) VALUES ('ROLE_VENDEDOR', 'VENDEDOR');
INSERT INTO role (description, name) VALUES ('ROLE_SUPERVISORSUCURSAL', 'SUPERVISOR DE TIENDA');
INSERT INTO role (description, name) VALUES ('ROLE_SUPERVISORFABRICA', 'SUPERVISOR DE FABRICA');
INSERT INTO role (description, name) VALUES ('ROLE_GERENTE', 'GERENTE');
INSERT INTO role (description, name) VALUES ('ROLE_OPERARIO', 'OPERARIO');
INSERT INTO user_roles (user_id, role_id) VALUES ('1', '1');

ALTER SEQUENCE usuario_id_seq RESTART WITH 2;

create table user_store (
        id serial primary key,
        user_id INTEGER NOT NULL,
        store_id INTEGER NOT NULL,

        CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES usuario(id),
        CONSTRAINT fk_stores FOREIGN KEY(store_id) REFERENCES stores(id)
);