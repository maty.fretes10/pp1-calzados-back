package com.ungs.calzados.exception;

public class SaleException extends RuntimeException {

    public SaleException() {
        super();
    }

    public SaleException(String message) {
        super(message);
    }
}