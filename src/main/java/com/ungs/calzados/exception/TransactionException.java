package com.ungs.calzados.exception;

public class TransactionException extends RuntimeException {

    public TransactionException() {
        super();
    }

    public TransactionException(String message) {
        super(message);
    }
}