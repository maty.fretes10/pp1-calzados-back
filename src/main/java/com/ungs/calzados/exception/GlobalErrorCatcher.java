package com.ungs.calzados.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class GlobalErrorCatcher {

    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleResourceNotFoundException(ResourceNotFoundException exception) {
        return constructErrorResponse(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleResourceNotFoundException(NoDataFoundException exception) {
        return constructErrorResponse(exception, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleDataAccessException(DataAccessException exception) {
        return constructErrorResponse(exception, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        return constructErrorListResponse(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalStateException.class})
    public ResponseEntity<ErrorObject> handleIllegalStateException(Exception exception) {
        return constructErrorResponse(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({IllegalArgumentException.class, SaleException.class, NotEnoughFundsException.class})
    public ResponseEntity<ErrorObject> handleIllegalArgumentException(RuntimeException exception) {
        return constructErrorResponse(exception, HttpStatus.PRECONDITION_FAILED);
    }

    @ExceptionHandler
    public ResponseEntity<StockErrorObject> handleNotEnoughStockException(NotEnoughStockException exception) {
        return constructErrorListResponse(exception, HttpStatus.CONFLICT);
    }

    private ResponseEntity<ErrorObject> constructErrorResponse(Exception exception, HttpStatus httpStatus) {
        ErrorObject errorObject = ErrorObject
                .builder()
                .message(exception.getMessage())
                .httpCode(httpStatus.value())
                .timestamp(System.currentTimeMillis())
                .build();

        return new ResponseEntity<>(errorObject, httpStatus);
    }

    private ResponseEntity<ErrorObject> constructErrorListResponse(MethodArgumentNotValidException exception, HttpStatus httpStatus) {

        List<String> errors = exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        ErrorObject errorObject = ErrorObject.builder()
                .messages(errors)
                .timestamp(System.currentTimeMillis())
                .httpCode(httpStatus.value())
                .build();


        return new ResponseEntity<>(errorObject, httpStatus);
    }

    private ResponseEntity<StockErrorObject> constructErrorListResponse(NotEnoughStockException exception, HttpStatus httpStatus) {

        StockErrorObject stockErrorObject = exception.getStockErrorObject();
        stockErrorObject.setHttpCode(httpStatus.value());
        stockErrorObject.setTimestamp(System.currentTimeMillis());

        return new ResponseEntity<>(stockErrorObject, httpStatus);
    }
}