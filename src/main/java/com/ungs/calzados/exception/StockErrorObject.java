package com.ungs.calzados.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StockErrorObject{
    private List<StockSingleErrorObject> errors;
    private Integer httpCode;
    private Long timestamp;
}
