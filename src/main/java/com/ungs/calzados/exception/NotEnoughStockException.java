package com.ungs.calzados.exception;

import lombok.Getter;

@Getter
public class NotEnoughStockException extends RuntimeException {

    private final StockErrorObject stockErrorObject;

    public NotEnoughStockException(StockErrorObject stockErrorObject) {
        super();
        this.stockErrorObject = stockErrorObject;
    }
}
