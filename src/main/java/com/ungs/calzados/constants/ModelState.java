package com.ungs.calzados.constants;

public enum ModelState {
    ACTIVE, DISCONTINUED
}
