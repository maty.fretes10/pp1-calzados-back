package com.ungs.calzados.constants;

public enum StateSale {
    APPROVED, REJECTED, IN_PROGRESS, CANCELLED
}
