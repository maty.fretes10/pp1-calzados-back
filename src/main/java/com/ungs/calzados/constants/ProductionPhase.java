package com.ungs.calzados.constants;

public enum ProductionPhase {
    INICIADO, CORTADO, APARADO, PREPARACION, MONTADO, PEGADO, TERMINADO, CANCELADO
}
