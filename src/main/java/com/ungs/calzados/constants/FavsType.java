package com.ungs.calzados.constants;

public enum FavsType {
    MATERIAL, SUPPLIER, PRODUCT
}
