package com.ungs.calzados.constants;

public enum MovementType {
    INPUT, OUTPUT
}
