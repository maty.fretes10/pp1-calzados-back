package com.ungs.calzados.constants;

public enum TransactionType {
    CASH, CREDIT_CARD, ACCOUNT
}
