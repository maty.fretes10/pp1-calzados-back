package com.ungs.calzados.constants;

public enum MaterialState {
    ACTIVE, INACTIVE
}
