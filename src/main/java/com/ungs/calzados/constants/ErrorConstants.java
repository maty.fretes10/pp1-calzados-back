package com.ungs.calzados.constants;

public class ErrorConstants {



    private ErrorConstants() {
    }

    public static final String ACCOUNT_BY_ID_IS_NULL = "Cuenta con id %s no se encontró";

    //Clients
    public static final String EMPTY_CLIENTS_LIST = "Lista de clientes vacía";
    public static final String CLIENT_BY_ID_NOT_FOUND = "Cliente con id %s no se encontró";
    public static final String DOCUMENT_TYPE_BY_ID_NOT_FOUND = "El Id del tipo de documento no se encontró";
    public static final String CATEGORY_AFIP_BY_ID_NOT_FOUND = "El Id de la categoria AFIP no se encontró";
    public static final String EMPTY_LOCALITIES_LIST_BY_PROVINCE_ID = "No se encontraron localidades de esa provincia";
    public static final String LOCALITY_BY_ID_NOT_FOUND = "No se encontro la localidad con id %s";


    //Products
    public static final String EMPTY_PRODUCTS_LIST = "Lista de productos vacía";
    public static final String COLOR_BY_ID_NOT_FOUND = "Color con id %s no se encontró";
    public static final String WAIST_BY_ID_NOT_FOUND = "Talle con id %s no se encontró";
    public static final String MODEL_PRODUCT_BY_ID_NOT_FOUND = "Modelo de producto con id %s no se encontró";
    public static final String STATUS_PRODUCTO_BY_ID_NOT_FOUND = "Estado del producto con id %s no se encontró";
    public static final String MODEL_PRODUCT_BY_ID_IS_NULL ="El id del modelo es null";
    public static final String STATE_PRODUCT_MODEL_DISCONTINUED ="El modelo con id %s ya se encuentra descontinuado";



    public static final String CART_BY_ID_NOT_FOUND = "Carrito con id %s no se encontró";
    public static final String STORE_BY_ID_NOT_FOUND = "Sucursal con id %s no se encontró";
    public static final String STORE_BY_EMAIL_ALREADY_EXISTS = "Ya existe una sucursal con ese correo";
    public static final String STORE_BY_SALE_POINT_ALREADY_EXISTS = "El punto de venta ya existe";
    public static final String SALE_BY_ID_NOT_EXISTS = "Venta con id %s no existe";
    public static final String CART_BY_ID_NOT_ACTIVE = "Carrito con id %s no existe";
    public static final String TOTAL_AMOUNT_NOT_MATCH = "Monto total no coincide";
    public static final String CLIENT_BY_ID_IS_NULL = "Cliente con id %s es null";
    public static final String PRODUCT_BY_ID_NOT_FOUND = "Producto con id %s no se encontró";
    public static final String ACCOUNT_BY_ID_NOT_FOUND = "Cuenta corriente con id %s no se encontro";
    public static final String EMPTY_ACCOUNT_HISTORY_LIST = "Historial de cuenta vacio";
    public static final String CLIENT_BY_STATE_INACTIVE_EXISTS = "Cliente con id %s ya está inactivo";
    public static final String EMPTY_SALE_LIST = "Lista de ventas vacía";

    //Proveedores
    public static final String EMPTY_SUPPLIERS_LIST = "Lista de proveedores vacía";
    public static final String SUPPLIER_BY_STATE_INACTIVE_EXISTS = "El proveedor ya se encuentra inactivo";
    public static final String SUPPLIER_BY_ID_IS_NULL = "El id del proveedor no se encontró";
    public static final String SUPPLIER_BY_ID_NOT_FOUND = "proveedor con id %s no se encontró";

    //Insumos
    public static final String EMPTY_MATERIALS_LIST = "Lista de insumos vacía";
    public static final String MATERIAL_BY_ID_IS_NULL = "El id del insumo no se encontró";
    public static final String MATERIAL_BY_ID_NOT_FOUND = "El insumo con id %s no se encontró";
    public static final String MATERIAL_BY_STATE_INACTIVE_EXISTS = "El insumo ya se encuentra inactivo";

    //Favoritos
    public static final String MATERIAL_ID_ALREADY_HAS_FAV = "El insumo con id %s ya tiene un proveedor favorito";

    //Stores
    public static final String EMPTY_STORES_LIST = "Lista de sucursales vacía";
    public static final String EMPTY_DAILIES_LIST ="Lista de cajas diarias vacía";
    public static final String EMPTY_ITEMS_LIST = "Lista de items vacía";
    public static final String DAILY_REGISTER_NOT_FOUND= "No se encontró una caja diaria abierta con la fecha de hoy";

    //Produccion
    public static final String EMPTY_ORDER_PRODUCTION_LIST = "Lista de ordenes de produccion vacia";
    public static final String PRODUCTION_ORDER_BY_ID_NOT_FOUND = "No se encontro la orden de produccion con ese id";
    public static final String EMPTY_PRODUCTION_MODEL_LIST = "Lista de modelos de produccion vacia";
    public static final String PRODUCTION_MODEL_BY_ID_NOT_FOUND = "No se encontro la orden de produccion";
    public static final String EMPTY_PRODUCTION_HISTORY_LIST = "Historial de la orden de produccion vacia";

    public static final String PRODUCT_ID_ALREADY_HAS_FAV = "El product con id %s ya tiene un proveedor favorito";
    public static final String FAV_RELATION_IS_NOT_SET =
            "La relación entre el producto o insumo id: %s y el proveedor id: %s no existe";

    public static final String EMPTY_PURCHASE_ORDER_LIST = "Lista de ordenes de compra vacia";
    public static final String INACTIVE_PURCHASE_ORDER = "La orden de compra ya fue cancelada";
    public static final String PURCHASE_ORDER_BY_ID_NOT_FOUND = "La orden de compra no se encontró";

    public static final String STATE_COLOR_INACTIVE = "El color ya se encuentra inactivo";
    public static final String STATE_WAIST_INACTIVE = "El talle ya se encuentra inactivo";

}
