package com.ungs.calzados.constants;

public enum AccountType {
    CLIENT, SUPPLIER
}
