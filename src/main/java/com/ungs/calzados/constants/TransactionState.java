package com.ungs.calzados.constants;

public enum TransactionState {
    APPROVED, REJECTED
}
