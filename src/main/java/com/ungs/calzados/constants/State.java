package com.ungs.calzados.constants;

public enum State {
    ACTIVE, INACTIVE
}
