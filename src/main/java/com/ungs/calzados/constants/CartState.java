package com.ungs.calzados.constants;

public enum CartState {
    ACTIVE, CANCELLED, COMPLETED
}
