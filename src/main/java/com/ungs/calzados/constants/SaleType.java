package com.ungs.calzados.constants;

public enum SaleType {
    WEB, STORE
}
