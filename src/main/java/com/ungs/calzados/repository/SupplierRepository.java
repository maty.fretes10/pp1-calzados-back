package com.ungs.calzados.repository;

import com.ungs.calzados.entity.Supplier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends CrudRepository<Supplier, Integer> {

    List<Supplier> findAll();

}
