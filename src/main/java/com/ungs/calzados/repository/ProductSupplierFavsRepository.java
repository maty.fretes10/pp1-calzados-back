package com.ungs.calzados.repository;

import com.ungs.calzados.entity.ProductSupplierFavs;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductSupplierFavsRepository extends  CrudRepository<ProductSupplierFavs, Integer> {
    @Query("SELECT x FROM ProductSupplierFavs x WHERE x.product.id=:id")
    List<ProductSupplierFavs> findAllByProductsId (Integer id);

    @Query("SELECT x FROM ProductSupplierFavs x WHERE x.supplier.id=:id")
    List<ProductSupplierFavs> findAllBySupplierId (Integer id);

    @Query("SELECT x FROM ProductSupplierFavs x WHERE x.supplier.id=:id")
    List<Optional <ProductSupplierFavs>> findAllBySupplierIdOpcionalList(Integer id);

    @Query("SELECT x FROM ProductSupplierFavs x WHERE x.product.id=:id")
    Optional<ProductSupplierFavs> findByProductIdOpcional(Integer id);

    List<ProductSupplierFavs> findAll();

    @Query("SELECT x FROM ProductSupplierFavs x WHERE x.product.id=:productId and x.supplier.id=:supplierId")
    Optional<ProductSupplierFavs> findByProductSupplierFavsOpcional (Integer productId, Integer supplierId);


}
