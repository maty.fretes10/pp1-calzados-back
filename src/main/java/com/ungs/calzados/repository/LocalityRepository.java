package com.ungs.calzados.repository;

import com.ungs.calzados.entity.Locality;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LocalityRepository extends CrudRepository<Locality, Integer> {
    @Query("select l from Locality l where l.province.id=:provinceId")
    List<Locality> findAllByProvinceId(Integer provinceId);
}
