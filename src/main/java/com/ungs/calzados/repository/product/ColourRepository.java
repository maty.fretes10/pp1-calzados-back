package com.ungs.calzados.repository.product;

import com.ungs.calzados.entity.product.Colour;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ColourRepository extends CrudRepository<Colour, Integer> {
    List<Colour> findAll();

    Optional<Colour> findById(Integer colourId);

    Optional<Colour> findByName(String name);

}
