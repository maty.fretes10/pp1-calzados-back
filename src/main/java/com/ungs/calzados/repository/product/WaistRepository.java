package com.ungs.calzados.repository.product;

import com.ungs.calzados.entity.product.Waist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface WaistRepository extends CrudRepository<Waist, Integer> {
    List<Waist> findAll();

    Optional<Waist> findById(Integer colourId);

    Optional<Waist> findByName(String name);
}
