package com.ungs.calzados.repository.product;

import com.ungs.calzados.entity.product.ProductModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductModelRepository extends CrudRepository<ProductModel, Integer> {
    List<ProductModel> findAll();

    Optional<ProductModel> findById(Integer modelId);
}
