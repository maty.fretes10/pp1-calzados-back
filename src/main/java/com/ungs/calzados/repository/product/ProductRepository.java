package com.ungs.calzados.repository.product;

import com.ungs.calzados.entity.product.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findAll();

    Optional<Product> findById(Integer clientId);

    @Query("SELECT x FROM Product x WHERE x.productModel.id = :model AND x.colour.id = :color AND x.waist.id = :waist")
    Optional<Product> findByModelColorWaist(Integer model, Integer color, Integer waist);

}
