package com.ungs.calzados.repository.product;

import com.ungs.calzados.entity.product.ProductStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductStatusRepository extends CrudRepository<ProductStatus, Integer> {
    List<ProductStatus> findAll();

    Optional<ProductStatus> findById(Integer colourId);
}
