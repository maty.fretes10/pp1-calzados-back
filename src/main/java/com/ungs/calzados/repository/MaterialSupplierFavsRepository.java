package com.ungs.calzados.repository;

import com.ungs.calzados.entity.MaterialSupplierFavs;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface MaterialSupplierFavsRepository extends CrudRepository<MaterialSupplierFavs, Integer> {
   @Query("SELECT x FROM MaterialSupplierFavs x WHERE x.material.id=:id")
   List<MaterialSupplierFavs> findAllByMaterialsId(Integer id);

   @Query("SELECT x FROM MaterialSupplierFavs x WHERE x.material.id=:id")
   Optional<MaterialSupplierFavs> findByMaterialsIdOpcional(Integer id);

   @Query("SELECT x FROM MaterialSupplierFavs x WHERE x.supplier.id=:id")
   List<MaterialSupplierFavs> findAllBySupplierId(Integer id);

   @Query("SELECT x FROM MaterialSupplierFavs x WHERE x.supplier.id=:id")
   List<Optional<MaterialSupplierFavs>> findAllBySupplierIdOpcionalList(Integer id);

   List<MaterialSupplierFavs> findAll();

   @Query("SELECT x FROM MaterialSupplierFavs x WHERE x.material.id=:materialId and x.supplier.id=:supplierId")
   Optional<MaterialSupplierFavs> findByMaterialSupplierFavsOpcional (Integer materialId, Integer supplierId);

}
