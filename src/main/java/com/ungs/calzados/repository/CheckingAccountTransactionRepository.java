package com.ungs.calzados.repository;

import com.ungs.calzados.entity.sale.CheckingAccountTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckingAccountTransactionRepository extends CrudRepository<CheckingAccountTransaction, Integer> {
}
