package com.ungs.calzados.repository;

import com.ungs.calzados.entity.DocumentType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentTypeRepository extends CrudRepository<DocumentType, Integer> {
    List<DocumentType> findAll();

    Optional<DocumentType> findById(Integer documentTypeId);
}
