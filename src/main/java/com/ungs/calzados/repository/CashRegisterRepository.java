package com.ungs.calzados.repository;

import com.ungs.calzados.entity.CashRegister;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CashRegisterRepository extends CrudRepository<CashRegister, Integer> {
    @Query ("SELECT x FROM CashRegister x WHERE x.store.id=:storeId" )
    Optional<CashRegister> findByStore(Integer storeId);


    }
