package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.StockStore;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface StockStoreRepository extends CrudRepository<StockStore, Integer> {

    Optional<StockStore> findById(Integer storeId);

    @Query("select s from StockStore s where s.store.id = :storeId and s.product.id IN :idList")
    List<StockStore> findByIdInList(Integer storeId, List<Integer> idList);

    @Query("select a from StockStore a where a.store.id = :storeId and a.product.id = :productId")
    Optional<StockStore> findByStoreIdAndProductId(Integer storeId, Integer productId);

    @Query("SELECT x FROM StockStore x WHERE x.product.id=:id")
    List<Optional<StockStore>> findByProductId(Integer id);

    @Query("SELECT x FROM StockStore x WHERE x.store.id=:id")
    List<Optional<StockStore>> findByStoreId(Integer id);
}
