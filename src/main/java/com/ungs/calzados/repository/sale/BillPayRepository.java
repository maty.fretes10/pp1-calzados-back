package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.BillPay;
import org.springframework.data.repository.CrudRepository;

public interface BillPayRepository extends CrudRepository<BillPay, Integer> {
}
