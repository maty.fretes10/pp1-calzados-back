package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.SaleWeb;
import org.springframework.data.repository.CrudRepository;

public interface SaleWebRepository extends CrudRepository<SaleWeb, Integer> {
}
