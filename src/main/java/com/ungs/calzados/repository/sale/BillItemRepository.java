package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.BillItem;
import org.springframework.data.repository.CrudRepository;

public interface BillItemRepository extends CrudRepository<BillItem, Integer> {
}
