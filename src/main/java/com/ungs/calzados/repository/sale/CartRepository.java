package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Integer> {
}
