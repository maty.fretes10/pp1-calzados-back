package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {
}
