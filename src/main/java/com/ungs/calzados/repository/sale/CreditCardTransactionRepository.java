package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.CreditCardTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CreditCardTransactionRepository extends CrudRepository<CreditCardTransaction, Integer> {
}
