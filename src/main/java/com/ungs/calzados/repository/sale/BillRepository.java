package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.Bill;
import org.springframework.data.repository.CrudRepository;

public interface BillRepository extends CrudRepository<Bill, Integer> {
}
