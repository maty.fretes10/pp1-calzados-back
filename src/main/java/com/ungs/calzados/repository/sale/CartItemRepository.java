package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.CartItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartItemRepository extends CrudRepository<CartItem, Integer> {
    @Query("select l from CartItem l where l.cart.id=:cartId")
    List<CartItem> findByCartId(Integer cartId);
}
