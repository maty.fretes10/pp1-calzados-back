package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.SaleStore;
import org.springframework.data.repository.CrudRepository;

public interface SaleStoreRepository extends CrudRepository<SaleStore, Integer> {
}
