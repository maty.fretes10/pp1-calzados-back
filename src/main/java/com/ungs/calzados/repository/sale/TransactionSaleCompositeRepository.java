package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.TransactionSaleComposite;
import com.ungs.calzados.entity.sale.TransactionSalePk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionSaleCompositeRepository extends CrudRepository<TransactionSaleComposite, TransactionSalePk> {

    @Query("SELECT x FROM TransactionSaleComposite x WHERE x.sale.id =:saleId")
    List<TransactionSaleComposite> findBySaleId(Integer saleId);
}
