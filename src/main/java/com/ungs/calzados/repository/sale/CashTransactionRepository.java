package com.ungs.calzados.repository.sale;

import com.ungs.calzados.entity.sale.CashTransaction;
import org.springframework.data.repository.CrudRepository;

public interface CashTransactionRepository extends CrudRepository<CashTransaction, Integer> {
}
