package com.ungs.calzados.repository;

import com.ungs.calzados.entity.buy.PurchaseOrderItem;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseOrderItemRepository extends CrudRepository<PurchaseOrderItem, Integer> {
}
