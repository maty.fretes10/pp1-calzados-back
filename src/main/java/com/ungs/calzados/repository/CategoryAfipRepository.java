package com.ungs.calzados.repository;

import com.ungs.calzados.entity.CategoryAfip;
import com.ungs.calzados.entity.DocumentType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryAfipRepository extends CrudRepository<CategoryAfip, Integer> {
    Optional<CategoryAfip> findById(Integer id);

    List<CategoryAfip> findAll();
}
