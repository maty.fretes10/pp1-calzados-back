package com.ungs.calzados.repository;

import com.ungs.calzados.entity.AccountHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountHistoryRepository extends CrudRepository<AccountHistory, Integer> {
    List<AccountHistory> findAll();

    @Query("select l from AccountHistory l where l.checkingAccountId.id=:accountId")
    List<AccountHistory> findAllByCheckingAccount (Integer accountId);
}
