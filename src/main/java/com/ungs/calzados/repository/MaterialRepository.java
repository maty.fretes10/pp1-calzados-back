package com.ungs.calzados.repository;


import com.ungs.calzados.entity.Material;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MaterialRepository extends CrudRepository<Material, Integer>{
     List<Material> findAll();
}


