package com.ungs.calzados.repository;

import com.ungs.calzados.entity.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Integer> {
    List<Client> findAll();

    Optional<Client> findById(Integer clientId);


}
