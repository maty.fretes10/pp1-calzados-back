package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.ProductionOrderHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionOrderHistoryRepository extends CrudRepository<ProductionOrderHistory, Integer> {
    @Query("select l from ProductionOrderHistory l where l.productionOrderId.id=:id")
    List<ProductionOrderHistory> findAllByOrderId(Integer id);
}
