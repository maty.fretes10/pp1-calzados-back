package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.ItemProduction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemProductionRepository extends CrudRepository<ItemProduction, Integer> {
    @Query("select l from ItemProduction l where l.productionModel.id=:productionModelId")
    List<ItemProduction> findItemProductionByProductionModelId(Integer productionModelId);
}
