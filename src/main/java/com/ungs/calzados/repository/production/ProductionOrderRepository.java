package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.ProductionOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionOrderRepository extends CrudRepository<ProductionOrder, Integer> {
}
