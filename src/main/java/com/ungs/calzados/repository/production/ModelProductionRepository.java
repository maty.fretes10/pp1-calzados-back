package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.ProductionModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelProductionRepository extends CrudRepository<ProductionModel, Integer> {
}
