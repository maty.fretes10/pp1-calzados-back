package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.ProductionOrderState;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductionOrderRepositoryState extends CrudRepository<ProductionOrderState, Integer> {
    List<ProductionOrderState> findAll();

    @Query("select l from ProductionOrderState l where l.productionOrderId.id=:id")
    Optional<ProductionOrderState> findByIdOrder(Integer id);
}
