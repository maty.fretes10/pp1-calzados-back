package com.ungs.calzados.repository.production;

import com.ungs.calzados.entity.production.StockFactory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockFactoryRepository extends CrudRepository<StockFactory, Integer> {

    Optional<StockFactory> findById(Integer storeId);

    @Query("select s from StockFactory s where s.store.id = :storeId and s.material.id IN :idList")
    List<StockFactory> findByIdInList(Integer storeId, List<Integer> idList);

    @Query("select a from StockFactory a where a.store.id = :storeId and a.material.id = :materialId")
    Optional<StockFactory> findByStoreIdAndMaterialId(Integer storeId, Integer materialId);

}
