package com.ungs.calzados.repository;

import com.ungs.calzados.entity.CheckingAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckingAccountRepository extends CrudRepository<CheckingAccount, Integer> {
}
