package com.ungs.calzados.repository;

import com.ungs.calzados.entity.RegisterItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegisterItemRepository extends CrudRepository<RegisterItem, Integer> {

    @Query("SELECT x FROM RegisterItem x WHERE x.dailyRegister.id=:dailyRegisterId")
    List<RegisterItem> findByRegisterId(Integer dailyRegisterId);


}
