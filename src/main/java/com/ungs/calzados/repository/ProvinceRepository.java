package com.ungs.calzados.repository;

import com.ungs.calzados.entity.Province;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProvinceRepository extends CrudRepository<Province, Integer> {
    List<Province> findAll();
}
