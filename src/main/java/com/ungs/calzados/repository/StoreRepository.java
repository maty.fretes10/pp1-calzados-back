package com.ungs.calzados.repository;

import com.ungs.calzados.entity.Store;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StoreRepository extends CrudRepository<Store, Integer> {
    Optional<Store> findById(Integer storeId);

    List<Store> findAll();

    boolean existsByEmailIgnoreCase(String email);
    boolean existsBySalePoint(String salePoint);
}
