package com.ungs.calzados.repository;

import com.ungs.calzados.entity.buy.PurchaseOrder;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Integer> {
}
