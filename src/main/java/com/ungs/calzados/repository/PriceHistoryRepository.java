package com.ungs.calzados.repository;

import com.ungs.calzados.entity.product.PriceHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceHistoryRepository extends CrudRepository<PriceHistory, Integer> {
}
