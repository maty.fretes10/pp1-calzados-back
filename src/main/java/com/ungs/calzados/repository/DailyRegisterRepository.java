package com.ungs.calzados.repository;

import com.ungs.calzados.entity.DailyRegister;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface DailyRegisterRepository extends CrudRepository<DailyRegister, Integer> {

    List<DailyRegister> findAll();

    @Query ("SELECT x FROM DailyRegister x WHERE x.cashRegister.id=:cashRegisterId")
    List<DailyRegister> findByCashRegisterId(Integer cashRegisterId);

    @Query("SELECT x FROM DailyRegister x WHERE x.date=:date AND x.cashRegister.id=:cashRegisterId")
    Optional<DailyRegister> existsByDate(LocalDate date, Integer cashRegisterId);
}
