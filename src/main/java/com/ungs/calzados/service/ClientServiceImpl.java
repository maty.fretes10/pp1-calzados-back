package com.ungs.calzados.service;

import com.ungs.calzados.constants.ClientState;
import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.ClientRequestDto;
import com.ungs.calzados.entity.*;
import com.ungs.calzados.repository.ClientRepository;
import com.ungs.calzados.dto.ClientResponseDto;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, ModelMapper modelMapper) {
        this.clientRepository = clientRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<ClientResponseDto> getClients() {
        List<Client> allClients = clientRepository.findAll();

        if (!allClients.isEmpty()) {
            return allClients.stream()
                    .parallel()
                    .map(client -> modelMapper.map(client, ClientResponseDto.class))
                    .collect(Collectors.toList());
        }

        log.warn(ErrorConstants.EMPTY_CLIENTS_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_CLIENTS_LIST);
    }
    @Override
    public void addClient(
            ClientRequestDto clientRequest,
            CheckingAccount checkingAccountEntity,
            DocumentType documentTypeEntity,
            CategoryAfip categoryAfipEntity,
            Locality localityEntity) {

        Client entity = modelMapper.map(clientRequest, Client.class);

        entity.setState(ClientState.ACTIVE);
        entity.setCreationDate(LocalDate.now());
        entity.setCheckingAccount(checkingAccountEntity);
        entity.setDocumentType(documentTypeEntity);
        entity.setCategoryAfip(categoryAfipEntity);
        entity.setLocality(localityEntity);

        clientRepository.save(entity);
    }

    @Override
    public Client getClientByID(Integer id) {
        final Optional<Client> optionalClient = this.clientRepository.findById(id);
        if (!optionalClient.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.CLIENT_BY_ID_NOT_FOUND);

        return optionalClient.get();
    }

    @Override
    public ClientResponseDto getClientByIdResponse(Integer clientId) {
        Optional<Integer> optionalId = Optional.ofNullable(clientId);
        if (!optionalId.isPresent()) {
            throw new IllegalStateException(ErrorConstants.CLIENT_BY_ID_IS_NULL);
        }

        Optional<Client> optionalClient = this.clientRepository.findById(clientId);
        if (!optionalClient.isPresent()) {
            String message = String.format(ErrorConstants.CLIENT_BY_ID_NOT_FOUND, clientId);
            log.warn(message);
            throw new ResourceNotFoundException(message);
        }

        Client clientFound = optionalClient.get();
        log.info("Client with id {} was found", clientId);
        return modelMapper.map(clientFound, ClientResponseDto.class);
    }

    @Override
    public void inactivateClient(Integer clientId) {
        Optional<Client> optionalById = clientRepository.findById(clientId);
        String errorMessage = String.format(ErrorConstants.CLIENT_BY_ID_NOT_FOUND, clientId);
        String errorMessageClientInactive = String.format(ErrorConstants.CLIENT_BY_STATE_INACTIVE_EXISTS, clientId);

        if (optionalById.isPresent()) {
            Client entity = optionalById.get();
            if (!entity.getState().equals(ClientState.INACTIVE)) {
                entity.setState(ClientState.INACTIVE);
                clientRepository.save(entity);
            } else {
                log.error(errorMessageClientInactive);
                throw new IllegalArgumentException(errorMessageClientInactive);
            }
        } else {
            log.error(errorMessage);
            throw new ResourceNotFoundException(errorMessage);
        }
    }

    @Override
    public void editClient(Integer clientId,
                           ClientRequestDto request,
                           DocumentType documentType,
                           CategoryAfip categoryAfip,
                           Locality locality) {

        Client nuevo = modelMapper.map(request, Client.class);
        nuevo.setDocumentType(documentType);
        nuevo.setCategoryAfip(categoryAfip);
        nuevo.setLocality(locality);
        nuevo.setState(ClientState.ACTIVE);
        nuevo.setCreationDate(getClientByID(clientId).getCreationDate());
        nuevo.setCheckingAccount(getClientByID(clientId).getCheckingAccount());

        nuevo.setId(clientId);
        clientRepository.save(nuevo);
    }

}
