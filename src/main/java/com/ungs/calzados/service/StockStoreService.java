package com.ungs.calzados.service;

import com.ungs.calzados.dto.StockStoreRequestDto;
import com.ungs.calzados.dto.StockStoreResponseDto;
import com.ungs.calzados.entity.StockStore;
import java.util.List;

public interface StockStoreService {
    List<StockStore> findByIdInList(Integer storeId, List<Integer> idList);

    StockStore save(StockStore stockStore);

    StockStore findStockStoreById(Integer storeId, Integer productId);

    void addStockStore(StockStoreRequestDto stockStore);

    void updateStockStore(StockStore stockStore, Integer quantity);

    List<StockStoreResponseDto> findByProductId(Integer productId);

    List<StockStoreResponseDto> findByStoreId(Integer storeId);
}
