package com.ungs.calzados.service;

import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.entity.AccountHistory;
import com.ungs.calzados.entity.CheckingAccount;
import com.ungs.calzados.repository.AccountHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;

@Service
@Slf4j
public class AccountHistoryServiceImpl implements AccountHistoryService {

    private final AccountHistoryRepository accountHistoryRepository;

    @Autowired
    public AccountHistoryServiceImpl(AccountHistoryRepository accountHistoryRepository) {
        this.accountHistoryRepository = accountHistoryRepository;
    }

    @Override
    public AccountHistory addAccountHistory(AccountHistory accountHistory, String type, CheckingAccount checkingAccountEntitySaved) {
        accountHistory.setDate(LocalDate.now());
        accountHistory.setCheckingAccountId(checkingAccountEntitySaved);

        if(type.equals("INPUT")){
            accountHistory.setType(MovementType.INPUT);
        }else{    accountHistory.setType(MovementType.OUTPUT);  }

        return accountHistoryRepository.save(accountHistory);
    }

}
