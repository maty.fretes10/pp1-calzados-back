package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.SupplierState;
import com.ungs.calzados.dto.SupplierRequestDto;
import com.ungs.calzados.dto.SupplierResponseDto;
import com.ungs.calzados.entity.*;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.MaterialSupplierFavsRepository;
import com.ungs.calzados.repository.ProductSupplierFavsRepository;
import com.ungs.calzados.repository.SupplierRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;
    private final ModelMapper modelMapper;
    private final ProductSupplierFavsRepository productSupplierFavsRepository;
    private final MaterialSupplierFavsRepository materialSupplierFavsRepository;

    @Autowired
    public SupplierServiceImpl(SupplierRepository supplierRepository,
                               ModelMapper modelMapper,
                               ProductSupplierFavsRepository productSupplierFavsRepository,
                               MaterialSupplierFavsRepository materialSupplierFavsRepository) {
        this.supplierRepository = supplierRepository;
        this.modelMapper = modelMapper;
        this.productSupplierFavsRepository = productSupplierFavsRepository;
        this.materialSupplierFavsRepository = materialSupplierFavsRepository;
    }

    @Override
    public void addSupplier(SupplierRequestDto supplierRequestDto, CheckingAccount checkingAccount, Locality locality) {
      Supplier entity = modelMapper.map(supplierRequestDto, Supplier.class);
      entity.setState(SupplierState.ACTIVE);
      entity.setCheckingAccount(checkingAccount);
      entity.setLocality(locality);
      supplierRepository.save(entity);

    }

    @Override
    public List<SupplierResponseDto> getSuppliers() {
        List <Supplier> proveedoresLista = supplierRepository.findAll();
        if (!proveedoresLista.isEmpty()){
            return proveedoresLista.stream().parallel().map(supplier -> modelMapper.map(supplier,SupplierResponseDto.class)).collect(Collectors.toList());
        }
    log.warn(ErrorConstants.EMPTY_SUPPLIERS_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_SUPPLIERS_LIST);
    }

    @Override
    public void inactivateSupplier(Integer idProveedor){
        Optional <Supplier> proveedor = supplierRepository.findById(idProveedor);
            if (!proveedor.isPresent()) {
                log.warn(ErrorConstants.SUPPLIER_BY_ID_IS_NULL);
                throw new ResourceNotFoundException(ErrorConstants.SUPPLIER_BY_ID_IS_NULL);
            }

            Supplier entity = proveedor.get();
            if (entity.getState().equals(SupplierState.INACTIVE)){
                log.warn(ErrorConstants.SUPPLIER_BY_STATE_INACTIVE_EXISTS);
                throw new ResourceNotFoundException(ErrorConstants.SUPPLIER_BY_STATE_INACTIVE_EXISTS);
            }

            List<Optional<ProductSupplierFavs>> favsProducts =
                    productSupplierFavsRepository.findAllBySupplierIdOpcionalList(idProveedor);

           List< Optional<MaterialSupplierFavs>> favsMaterials =
                   materialSupplierFavsRepository.findAllBySupplierIdOpcionalList(idProveedor);


            if (!favsProducts.isEmpty()){
                List<ProductSupplierFavs> fav= productSupplierFavsRepository.findAllBySupplierId(idProveedor);
                int largo = fav.size();
                for (int x=0; x<largo; x++){
                         productSupplierFavsRepository.delete(fav.get(x));
                }}

            if (!favsMaterials.isEmpty()){
                List<MaterialSupplierFavs> fav = materialSupplierFavsRepository.findAllBySupplierId(idProveedor);
                int largo = fav.size();
                for (int i=0; i<largo; i++){
                    materialSupplierFavsRepository.delete(fav.get(i));
                }
            }

                entity.setState(SupplierState.INACTIVE);
                supplierRepository.save(entity);
    }

    @Override
    public Supplier getSupplierByID(Integer id){
        final Optional<Supplier> optionalSupplier = this.supplierRepository.findById(id);
        if (!optionalSupplier.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.SUPPLIER_BY_ID_NOT_FOUND);

        return optionalSupplier.get();
    }

    @Override
    public void editSupplier(Integer supplierId, SupplierRequestDto request, Locality locality) {

        Supplier nuevo = modelMapper.map(request, Supplier.class);
        nuevo.setLocality(locality);
        nuevo.setCheckingAccount(getSupplierByID(supplierId).getCheckingAccount());
        nuevo.setState(SupplierState.ACTIVE);


        nuevo.setId(supplierId);
        supplierRepository.save(nuevo);

    }
}



