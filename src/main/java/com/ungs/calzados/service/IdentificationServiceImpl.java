package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.CategoryAfipResponseDto;
import com.ungs.calzados.dto.DocumentTypeResponseDto;
import com.ungs.calzados.entity.CategoryAfip;
import com.ungs.calzados.entity.DocumentType;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.CategoryAfipRepository;
import com.ungs.calzados.repository.DocumentTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class IdentificationServiceImpl implements IdentificationService {
    private final DocumentTypeRepository documentTypeRepository;
    private final CategoryAfipRepository categoryAfipRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public IdentificationServiceImpl(
            DocumentTypeRepository documentTypeRepository,
            CategoryAfipRepository categoryAfipRepository,
            ModelMapper modelMapper) {

        this.documentTypeRepository = documentTypeRepository;
        this.categoryAfipRepository = categoryAfipRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<DocumentTypeResponseDto> getDocumentTypes() {
        List<DocumentType> allDocumentTypes = documentTypeRepository.findAll();

        if (!allDocumentTypes.isEmpty()) {
            log.info("Document type found count: {}", allDocumentTypes.size());

            return allDocumentTypes.stream()
                    .parallel()
                    .map(id -> modelMapper.map(id, DocumentTypeResponseDto.class))
                    .collect(Collectors.toList());
        }

        throw new NoDataFoundException();
    }

    @Override
    public DocumentType getDocumentTypeEntity(Integer documentTypeId) {
        Optional<DocumentType> documentTypeOptional = documentTypeRepository.findById(documentTypeId);
        String errorMessage = String.format(ErrorConstants.DOCUMENT_TYPE_BY_ID_NOT_FOUND, documentTypeId);

        if (documentTypeOptional.isPresent()) {
            DocumentType documentTypeFound = documentTypeOptional.get();
            log.info("Document type with id {} was found: {}", documentTypeId, documentTypeFound);
            return documentTypeFound;
        }

        log.error(errorMessage);
        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public CategoryAfip getCategoryAfipById(Integer categoryAfipId) {
        String errorMessage = String.format(ErrorConstants.CATEGORY_AFIP_BY_ID_NOT_FOUND, categoryAfipId);
        Optional<CategoryAfip> categoryAfipOptional = categoryAfipRepository.findById(categoryAfipId);

        if (categoryAfipOptional.isPresent()) {
            return categoryAfipOptional.get();
        }

        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public List<CategoryAfipResponseDto> getCategoriesAfip() {
        List<CategoryAfip> allCategoryAfip = categoryAfipRepository.findAll();

        if (!allCategoryAfip.isEmpty()) {
            log.info("Document type found count: {}", allCategoryAfip.size());

            return allCategoryAfip.stream()
                    .parallel()
                    .map(id -> modelMapper.map(id, CategoryAfipResponseDto.class))
                    .collect(Collectors.toList());
        }

        throw new NoDataFoundException();
    }

}
