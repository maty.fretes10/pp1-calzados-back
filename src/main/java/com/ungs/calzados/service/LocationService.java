package com.ungs.calzados.service;

import com.ungs.calzados.dto.LocalityResponseDto;
import com.ungs.calzados.dto.ProvinceResponseDto;
import com.ungs.calzados.entity.Locality;


import java.util.List;

public interface LocationService {
    List<ProvinceResponseDto> getAllProvinces();

    List<LocalityResponseDto> getAllLocalitiesByProvinceId(Integer provinceId);

    Locality getLocalityById(Integer localityId);
    LocalityResponseDto getLocalityByIdDto(Integer localityId);
}
