package com.ungs.calzados.service.production;


import com.ungs.calzados.dto.production.ProductionOrderResponseDto;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.Waist;
import com.ungs.calzados.entity.production.ProductionModel;
import com.ungs.calzados.entity.production.ProductionOrder;

import java.util.List;

public interface ProductionOrderService {

    ProductionOrder addProductionOrder(ProductionOrder productionOrder,
                                       Colour colour,
                                       ProductionModel productModel,
                                       Waist waist,
                                       Store store);

    List<ProductionOrderResponseDto> getOrderResponse();

    ProductionOrder getPOById(Integer id);

    void editProductionOrder(ProductionOrder productionOrder,
                                       Colour colour,
                                       Waist waist);

    List<ProductionOrder> getOrder();
}
