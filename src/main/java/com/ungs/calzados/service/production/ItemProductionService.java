package com.ungs.calzados.service.production;

import com.ungs.calzados.entity.production.ItemProduction;
import java.util.List;

public interface ItemProductionService {
    void addAllItemMaterial(List<ItemProduction> productionOrderMaterialList);

    void deleteAllItemMaterial(Integer idProductionModel);
}
