package com.ungs.calzados.service.production;

import com.ungs.calzados.dto.production.POStateRequestDto;
import com.ungs.calzados.dto.production.POStateResponseDto;
import com.ungs.calzados.entity.production.ProductionOrder;
import com.ungs.calzados.entity.production.ProductionOrderState;

import java.util.List;

public interface POStateService {
    void addPOState(ProductionOrder productionOrder);

    void editPOState(POStateRequestDto poStateRequestDto);

    ProductionOrderState getPOStateByIdOrder(Integer POStateId);

    List<POStateResponseDto> getPOstate();
}
