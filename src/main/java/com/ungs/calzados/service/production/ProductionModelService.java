package com.ungs.calzados.service.production;

import com.ungs.calzados.dto.production.ProductionModelResponseDto;
import com.ungs.calzados.entity.production.ProductionModel;

import java.util.List;

public interface ProductionModelService {
    ProductionModel addProductionModel(ProductionModel productModel);

    List<ProductionModelResponseDto> getProductModels();

    ProductionModel getProductModelById(Integer id);

    void inactivateProductionModel(Integer idProductionModel);
}
