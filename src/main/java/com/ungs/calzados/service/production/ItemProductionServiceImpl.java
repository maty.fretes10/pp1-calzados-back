package com.ungs.calzados.service.production;

import com.ungs.calzados.entity.production.ItemProduction;
import com.ungs.calzados.repository.production.ItemProductionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemProductionServiceImpl implements ItemProductionService {

    private final ItemProductionRepository itemRepositoryMaterial;

    @Autowired
    public ItemProductionServiceImpl(ItemProductionRepository productionOrderRepositoryMaterial) {
        this.itemRepositoryMaterial = productionOrderRepositoryMaterial;
    }

    @Override
    public void addAllItemMaterial(List<ItemProduction> productionOrderMaterialList) {
        itemRepositoryMaterial.saveAll(productionOrderMaterialList);
    }

    @Override
    public void deleteAllItemMaterial(Integer idProductionModel) {
        List<ItemProduction> itemProductionList = itemRepositoryMaterial.findItemProductionByProductionModelId(idProductionModel);
        itemRepositoryMaterial.deleteAll(itemProductionList);
    }
}
