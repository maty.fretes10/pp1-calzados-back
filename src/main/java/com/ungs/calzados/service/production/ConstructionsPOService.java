package com.ungs.calzados.service.production;

import com.ungs.calzados.dto.PurchaseOrderItemRequestDto;
import com.ungs.calzados.dto.PurchaseOrderRequestDto;
import com.ungs.calzados.dto.production.ProductionOrderEditRequestDto;
import com.ungs.calzados.dto.production.ProductionOrderRequestDto;
import com.ungs.calzados.dto.production.ProductionOrderSavedResponseDto;
import com.ungs.calzados.entity.Material;
import com.ungs.calzados.entity.MaterialSupplierFavs;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.Waist;
import com.ungs.calzados.entity.production.ProductionModel;
import com.ungs.calzados.entity.production.ProductionOrder;
import com.ungs.calzados.entity.production.ItemProduction;
import com.ungs.calzados.entity.production.StockFactory;
import com.ungs.calzados.exception.*;
import com.ungs.calzados.repository.MaterialSupplierFavsRepository;
import com.ungs.calzados.service.MaterialService;
import com.ungs.calzados.service.ProductService;
import com.ungs.calzados.service.PurchaseOrderService;
import com.ungs.calzados.service.StoreService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ConstructionsPOService {

    private final StockFactoryService stockFactoryService;
    private final ProductionOrderService productionOrderService;
    private final ItemProductionService productionOrderMaterialService;
    private final MaterialService materialService;
    private final ProductService productService;
    private final POStateService poStateService;
    private final ProductionModelService productionModelService;
    private final StoreService storeService;
    private final PurchaseOrderService purchaseOrderService;
    private final MaterialSupplierFavsRepository materialSupplierFavsRepository;

    @Autowired
    public ConstructionsPOService(
            StockFactoryService stockFactoryService,
            ProductionOrderService productionOrderService,
            ItemProductionService productionOrderMaterialService,
            MaterialService materialService,
            ProductService productService,
            POStateService poStateService,
            ProductionModelService productionModelService,
            StoreService storeService,
            PurchaseOrderService purchaseOrderService,
            MaterialSupplierFavsRepository materialSupplierFavsRepository) {

        this.stockFactoryService = stockFactoryService;
        this.productionOrderService = productionOrderService;
        this.materialService = materialService;
        this.productionOrderMaterialService = productionOrderMaterialService;
        this.productService = productService;
        this.poStateService = poStateService;
        this.productionModelService = productionModelService;
        this.storeService = storeService;
        this.purchaseOrderService = purchaseOrderService;
        this.materialSupplierFavsRepository = materialSupplierFavsRepository;
    }

    @Transactional
    public ProductionOrderSavedResponseDto addProductionOrder(ProductionOrderRequestDto productionOrderRequestDto) {
        ProductionModel productionModel = productionModelService.getProductModelById(productionOrderRequestDto.getModelProductionId());

        List<StockSingleErrorObject> errorList = new ArrayList<>();
        ProductionOrder productionOrder = ProductionOrder.builder().quantity(productionOrderRequestDto.getQuantity()).build();

        Colour colourEntity = productService.getColourById(productionOrderRequestDto.getColourProductionId());
        Waist waist = productService.getWaistById(productionOrderRequestDto.getWaistProductionId());
        Store store = storeService.getStoreById(productionOrderRequestDto.getStoreId());

        List<Integer> materialsRequiredIdList = new ArrayList<>();
        for (ItemProduction itemProduction: productionModel.getMaterials()) {
            materialsRequiredIdList.add(itemProduction.getMaterial().getId());
        }
        List<StockFactory> availableMaterialsInStock =
                stockFactoryService.findByIdInList(productionOrderRequestDto.getStoreId(), materialsRequiredIdList);

        availableMaterialsInStock.stream().forEach(availableMaterialInStock -> {
            ItemProduction itemInStock = productionModel
                    .getMaterials()
                    .stream()
                    .filter(material -> material.getMaterial().equals(availableMaterialInStock.getMaterial()))
                    .findFirst().get();

            int totalQuantity = itemInStock.getQuantity()*productionOrderRequestDto.getQuantity();
            if (totalQuantity <= availableMaterialInStock.getTotalStock()) {
                try {
                    updateStockInStore(availableMaterialInStock, totalQuantity);
                } catch (JRException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                updateStockInMaterials(itemInStock.getMaterial().getId(), totalQuantity);
            } else {
                errorList.add(StockSingleErrorObject.builder()
                        .productId(availableMaterialInStock.getId())
                        .availableStock(availableMaterialInStock.getTotalStock())
                        .requiredStock(totalQuantity)
                        .build());
            }
        });

        if (!errorList.isEmpty()) {
            throw new NotEnoughStockException(StockErrorObject.builder().errors(errorList).build());
        }
        ProductionOrder productionOrder1 = productionOrderService.addProductionOrder(productionOrder, colourEntity, productionModel, waist, store);
        poStateService.addPOState(productionOrder1);
        return ProductionOrderSavedResponseDto.builder().ProductionOrderId(productionOrder1.getId()).build();
    }

    private void updateStockInMaterials(Integer materialId, Integer requiredStock) {
        materialService.updateStockMaterial(materialId, requiredStock);
    }

    private void updateStockInStore(StockFactory availableMaterialInStock, Integer materialStockRequest) throws JRException, FileNotFoundException {
        int newStockValue = availableMaterialInStock.getTotalStock() - materialStockRequest;
        availableMaterialInStock.setTotalStock(newStockValue);

        Material material = materialService.getMaterialById(availableMaterialInStock.getMaterial().getId());
        if(newStockValue <= material.getStockMin() ){
            Optional<MaterialSupplierFavs> materialSupplierFavs = materialSupplierFavsRepository.findByMaterialsIdOpcional(material.getId());
            if(materialSupplierFavs.isPresent()){
                PurchaseOrderRequestDto orderRequestDto = new PurchaseOrderRequestDto();
                orderRequestDto.setStoreId(availableMaterialInStock.getStore().getId());
                orderRequestDto.setSupplierId(materialSupplierFavs.get().getSupplier().getId());
                PurchaseOrderItemRequestDto itemRequestDto = new PurchaseOrderItemRequestDto();
                itemRequestDto.setDetail( material.getName() + " " + material.getDescription());
                itemRequestDto.setQuantity(material.getStockMin()*2);
                itemRequestDto.setUnitPrice(material.getPriceBuy());
                List<PurchaseOrderItemRequestDto> list = new ArrayList<>();
                list.add(itemRequestDto);
                orderRequestDto.setItems(list);
                purchaseOrderService.addPurchaseOrder(orderRequestDto);
            }
        }
        stockFactoryService.save(availableMaterialInStock);
    }

    @Transactional
    public void editProductionOrder(ProductionOrderEditRequestDto productionOrderEditRequestDto) {
        Colour colourEntity = productService.getColourById(productionOrderEditRequestDto.getColourProductionId());
        Waist waist = productService.getWaistById(productionOrderEditRequestDto.getWaistProductionId());
        ProductionOrder productionOrder = productionOrderService.getPOById(productionOrderEditRequestDto.getProductionOrderId());

        productionOrderService.editProductionOrder(productionOrder, colourEntity, waist);
    }
}
