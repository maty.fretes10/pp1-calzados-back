package com.ungs.calzados.service.production;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.production.ProductionOrderResponseDto;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.Waist;
import com.ungs.calzados.entity.production.ProductionModel;
import com.ungs.calzados.entity.production.ProductionOrder;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.production.ProductionOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductionOrderServiceImpl implements ProductionOrderService{

    private final ProductionOrderRepository productionOrderRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ProductionOrderServiceImpl (ProductionOrderRepository productionOrderRepository,
                                       ModelMapper modelMapper){
        this.productionOrderRepository = productionOrderRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public ProductionOrder addProductionOrder(ProductionOrder productionOrder,
                                              Colour colour,
                                              ProductionModel productModel,
                                              Waist waist,
                                              Store store) {
        productionOrder.setColourProduction(colour);
        productionOrder.setModelProduction(productModel);
        productionOrder.setWaistProduction(waist);
        productionOrder.setModificationDate(LocalDate.now());

        productionOrder.setStore(store);

        return productionOrderRepository.save(productionOrder);
    }

    @Override
    public List<ProductionOrderResponseDto> getOrderResponse() {
        List<ProductionOrder> allorder = (List<ProductionOrder>) productionOrderRepository.findAll();

        if (!allorder.isEmpty()) {
            return allorder.stream()
                    .parallel()
                    .map(ord -> modelMapper.map(ord, ProductionOrderResponseDto.class))
                    .collect(Collectors.toList());
        }

        log.warn(ErrorConstants.EMPTY_ORDER_PRODUCTION_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_ORDER_PRODUCTION_LIST);
    }

    @Override
    public ProductionOrder getPOById(Integer id) {
        final Optional<ProductionOrder> optionalPO = this.productionOrderRepository.findById(id);
        if (!optionalPO.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.PRODUCTION_ORDER_BY_ID_NOT_FOUND);

        return optionalPO.get();
    }

    @Override
    public void editProductionOrder(ProductionOrder productionOrder, Colour colour, Waist waist) {
        productionOrder.setColourProduction(colour);
        productionOrder.setWaistProduction(waist);
        productionOrder.setModificationDate(LocalDate.now());

        productionOrderRepository.save(productionOrder);
    }

    @Override
    public List<ProductionOrder> getOrder() {
        return (List<ProductionOrder>) productionOrderRepository.findAll();
    }
}
