package com.ungs.calzados.service.production;

import com.ungs.calzados.constants.ModelState;
import com.ungs.calzados.dto.production.ItemProductionRequestDto;
import com.ungs.calzados.dto.production.ProductionModelRequestDto;
import com.ungs.calzados.entity.product.ProductModel;
import com.ungs.calzados.entity.production.ItemProduction;
import com.ungs.calzados.entity.production.ProductionModel;
import com.ungs.calzados.service.MaterialService;
import com.ungs.calzados.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ConstructionsModelProductionService {

    private final ProductionModelService productionModelService;
    private final ProductService productService;
    private final ModelMapper modelMapper;
    private final ItemProductionService itemProductionService;
    private final MaterialService materialService;

    public ConstructionsModelProductionService(ProductionModelService productionModelService,
                                               ProductService productService,
                                               ModelMapper modelMapper,
                                               ItemProductionService itemProductionService,
                                               MaterialService materialService) {
        this.productionModelService = productionModelService;
        this.productService = productService;
        this.modelMapper = modelMapper;
        this.itemProductionService = itemProductionService;
        this.materialService = materialService;
    }

    @Transactional
    public void constructionsSaveProductionModel(ProductionModelRequestDto productionModelRequest) {
        ProductModel productModel = productService.getProductModelById(productionModelRequest.getModelId());
        ProductionModel model = ProductionModel.builder().modelProduct(productModel).build();
        List<ItemProduction> ItemProductionList = new ArrayList<>();
        productionModelService.addProductionModel(model);

        for (ItemProductionRequestDto item: productionModelRequest.getMaterials()) {
            ItemProduction entity = modelMapper.map(item, ItemProduction.class);
            entity.setMaterial(materialService.getMaterialById(item.getMaterialId()));
            entity.setQuantity(item.getRequiredStock());
            entity.setProductionModel(model);

            ItemProductionList.add(entity);
        }

        model.setState(ModelState.ACTIVE);
        model.setMaterials(ItemProductionList);
        itemProductionService.addAllItemMaterial(ItemProductionList);
    }

    @Transactional
    public void constructionsEditProductionModel(ProductionModelRequestDto productionModelRequest, Integer idProductionModel) {
        ProductModel productModel = productService.getProductModelById(productionModelRequest.getModelId());
        ProductionModel model = productionModelService.getProductModelById(idProductionModel);
        model.setModelProduct(productModel);

        itemProductionService.deleteAllItemMaterial(model.getId());
        productionModelService.addProductionModel(model);

        List<ItemProduction> ItemProductionList = new ArrayList<>();
        for (ItemProductionRequestDto item: productionModelRequest.getMaterials()) {
            ItemProduction entity = modelMapper.map(item, ItemProduction.class);
            entity.setMaterial(materialService.getMaterialById(item.getMaterialId()));
            entity.setQuantity(item.getRequiredStock());
            entity.setProductionModel(model);

            ItemProductionList.add(entity);
        }

        model.setMaterials(ItemProductionList);
        itemProductionService.addAllItemMaterial(ItemProductionList);
    }
}
