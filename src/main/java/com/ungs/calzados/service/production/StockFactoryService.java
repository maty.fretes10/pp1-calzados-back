package com.ungs.calzados.service.production;

import com.ungs.calzados.entity.production.StockFactory;

import java.util.List;

public interface StockFactoryService {
    List<StockFactory> findByIdInList(Integer storeId, List<Integer> idList);
    StockFactory save(StockFactory stockStore);
    StockFactory findStockFactoryById(Integer storeId, Integer materialId);
}
