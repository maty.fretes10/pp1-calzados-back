package com.ungs.calzados.service.production;

import com.ungs.calzados.entity.production.StockFactory;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.production.StockFactoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class StockFactoryServiceImpl implements StockFactoryService {

    private StockFactoryRepository stockFactoryRepository;

    @Autowired
    public StockFactoryServiceImpl(StockFactoryRepository stockFactoryRepository) {
        this.stockFactoryRepository = stockFactoryRepository;

    }

    @Override
    public List<StockFactory> findByIdInList(Integer storeId, List<Integer> idList) {
        return stockFactoryRepository.findByIdInList(storeId, idList);
    }

    @Override
    public StockFactory save(StockFactory stockFactory) {
        return stockFactoryRepository.save(stockFactory);
    }

    @Override
    public StockFactory findStockFactoryById(Integer storeId, Integer materialId) {
        final Optional<StockFactory> optionalStockFactory = this.stockFactoryRepository.findByStoreIdAndMaterialId(storeId, materialId);
        if (!optionalStockFactory.isPresent())
            throw new ResourceNotFoundException("Registro de stock factory no encontrado");

        return optionalStockFactory.get();
    }

}
