package com.ungs.calzados.service.production;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.ModelState;
import com.ungs.calzados.dto.production.ProductionModelResponseDto;
import com.ungs.calzados.entity.production.ProductionModel;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.production.ModelProductionRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductionModelServiceImpl implements  ProductionModelService{

    ModelProductionRepository modelProductionRepository;
    ModelMapper modelMapper;

    @Autowired
    public ProductionModelServiceImpl(ModelProductionRepository modelProductionRepository,
                                      ModelMapper modelMapper){
        this.modelProductionRepository = modelProductionRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public ProductionModel addProductionModel(ProductionModel productModel) {
        return modelProductionRepository.save(productModel);
    }

    @Override
    public List<ProductionModelResponseDto> getProductModels() {
        List<ProductionModel> allProductModel = (List<ProductionModel>) modelProductionRepository.findAll();
        List<ProductionModel> retorno = new ArrayList<>();
        for (ProductionModel item : allProductModel) {
            if(item.getState().equals(ModelState.ACTIVE)){
                retorno.add(item);
            }
        }
        if (retorno.isEmpty()) {
            log.warn(ErrorConstants.EMPTY_PRODUCTION_MODEL_LIST);
            throw new NoDataFoundException(ErrorConstants.EMPTY_PRODUCTION_MODEL_LIST);
        }
        return retorno.stream()
                .parallel()
                .map(model -> modelMapper.map(model, ProductionModelResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ProductionModel getProductModelById(Integer id) {
        final Optional<ProductionModel> optionalModel = this.modelProductionRepository.findById(id);
        if (!optionalModel.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.PRODUCTION_MODEL_BY_ID_NOT_FOUND);

        return optionalModel.get();
    }

    @Override
    public void inactivateProductionModel(Integer idProductionModel) {
        ProductionModel model = getProductModelById(idProductionModel);
        if(model.getState().equals(ModelState.DISCONTINUED)){
            throw new IllegalArgumentException("El modelo de produccion ya esta inactivo");
        }
        model.setState(ModelState.DISCONTINUED);
        modelProductionRepository.save(model);
    }
}
