package com.ungs.calzados.service.production;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.production.POHistoryResponseDto;
import com.ungs.calzados.entity.production.ProductionOrder;
import com.ungs.calzados.entity.production.ProductionOrderHistory;
import com.ungs.calzados.entity.production.ProductionOrderState;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.repository.production.ProductionOrderHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class POHistoryServiceImpl implements POHistoryService{

    private final ProductionOrderHistoryRepository productionOrderHistoryRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public POHistoryServiceImpl (ProductionOrderHistoryRepository productionOrderHistoryRepository,
                                 ModelMapper modelMapper){
        this.productionOrderHistoryRepository = productionOrderHistoryRepository;
        this.modelMapper=modelMapper;
    }

    @Override
    public void addPOHistory(ProductionOrderHistory productionOrderHistory,
                             ProductionOrder productionOrder,
                             String newState,
                             ProductionOrderState productionOrderState) {

        productionOrderHistory.setProductionOrderId(productionOrder);
        productionOrderHistory.setModificationDate(LocalDate.now());
        productionOrderHistory.setDescription(newState);

        productionOrderHistoryRepository.save(productionOrderHistory);
    }

    @Override
    public List<POHistoryResponseDto> getPOHystoryByOrder(Integer id) {
        List<ProductionOrderHistory> allPOHistoryByOrder = productionOrderHistoryRepository.findAllByOrderId(id);

        if (!allPOHistoryByOrder.isEmpty()) {
            return allPOHistoryByOrder.stream()
                    .parallel()
                    .map(order -> modelMapper.map(order, POHistoryResponseDto.class))
                    .collect(Collectors.toList());
        }

        log.warn(ErrorConstants.EMPTY_PRODUCTION_HISTORY_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_PRODUCTION_HISTORY_LIST);
    }


}
