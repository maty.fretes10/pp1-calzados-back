package com.ungs.calzados.service.production;

import com.ungs.calzados.dto.production.POHistoryResponseDto;
import com.ungs.calzados.entity.production.ProductionOrder;
import com.ungs.calzados.entity.production.ProductionOrderHistory;
import com.ungs.calzados.entity.production.ProductionOrderState;

import java.util.List;

public interface POHistoryService {
    void addPOHistory(ProductionOrderHistory productionOrderHistory,
                      ProductionOrder productionOrder,
                      String newState,
                      ProductionOrderState productionOrderState);

    List<POHistoryResponseDto> getPOHystoryByOrder(Integer id);
}
