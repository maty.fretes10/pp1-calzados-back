package com.ungs.calzados.service.production;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.ProductionPhase;
import com.ungs.calzados.constants.State;
import com.ungs.calzados.dto.production.POStateRequestDto;
import com.ungs.calzados.dto.production.POStateResponseDto;
import com.ungs.calzados.entity.StockStore;
import com.ungs.calzados.entity.product.Product;
import com.ungs.calzados.entity.product.ProductModel;
import com.ungs.calzados.entity.production.*;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.production.ProductionOrderRepositoryState;
import com.ungs.calzados.service.MaterialService;
import com.ungs.calzados.service.ProductService;
import com.ungs.calzados.service.StockStoreService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class POStateServiceImpl implements  POStateService{

    private final ProductionOrderRepositoryState productionOrderRepositoryState;
    private final ModelMapper modelMapper;
    private final POHistoryService poHistoryService;
    private final ProductionOrderService productionOrderService;
    private final ProductService productService;
    private final MaterialService materialService;
    private final StockFactoryService stockFactoryService;
    private final StockStoreService stockStoreService;

    @Autowired
    public POStateServiceImpl (ProductionOrderRepositoryState productionOrderRepositoryState,
                               ModelMapper modelMapper,
                               POHistoryService poHistoryService,
                               ProductionOrderService productionOrderService,
                               ProductService productService,
                               MaterialService materialService,
                               StockFactoryService stockFactoryService,
                               StockStoreService stockStoreService){
        this.productionOrderRepositoryState = productionOrderRepositoryState;
        this.modelMapper = modelMapper;
        this.poHistoryService = poHistoryService;
        this.productionOrderService = productionOrderService;
        this.productService = productService;
        this.materialService = materialService;
        this.stockFactoryService = stockFactoryService;
        this.stockStoreService = stockStoreService;
    }

    @Override
    public void addPOState(ProductionOrder productionOrder) {
        ProductionOrderState entity = new ProductionOrderState();
        entity.setDescription("CORTADO");
        entity.setDescription(ProductionPhase.INICIADO.toString());
        entity.setProductionOrderId(productionOrder);
        productionOrderRepositoryState.save(entity);

        ProductionOrderHistory history = new ProductionOrderHistory();
        poHistoryService.addPOHistory(history, productionOrder, entity.getDescription(),entity);
    }

    @Override
    public void editPOState(POStateRequestDto poStateRequestDto) {
        ProductionOrderState entity = this.getPOStateByIdOrder(poStateRequestDto.getProductionOrderId());

        if(entity.getDescription().equals(ProductionPhase.CANCELADO.toString())){
            String message = "La orden de produccion fue cancelada. Por favor cree otra orden de produccion";
            log.info(message);
            throw new IllegalStateException(message);
        }

        if(entity.getDescription().equals(ProductionPhase.TERMINADO.toString())){
            throw new IllegalStateException("La orden ya fue finalizada");
        }

        ProductionOrder productionOrder = productionOrderService.getPOById(poStateRequestDto.getProductionOrderId());
        String desc = "PASA DE " + entity.getDescription() + " A " + poStateRequestDto.getDescription();

        if(poStateRequestDto.getDescription().equals(ProductionPhase.TERMINADO.toString())) {
            Optional<Product> productOptional = productService.getProductByModelColorWaist(productionOrder.getModelProduction().getModelProduct().getId(), productionOrder.getColourProduction().getId(), productionOrder.getWaistProduction().getId());

            if(productOptional.isPresent()){
                Product product = productOptional.get();
                log.info("El producto ya existe, actualizando stock de: " + product.getDescription());
                productService.updateStockManufacturedProduct(product.getId(), productionOrder.getQuantity());

                StockStore stockStore = stockStoreService.findStockStoreById(productionOrder.getStore().getId(),product.getId());
                stockStoreService.updateStockStore(stockStore, productionOrder.getQuantity());
            }
            else {
                log.info("El producto no existe, creando un nuevo producto");
                Product newProduct = new Product();
                ProductModel model = productionOrder.getModelProduction().getModelProduct();
                addNewProduct(productionOrder, newProduct, model);
            }
        }

        if(poStateRequestDto.getDescription().equals(ProductionPhase.CANCELADO.toString())) {
            checkingReturnStockMaterialsByState(poStateRequestDto, entity, productionOrder);
        }
        entity.setDescription(poStateRequestDto.getDescription());
        productionOrderRepositoryState.save(entity);
        ProductionOrderHistory history = new ProductionOrderHistory();
        poHistoryService.addPOHistory(history, productionOrder, desc,entity);
    }

    @Override
    public ProductionOrderState getPOStateByIdOrder(Integer POStateId) {
        final Optional<ProductionOrderState> optionalPOState = this.productionOrderRepositoryState.findByIdOrder(POStateId);
        if (!optionalPOState.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.PRODUCTION_ORDER_BY_ID_NOT_FOUND);

        return optionalPOState.get();
    }

    @Override
    public List<POStateResponseDto> getPOstate() {
        List<ProductionOrderState> allPOState = productionOrderRepositoryState.findAll();

        if (!allPOState.isEmpty()) {
            return allPOState.stream()
                    .parallel()
                    .map(pOstate -> modelMapper.map(pOstate, POStateResponseDto.class))
                    .collect(Collectors.toList());
        }

        log.warn(ErrorConstants.EMPTY_ORDER_PRODUCTION_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_ORDER_PRODUCTION_LIST);
    }

    private void addNewProduct(ProductionOrder productionOrder, Product newProduct, ProductModel model) {
        float priceTotal = 0;
        List<ItemProduction> listMaterials = productionOrder.getModelProduction().getMaterials();

        for (ItemProduction mat: listMaterials) {
            priceTotal += mat.getMaterial().getPriceBuy() * mat.getQuantity();
        }

        newProduct.setDescription(model.getName()+ " " + model.getMark());
        newProduct.setStockConsolidated(productionOrder.getQuantity());
        newProduct.setCreationDate(LocalDate.now());
        newProduct.setYearFabrication(LocalDate.now().getYear());
        newProduct.setPriceBuy((float) (priceTotal + (priceTotal*0.2)));
        newProduct.setPriceSale( newProduct.getPriceBuy()*2 );
        newProduct.setWaist(productionOrder.getWaistProduction());
        newProduct.setColour(productionOrder.getColourProduction());
        newProduct.setProductModel(model);
        newProduct.setStockSecurity(productionOrder.getQuantity() - 1);
        newProduct.setStockMinimum(productionOrder.getQuantity() - 1);
        newProduct.setProductStatus(productService.getProductStatusById(1));
        newProduct.setState(State.ACTIVE);

        productService.addManufacturedProduct(newProduct, productionOrder.getStore().getId());
    }

    private void stockReturn(ProductionOrder productionOrder, List<ItemProduction> listMaterials, ProductionPhase phase) {
        StockFactory stockFactory;
        for (ItemProduction mat: listMaterials) {
            if(mat.getPhase().equals(phase)){
                int idMaterial = mat.getMaterial().getId();
                int stockMaterial = mat.getQuantity() * productionOrder.getQuantity();
                returnStockInMaterials(idMaterial, stockMaterial);
                stockFactory = stockFactoryService.findStockFactoryById(productionOrder.getStore().getId(), idMaterial);
                returnStockInStore(stockFactory, stockMaterial);
            }
        }
    }

    private void returnStockInMaterials(Integer materialId, Integer requiredStock) {
        materialService.returnStockMaterial(materialId, requiredStock);
    }

    private void returnStockInStore(StockFactory availableMaterialInStock, Integer materialStockRequest) {
        int newStockValue = availableMaterialInStock.getTotalStock() + materialStockRequest;
        availableMaterialInStock.setTotalStock(newStockValue);
        stockFactoryService.save(availableMaterialInStock);
    }

    private void checkingReturnStockMaterialsByState(POStateRequestDto poStateRequestDto, ProductionOrderState entity, ProductionOrder productionOrder) {
        log.info("Orden de produccion con id: " + poStateRequestDto.getProductionOrderId()+ " fue CANCELADO");
        log.info("Devolver el stock de productos en PEGADO");
        List<ItemProduction> listMaterials = productionOrder.getModelProduction().getMaterials();

        stockReturn(productionOrder, listMaterials, ProductionPhase.PEGADO);

        if(entity.getDescription().equals(ProductionPhase.PREPARACION.toString())){
            log.info("Se cancelo en PREPARACION, Devolver el stock de productos en MONTADO");
            stockReturn(productionOrder, listMaterials, ProductionPhase.MONTADO);
        }
        else if(entity.getDescription().equals(ProductionPhase.APARADO.toString())){
            log.info("Se cancelo en APARADO, Devolver el stock de productos en PREPARACION Y MONTADO ");
            stockReturn(productionOrder, listMaterials, ProductionPhase.MONTADO);
            stockReturn(productionOrder, listMaterials, ProductionPhase.PREPARACION);
        }
        else if(entity.getDescription().equals(ProductionPhase.CORTADO.toString())){
            log.info("Se cancelo en CORTADO, Devolver el stock de productos en APARADO, PREPARACION Y MONTADO");
            stockReturn(productionOrder, listMaterials, ProductionPhase.MONTADO);
            stockReturn(productionOrder, listMaterials, ProductionPhase.PREPARACION);
            stockReturn(productionOrder, listMaterials, ProductionPhase.APARADO);
        }
        else if(entity.getDescription().equals(ProductionPhase.INICIADO.toString())){
            log.info("Se cancelo en INICIADO, Devolver el stock de productos en CORTADO, APARADO, PREPARACION Y MONTADO");
            stockReturn(productionOrder, listMaterials, ProductionPhase.MONTADO);
            stockReturn(productionOrder, listMaterials, ProductionPhase.PREPARACION);
            stockReturn(productionOrder, listMaterials, ProductionPhase.APARADO);
            stockReturn(productionOrder, listMaterials, ProductionPhase.CORTADO);
        }
    }
}
