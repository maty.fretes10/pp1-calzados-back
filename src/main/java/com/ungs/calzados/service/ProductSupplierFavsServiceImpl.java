package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.ProductSupplierFavsRequestDto;
import com.ungs.calzados.dto.ProductSupplierFavsResponseDto;
import com.ungs.calzados.entity.MaterialSupplierFavs;
import com.ungs.calzados.entity.ProductSupplierFavs;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.repository.ProductSupplierFavsRepository;
import lombok.extern.slf4j.Slf4j;
import org.jfree.util.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductSupplierFavsServiceImpl implements ProductSupplierFavsService{

    private final ProductSupplierFavsRepository productSupplierFavsRepository;
    private final ModelMapper modelMapper;
    private final SupplierService supplierService;
    private final ProductService productService;

    @Autowired
    public ProductSupplierFavsServiceImpl (ProductSupplierFavsRepository productSupplierFavsRepository,
                                           ModelMapper modelMapper,
                                           SupplierService supplierService,
                                           ProductService productService){

        this.productSupplierFavsRepository=productSupplierFavsRepository;
        this.modelMapper=modelMapper;
        this.supplierService=supplierService;
        this.productService=productService;
    }

    @Override
    public List<ProductSupplierFavsResponseDto> getProductSupplierFavById(Integer id, FavsType favsType) {
        Optional<Integer> optionalId = Optional.ofNullable(id);

        if (!optionalId.isPresent()) {
            throw new IllegalStateException("");
        }

        List<ProductSupplierFavsResponseDto> retorno = null;

        if (favsType.equals(FavsType.PRODUCT)) {
            List<ProductSupplierFavs> listByProductsId = productSupplierFavsRepository.findAllByProductsId(id);
            retorno = mapProductSupplierFavs(listByProductsId);
        }
        else if (favsType.equals(FavsType.SUPPLIER)){
            List<ProductSupplierFavs> listBySuppliersId = productSupplierFavsRepository.findAllBySupplierId(id);
            retorno = mapProductSupplierFavs(listBySuppliersId);
        }

        if (retorno.isEmpty()){
            log.warn("la lista de productSupplierFav esta vacia");
            throw new NoDataFoundException();
        }

        return retorno;
    }

    public List<ProductSupplierFavsResponseDto> mapProductSupplierFavs(List<ProductSupplierFavs> lista) {
        log.info("entré al método mapProductSupplierFavs");

        return lista.stream()
                .parallel()
                .map(productSupplierFavs -> modelMapper.map(productSupplierFavs, ProductSupplierFavsResponseDto.class))
                .collect(Collectors.toList());
    }


    @Override
    public void addFavSupplier(ProductSupplierFavsRequestDto productSupplierFavsRequestDto) {

        List<ProductSupplierFavs> lista = productSupplierFavsRepository.findAll();
        for (ProductSupplierFavs productId :lista) {
            if (productSupplierFavsRequestDto.getIdProduct() == productId.getProduct().getId()){
                String mensaje = String.format(ErrorConstants.PRODUCT_ID_ALREADY_HAS_FAV,
                        productId.getProduct().getId());
                log.warn(mensaje);
                throw new IllegalStateException(mensaje);
            }
        }


        ProductSupplierFavs entity = new ProductSupplierFavs();
        entity.setSupplier(supplierService.getSupplierByID(productSupplierFavsRequestDto.getIdSupplier()));
        entity.setProduct(productService.getProductById(productSupplierFavsRequestDto.getIdProduct()));
        productSupplierFavsRepository.save(entity);


    }


    @Override
    public void delFavProduct(Integer productId, Integer supplierId) {

        Optional<ProductSupplierFavs> entity = productSupplierFavsRepository
                .findByProductSupplierFavsOpcional(productId, supplierId);

        if (!entity.isPresent()) {

            String mensaje = String.format(ErrorConstants.FAV_RELATION_IS_NOT_SET, productId, supplierId);
            log.warn(mensaje);
            throw new IllegalStateException(mensaje);

        }

        productSupplierFavsRepository.delete(entity.get());

    }


    @Override
    public void replaceFavSupplier(ProductSupplierFavsRequestDto productSupplierFavsRequestDto) {

        Integer idProducto = productSupplierFavsRequestDto.getIdProduct();
        Integer idProveedor = productSupplierFavsRequestDto.getIdSupplier();

        Optional <ProductSupplierFavs> productSupplierFavs =
                productSupplierFavsRepository.findByProductIdOpcional(idProducto);

        if (!( productSupplierFavs.isPresent())) {
            String mensaje = String.format(ErrorConstants.PRODUCT_BY_ID_NOT_FOUND, idProducto);

            log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }

        ProductSupplierFavs entity = productSupplierFavs.get();

        entity.setSupplier(supplierService.getSupplierByID(idProveedor));

        productSupplierFavsRepository.save(entity);

    }



}

