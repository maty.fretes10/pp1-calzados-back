package com.ungs.calzados.service;

import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.dto.DailyRegisterResponseDto;
import com.ungs.calzados.dto.RegisterItemResponseDto;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.entity.RegisterItem;

import java.util.List;

public interface DailyRegisterService {

    void addDaily (Integer sucursalId);

    List<DailyRegisterResponseDto> listDailies(Integer sucursalId);

    void closeDaily(Integer sucursalId);

    DailyRegister getDailyOpenBySucursal(Integer sucursalId);

    void addBalance(DailyRegister dailyRegister, MovementType movementType, Double amout);


}
