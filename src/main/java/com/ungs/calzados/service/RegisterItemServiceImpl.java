package com.ungs.calzados.service;


import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.dto.RegisterItemResponseDto;
import com.ungs.calzados.dto.product.ProductResponseDto;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.entity.RegisterItem;
import com.ungs.calzados.entity.product.Product;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.repository.DailyRegisterRepository;
import com.ungs.calzados.repository.RegisterItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RegisterItemServiceImpl implements RegisterItemService{

    private final RegisterItemRepository registerItemRepository;
    private final DailyRegisterRepository dailyRegisterRepository;
    private final DailyRegisterService dailyRegisterService;
    private final ModelMapper modelMapper;

    public RegisterItemServiceImpl(RegisterItemRepository registerItemRepository,
                                   DailyRegisterRepository dailyRegisterRepository,
                                   DailyRegisterService dailyRegisterService,
                                   ModelMapper modelMapper) {
        this.registerItemRepository = registerItemRepository;
        this.dailyRegisterRepository = dailyRegisterRepository;
        this.dailyRegisterService = dailyRegisterService;
        this.modelMapper = modelMapper;
    }


    @Override
    public void addItem(String description, MovementType movementType, Double amout, DailyRegister dailyRegister) {
        RegisterItem item = new RegisterItem();
        LocalDateTime date = LocalDateTime.now();
        item.setDescription(description);
        item.setMovementType(movementType);
        item.setAmount(amout);
        item.setDate(date);
        item.setDailyRegister(dailyRegister);


        dailyRegisterService.addBalance(dailyRegister, movementType, amout);

        registerItemRepository.save(item);

    }

    public List<RegisterItemResponseDto> listItems(Integer sucursalId){

        Integer dailyId = dailyRegisterService.getDailyOpenBySucursal(sucursalId).getId();

        List<RegisterItem> itemList = registerItemRepository.findByRegisterId(dailyId);

        if (!itemList.isEmpty()){
            return itemList.stream().parallel().map(registerItem ->
                    modelMapper.map(registerItem, RegisterItemResponseDto.class)).collect(Collectors.toList());
        }
        throw new NoDataFoundException(ErrorConstants.EMPTY_ITEMS_LIST);
    }
}
