package com.ungs.calzados.service;

import com.ungs.calzados.dto.ClientRequestDto;
import com.ungs.calzados.dto.ClientResponseDto;
import com.ungs.calzados.entity.*;
import java.util.List;

public interface ClientService {
    List<ClientResponseDto> getClients();

    void addClient(
            ClientRequestDto clientRequest,
            CheckingAccount checkingAccountEntity,
            DocumentType documentTypeEntity,
            CategoryAfip categoryAfipEntity,
            Locality localityEntity);

    Client getClientByID(Integer id);

    ClientResponseDto getClientByIdResponse(Integer clienteId);

    void inactivateClient(Integer clientId);

    void editClient(
            Integer clientId,
            ClientRequestDto clientRequestDto,
            DocumentType documentType,
            CategoryAfip categoryAfip,
            Locality locality);

}