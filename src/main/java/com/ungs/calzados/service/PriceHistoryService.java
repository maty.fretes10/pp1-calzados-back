package com.ungs.calzados.service;

import com.ungs.calzados.entity.product.PriceHistory;
import com.ungs.calzados.entity.product.Product;

public interface PriceHistoryService {
    PriceHistory addPriceHistory(Product product);
}
