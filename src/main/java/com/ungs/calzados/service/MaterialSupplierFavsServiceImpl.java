package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.MaterialSupplierFavsRequestDto;
import com.ungs.calzados.dto.MaterialSupplierFavsResponseDto;
import com.ungs.calzados.entity.MaterialSupplierFavs;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.repository.MaterialSupplierFavsRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MaterialSupplierFavsServiceImpl implements MaterialSupplierFavsService{

    private final MaterialSupplierFavsRepository materialSupplierFavsRepository;
    private final ModelMapper modelMapper;
    private final SupplierService supplierService;
    private final MaterialService materialService;

    @Autowired
    public MaterialSupplierFavsServiceImpl (MaterialSupplierFavsRepository materialSupplierFavsRepository,
                                            ModelMapper modelMapper,
                                            SupplierService supplierService,
                                            MaterialService materialService){
        this.materialSupplierFavsRepository=materialSupplierFavsRepository;
        this.modelMapper=modelMapper;
        this.supplierService=supplierService;
        this.materialService=materialService;
    }


    @Override
    public List<MaterialSupplierFavsResponseDto> getMaterialSupplierFavById(Integer id, FavsType favsType) {
        Optional<Integer> optionalId = Optional.ofNullable(id);

        if (!optionalId.isPresent()) {
            throw new IllegalStateException("");
        }

        List<MaterialSupplierFavsResponseDto> retorno = null;

        if (favsType.equals(FavsType.MATERIAL)) {
            List<MaterialSupplierFavs> listByMaterialsId = materialSupplierFavsRepository.findAllByMaterialsId(id);
            retorno = mapMaterialSupplierFavs(listByMaterialsId);
        }
        else if (favsType.equals(FavsType.SUPPLIER)){
            List<MaterialSupplierFavs> listBySuppliersId = materialSupplierFavsRepository.findAllBySupplierId(id);
            retorno = mapMaterialSupplierFavs(listBySuppliersId);
        }


        if (retorno.isEmpty()){
            log.warn("la lista de materialSupplierFav esta vacia");
            throw new NoDataFoundException();
        }

        return retorno;
    }

    private List<MaterialSupplierFavsResponseDto> mapMaterialSupplierFavs(List<MaterialSupplierFavs> lista) {
        return lista.stream()
                .parallel()
                .map(materialSupplierFavs -> modelMapper.map(materialSupplierFavs, MaterialSupplierFavsResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void addFavSupplier(MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto) {


        List<MaterialSupplierFavs> lista = materialSupplierFavsRepository.findAll();
        for (MaterialSupplierFavs materialId :lista) {
            if (materialSupplierFavsRequestDto.getIdMaterial() == materialId.getMaterial().getId()){
                String mensaje = String.format(ErrorConstants.MATERIAL_ID_ALREADY_HAS_FAV,
                        materialId.getMaterial().getId());
                log.warn(mensaje);
                throw new IllegalStateException(mensaje);
            }
        }


        MaterialSupplierFavs entity = new MaterialSupplierFavs();
        entity.setSupplier(supplierService.getSupplierByID(materialSupplierFavsRequestDto.getIdSupplier()));
        entity.setMaterial(materialService.getMaterialById(materialSupplierFavsRequestDto.getIdMaterial()));
        materialSupplierFavsRepository.save(entity);
    }

    @Override

    public void delFavMaterial(Integer materialId, Integer supplierId) {

        Optional<MaterialSupplierFavs> entity = materialSupplierFavsRepository
                .findByMaterialSupplierFavsOpcional(materialId, supplierId);

        if (!entity.isPresent()) {

            String mensaje = String.format(ErrorConstants.FAV_RELATION_IS_NOT_SET, materialId, supplierId);
            log.warn(mensaje);
            throw new IllegalStateException(mensaje);

        }

        materialSupplierFavsRepository.delete(entity.get());

    }





    @Override
    public void replaceFavSupplier(MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto) {

        Integer idMaterial = materialSupplierFavsRequestDto.getIdMaterial();
        Integer idProveedor = materialSupplierFavsRequestDto.getIdSupplier();

        Optional <MaterialSupplierFavs> materialSupplierFavs =
                materialSupplierFavsRepository.findByMaterialsIdOpcional(idMaterial);

        if (!(materialSupplierFavs.isPresent())) {

            String mensaje = String.format(ErrorConstants.MATERIAL_BY_ID_NOT_FOUND, idMaterial);

            log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }

        MaterialSupplierFavs entity = materialSupplierFavs.get();

        entity.setSupplier(supplierService.getSupplierByID(idProveedor));

        materialSupplierFavsRepository.save(entity);

    }
}
