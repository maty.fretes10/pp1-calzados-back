package com.ungs.calzados.service;

import com.ungs.calzados.constants.AccountType;
import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.dto.AccountHistoryResponseDto;
import com.ungs.calzados.dto.CheckingAccountRequestDto;
import com.ungs.calzados.dto.CheckingAccountResponseDto;
import com.ungs.calzados.entity.AccountHistory;
import com.ungs.calzados.entity.CheckingAccount;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.Supplier;
import com.ungs.calzados.entity.sale.CheckingAccountTransaction;
import com.ungs.calzados.entity.sale.Transaction;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.NotEnoughFundsException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.AccountHistoryRepository;
import com.ungs.calzados.repository.CheckingAccountRepository;
import com.ungs.calzados.repository.CheckingAccountTransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CheckingAccountServiceImpl implements CheckingAccountService {

    private final CheckingAccountRepository checkingAccountRepository;
    private final CheckingAccountTransactionRepository transactionRepository;
    private final AccountHistoryRepository accountHistoryRepository;
    private final ClientService clientService;
    private final ModelMapper modelMapper;
    private final SupplierService supplierService;

    @Autowired
    public CheckingAccountServiceImpl(CheckingAccountRepository checkingAccountRepository,
                                      CheckingAccountTransactionRepository transactionRepository,
                                      AccountHistoryRepository accountHistoryRepository,
                                      ClientService clientService, SupplierService supplierService,
                                      ModelMapper modelMapper) {
        this.checkingAccountRepository = checkingAccountRepository;
        this.transactionRepository = transactionRepository;
        this.accountHistoryRepository = accountHistoryRepository;
        this.clientService = clientService;
        this.supplierService = supplierService;
        this.modelMapper = modelMapper;
    }

    @Override
    public CheckingAccount addCheckingAccount(CheckingAccount checkingAccount) {
        return checkingAccountRepository.save(checkingAccount);
    }

    @Override
    public CheckingAccountTransaction generateSaleTransaction(Transaction transaction, Double amount) {

        CheckingAccount checkingAccount = transaction.getClient().getCheckingAccount();
        checkCreditLimitForUser(transaction, amount, checkingAccount);

        AccountHistory accountHistory = new AccountHistory();
        saveMovement(accountHistory, amount, checkingAccount, MovementType.OUTPUT);

        checkingAccount.setBalance(checkingAccount.getBalance() - amount);
        this.checkingAccountRepository.save(checkingAccount);

        CheckingAccountTransaction checkingAccountTransaction = new CheckingAccountTransaction(transaction);
        checkingAccountTransaction.setMovementType(MovementType.OUTPUT);

        return transactionRepository.save(checkingAccountTransaction);
    }

    @Override
    public CheckingAccountResponseDto getBalance(AccountType accountType, Integer socioID) {

        Optional<Integer> optionalId = Optional.ofNullable(socioID);

        if (!optionalId.isPresent()) {
            throw new IllegalStateException(ErrorConstants.ACCOUNT_BY_ID_IS_NULL);
        }

        CheckingAccountResponseDto retorno = null;

        if (accountType.equals(AccountType.CLIENT)) {
            Client client = clientService.getClientByID(socioID);
            retorno = searchCheckingAccount(client.getCheckingAccount().getId());
        } else {
            Supplier supplier = supplierService.getSupplierByID(socioID);
            retorno = searchCheckingAccount(supplier.getCheckingAccount().getId());
        }

        return retorno;
    }

    private CheckingAccountResponseDto searchCheckingAccount(Integer id) {
        Optional<CheckingAccount> optionalCheckingAccount = this.checkingAccountRepository.findById(id);
        if (!optionalCheckingAccount.isPresent()) {
            String message = String.format(ErrorConstants.ACCOUNT_BY_ID_NOT_FOUND, id);
            log.warn(message);
            throw new ResourceNotFoundException(message);
        }

        CheckingAccount checkingAccountFound = optionalCheckingAccount.get();
        return modelMapper.map(checkingAccountFound, CheckingAccountResponseDto.class);
    }

    @Override
    public List<AccountHistoryResponseDto> getAccountHistory(AccountType accountType, Integer socioId) {

        Optional<Integer> optionalId = Optional.ofNullable(socioId);
        if (!optionalId.isPresent()) {
            throw new IllegalStateException(ErrorConstants.ACCOUNT_BY_ID_IS_NULL);
        }

        List<AccountHistoryResponseDto> retorno = null;

        if (accountType.equals(AccountType.CLIENT)) {
            Client client = clientService.getClientByID(socioId);
            Integer accountId = client.getCheckingAccount().getId();
            retorno = getAccountHistoryResponseDtos(accountId);
        } else {
            Supplier supplier = supplierService.getSupplierByID(socioId);
            Integer accountId = supplier.getCheckingAccount().getId();
            retorno = getAccountHistoryResponseDtos(accountId);
        }
        return retorno;
    }

    private List<AccountHistoryResponseDto> getAccountHistoryResponseDtos(Integer accountId) {
        List<AccountHistory> accountHistories = accountHistoryRepository.findAllByCheckingAccount(accountId);

        if (!accountHistories.isEmpty()) {
            return accountHistories.stream()
                    .parallel()
                    .map(accountHistory -> modelMapper.map(accountHistory, AccountHistoryResponseDto.class))
                    .collect(Collectors.toList());
        }

        log.warn(ErrorConstants.EMPTY_ACCOUNT_HISTORY_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_ACCOUNT_HISTORY_LIST);
    }

    @Override
    public void getDeposit(AccountType accountType, Integer socioId, CheckingAccountRequestDto accountRequestDto) {
        Optional<Integer> optionalId = Optional.ofNullable(socioId);
        if (!optionalId.isPresent()) {
            throw new IllegalStateException(ErrorConstants.ACCOUNT_BY_ID_IS_NULL);
        }
        if (accountType.equals(AccountType.CLIENT)) {
            Client client = clientService.getClientByID(socioId);
            Integer accountId = client.getCheckingAccount().getId();
            applyDeposit(accountId, accountRequestDto);
        } else {
            Supplier supplier = supplierService.getSupplierByID(socioId);
            Integer accountId = supplier.getCheckingAccount().getId();
            applyDeposit(accountId, accountRequestDto);
        }
    }

    private void applyDeposit(Integer accountId, CheckingAccountRequestDto accountRequestDto) {
        Optional<CheckingAccount> optionalCheckingAccount = checkingAccountRepository.findById(accountId);
        if (!optionalCheckingAccount.isPresent()) {
            String message = String.format(ErrorConstants.ACCOUNT_BY_ID_NOT_FOUND, accountId);
            log.warn(message);
            throw new ResourceNotFoundException(message);
        }

        CheckingAccount checkingAccount = optionalCheckingAccount.get();
        log.info("Sumar " + checkingAccount.getBalance() + "del balance con el deposito de  " + accountRequestDto.getBalance());
        checkingAccount.setBalance(checkingAccount.getBalance() + accountRequestDto.getBalance());

        this.checkingAccountRepository.save(checkingAccount);

        AccountHistory accountHistory = new AccountHistory();

        saveMovement(accountHistory, accountRequestDto.getBalance(), checkingAccount, MovementType.INPUT);
    }

    private void checkCreditLimitForUser(Transaction transaction, Double amount, CheckingAccount checkingAccount) {
        if (transaction.getClient().getCreditLimit() < -1 * (checkingAccount.getBalance() - amount))
            throw new NotEnoughFundsException("El cliente supero el limite de credito");
    }

    private void saveMovement(AccountHistory accountHistory, Double amount, CheckingAccount checkingAccount, MovementType movementType) {
        accountHistory.setDate(LocalDate.now());
        accountHistory.setCheckingAccountId(checkingAccount);
        accountHistory.setMoney((float) (amount + 0f));
        accountHistory.setType(movementType);
        accountHistoryRepository.save(accountHistory);
    }
}
