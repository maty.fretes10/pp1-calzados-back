package com.ungs.calzados.service;

import com.ungs.calzados.dto.sale.StoreRequestDto;
import com.ungs.calzados.dto.sale.StoreResponseDto;
import com.ungs.calzados.entity.Store;

import java.util.List;

public interface StoreService {
    Store getStoreById(Integer storeId);

    void addStore(StoreRequestDto storeRequest);

    StoreResponseDto getStoreByIdDto(Integer storeId);

    List<StoreResponseDto> getStores();

    void editStore(StoreRequestDto request, Integer storeId);
}
