package com.ungs.calzados.service;

import com.ungs.calzados.dto.PurchaseOrderRequestDto;
import com.ungs.calzados.dto.PurchaseOrderResponseDto;
import com.ungs.calzados.dto.sale.CreateBillRequestDto;
import com.ungs.calzados.entity.buy.PurchaseOrder;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;
import java.util.List;

public interface PurchaseOrderService {
    void addPurchaseOrder(PurchaseOrderRequestDto bill) throws JRException, FileNotFoundException;

    List<PurchaseOrderResponseDto> getPurchaseOrder();

    void inactivatePurchaseOrder(Integer purchaseOrderId);

    PurchaseOrderResponseDto getPurchaseOrderResponseDto(Integer purchaseOrderId);

    PurchaseOrder getPurchaseOrder(Integer purchaseOrderId);
}
