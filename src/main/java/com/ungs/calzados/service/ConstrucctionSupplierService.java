package com.ungs.calzados.service;

import com.ungs.calzados.dto.SupplierRequestDto;
import com.ungs.calzados.entity.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ConstrucctionSupplierService {

    private final SupplierService supplierService;
    private final CheckingAccountService checkingAccountService;
    private final AccountHistoryService accountHistoryService;
    private final LocationService locationService;
    private static final Double DEFAULT_EMPTY_BALANCE = 0d;

    public ConstrucctionSupplierService(
            SupplierService supplierService,
            CheckingAccountService checkingAccountService,
            AccountHistoryService accountHistoryService,
            LocationService locationService
    ) {
        this.supplierService = supplierService;
        this.checkingAccountService = checkingAccountService;
        this.accountHistoryService = accountHistoryService;
        this.locationService = locationService;

    }

    @Transactional
    public void constructionsSaveSupplier(SupplierRequestDto supplierRequest) {

        CheckingAccount checkingAccountEntitySaved = checkingAccountService
                .addCheckingAccount(CheckingAccount.builder().balance(DEFAULT_EMPTY_BALANCE).build());

        accountHistoryService.addAccountHistory(AccountHistory.builder().money(0).build(), "INPUT", checkingAccountEntitySaved);

        Locality localityEntityFound =
                locationService.getLocalityById(supplierRequest.getLocalityId());

       supplierService.addSupplier(
                supplierRequest,
                checkingAccountEntitySaved,
                localityEntityFound
        );
    }

    @Transactional
    public void constructionsEditSupplier(Integer supplierId, SupplierRequestDto supplierRequest) {
        Locality localityEntityFound =
                locationService.getLocalityById(supplierRequest.getLocalityId());

        supplierService.editSupplier(
                supplierId,
                supplierRequest,
                localityEntityFound
        );
    }
}

