package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.ModelState;
import com.ungs.calzados.constants.State;
import com.ungs.calzados.dto.ProductModelRequestDto;
import com.ungs.calzados.dto.product.*;
import com.ungs.calzados.entity.StockStore;
import com.ungs.calzados.entity.product.*;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.product.*;
import com.ungs.calzados.repository.sale.StockStoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;
    private final ColourRepository colourRepository;
    private final WaistRepository waistRepository;
    private final ProductModelRepository productModelRepository;
    private final ProductStatusRepository productStatusRepository;
    private final ModelMapper modelMapper;
    private final StockStoreRepository stockStoreRepository;
    private final StoreService storeService;
    private final PriceHistoryService priceHistoryService;

    @Autowired
    public ProductServiceImpl( ProductRepository productRepository,
                               ColourRepository colourRepository,
                               WaistRepository waistRepository,
                               ProductModelRepository productModelRepository,
                               ProductStatusRepository productStatusRepository,
                               ModelMapper modelMapper,
                               StockStoreRepository stockStoreRepository,
                               StoreService storeService,
                               PriceHistoryService priceHistoryService){
            this.productRepository = productRepository;
            this.colourRepository = colourRepository;
            this.waistRepository = waistRepository;
            this.productModelRepository = productModelRepository;
            this.productStatusRepository = productStatusRepository;
            this.modelMapper = modelMapper;
            this.storeService = storeService;
            this.stockStoreRepository = stockStoreRepository;
            this.priceHistoryService = priceHistoryService;
    }

    @Override
    public void addProduct(ProductRequestDto productRequestDto,
                           Colour colour,
                           ProductModel productModel,
                           ProductStatus productStatus,
                           Waist waist) {

        Optional<Product> product = getProductByModelColorWaist(productModel.getId(), colour.getId(), waist.getId());
        if (product.isPresent()){
            throw new IllegalArgumentException("Ya existe un producto con el modelo, color y talle");
        }

        Product entity = modelMapper.map(productRequestDto, Product.class);
        entity.setCreationDate(LocalDate.now());
        entity.setColour(colour);
        entity.setProductModel(productModel);
        entity.setProductStatus(productStatus);
        entity.setWaist(waist);
        entity.setState(State.ACTIVE);
        entity.setStockConsolidated(0);
        productRepository.save(entity);
        priceHistoryService.addPriceHistory(entity);
    }

    @Override
    public void addManufacturedProduct(Product product, Integer storeId){
        productRepository.save(product);

        priceHistoryService.addPriceHistory(product);

        StockStore entityStockStore = new StockStore();
        entityStockStore.setProduct(product);
        entityStockStore.setStore(storeService.getStoreById(storeId));
        entityStockStore.setTotalStock(product.getStockConsolidated());

        stockStoreRepository.save(entityStockStore);
    }

    @Override
    public void updateStockManufacturedProduct(Integer productId, Integer quantity) {
        Product foundProduct = getProductById(productId);
        int updateStock = foundProduct.getStockConsolidated() + quantity;
        foundProduct.setStockConsolidated(updateStock);

        productRepository.save(foundProduct);
    }

    @Override
    public Product getProductById(Integer id) {
        final Optional<Product> optionalProduct = this.productRepository.findById(id);
        if (!optionalProduct.isPresent())
            throw new ResourceNotFoundException(ErrorConstants.PRODUCT_BY_ID_NOT_FOUND);

        return optionalProduct.get();
    }

    @Override
    public Optional<Product> getProductByModelColorWaist(Integer modelId, Integer colorId, Integer waistId) {
        Optional<Product> optionalProducts = this.productRepository.findByModelColorWaist(modelId, colorId, waistId);

        return optionalProducts;
    }

    @Override
    public List<ProductResponseDto> getProducts() {
        List<Product> allProducts = productRepository.findAll();

        if (!allProducts.isEmpty()) {
            return allProducts.stream()
                    .parallel()
                    .map(product -> modelMapper.map(product, ProductResponseDto.class))
                    .collect(Collectors.toList());
        }
        throw new NoDataFoundException(ErrorConstants.EMPTY_PRODUCTS_LIST);
    }

    @Override
    public void addColour(ColourRequestDto colourRequestDto) {
        Optional<Colour> colour = colourRepository.findByName(colourRequestDto.getName().toUpperCase());
        if(colour.isPresent() && colour.get().getState().equals(State.ACTIVE)){
            throw new IllegalStateException("Ya existe el color " + colourRequestDto.getName());
        }
        Colour entity = modelMapper.map(colourRequestDto, Colour.class);
        entity.setName(entity.getName().toUpperCase());
        entity.setState(State.ACTIVE);
        colourRepository.save(entity);
    }

    @Override
    public Colour getColourById(Integer colourId) {
        String errorMessage = String.format(ErrorConstants.COLOR_BY_ID_NOT_FOUND, colourId);
        Optional<Colour> ColourOptional = colourRepository.findById(colourId);

        if (ColourOptional.isPresent()) {
            return ColourOptional.get();
        }

        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public List<ColourResponseDto> getColors() {
        List<Colour> allcolors = colourRepository.findAll();
        List<ColourResponseDto> list = new ArrayList<>();
        for (Colour colour: allcolors) {
            if(colour.getState().equals(State.ACTIVE)){
                list.add(modelMapper.map(colour, ColourResponseDto.class));
            }
        }

        if (!list.isEmpty())
            return list;

        throw new NoDataFoundException();
    }

    @Override
    public void inactivateColour(Integer colorId) {
        Optional <Colour> colourOptional = colourRepository.findById(colorId);
        if (!colourOptional.isPresent()) {
            log.warn(ErrorConstants.COLOR_BY_ID_NOT_FOUND);
            throw new ResourceNotFoundException(ErrorConstants.COLOR_BY_ID_NOT_FOUND);
        }

        Colour entity = colourOptional.get();
        if (entity.getState().equals(State.INACTIVE)){
            String mensaje = String.format(ErrorConstants.STATE_COLOR_INACTIVE, colorId);
            log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }
        entity.setState(State.INACTIVE);
        colourRepository.save(entity);
    }

    @Override
    public void editColour(Integer idColour, ColourRequestDto colourRequestDto) {
        Optional <Colour> colorOptional = colourRepository.findById(idColour);
        if (!colorOptional.isPresent()) {
            log.warn(ErrorConstants.COLOR_BY_ID_NOT_FOUND);
            throw new ResourceNotFoundException(ErrorConstants.COLOR_BY_ID_NOT_FOUND);
        }

        Colour colour = colorOptional.get();
        colour.setName(colourRequestDto.getName().toUpperCase());
        colourRepository.save(colour);
    }

    @Override
    public void addWaist(WaistRequestDto waistRequestDto) {
        Optional<Waist> waist = waistRepository.findByName(waistRequestDto.getName().toUpperCase());
        if(waist.isPresent() && waist.get().getState().equals(State.ACTIVE)){
            throw new IllegalStateException("Ya existe el talle " + waistRequestDto.getName());
        }
        Waist entity = modelMapper.map(waistRequestDto, Waist.class);
        entity.setState(State.ACTIVE);
        waistRepository.save(entity);
    }

    @Override
    public Waist getWaistById(Integer waistId) {
        String errorMessage = String.format(ErrorConstants.WAIST_BY_ID_NOT_FOUND, waistId);
        Optional<Waist> waistOptional = waistRepository.findById(waistId);

        if (waistOptional.isPresent()) {
            return waistOptional.get();
        }

        throw new ResourceNotFoundException(errorMessage);
    }


    @Override
    public List<WaistResponseDto> getWaist() {
        List<Waist> allwaist = waistRepository.findAll();
        List<WaistResponseDto> list = new ArrayList<>();
        for (Waist waist: allwaist) {
            if(waist.getState().equals(State.ACTIVE)){
                list.add(modelMapper.map(waist, WaistResponseDto.class));
            }
        }

        if (!list.isEmpty())
            return list;

        throw new NoDataFoundException();
    }

    @Override
    public void inactivateWaist(Integer waistId) {
        Optional <Waist> waistOptional = waistRepository.findById(waistId);
        if (!waistOptional.isPresent()) {
            log.warn(ErrorConstants.WAIST_BY_ID_NOT_FOUND);
            throw new ResourceNotFoundException(ErrorConstants.WAIST_BY_ID_NOT_FOUND);
        }

        Waist entity = waistOptional.get();
        if (entity.getState().equals(State.INACTIVE)){
            String mensaje = String.format(ErrorConstants.STATE_WAIST_INACTIVE, waistId);
            log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }
        entity.setState(State.INACTIVE);
        waistRepository.save(entity);
    }

    @Override
    public void editWaist(Integer waistId, WaistRequestDto waistRequestDto) {
        Optional <Waist> waistOptional = waistRepository.findById(waistId);
        if (!waistOptional.isPresent()) {
            log.warn(ErrorConstants.WAIST_BY_ID_NOT_FOUND);
            throw new ResourceNotFoundException(ErrorConstants.WAIST_BY_ID_NOT_FOUND);
        }

        Waist waist = waistOptional.get();
        waist.setName(waistRequestDto.getName());
        waistRepository.save(waist);
    }

    @Override
    public void addProductModel(ProductModelRequestDto productModelRequestDto) {
        ProductModel entity = modelMapper.map(productModelRequestDto, ProductModel.class);
        entity.setState(ModelState.ACTIVE);
        productModelRepository.save(entity);
    }


    @Override
    public ProductModel getProductModelById(Integer productModelId) {
        String errorMessage = String.format(ErrorConstants.MODEL_PRODUCT_BY_ID_NOT_FOUND, productModelId);
        Optional<ProductModel> productModelOptional = productModelRepository.findById(productModelId);

        if (productModelOptional.isPresent()) {
            return productModelOptional.get();
        }

        throw new ResourceNotFoundException(errorMessage);
    }



    @Override
    public List<ProductModelResponseDto> getProductsModels() {
        List<ProductModelResponseDto> allmodels = productModelRepository.findAll().stream()
                .map(model -> modelMapper.map(model, ProductModelResponseDto.class))
                .collect(Collectors.toList());

        if (!allmodels.isEmpty())
            return allmodels;

        throw new NoDataFoundException();
    }

    @Override
    public ProductStatus getProductStatusById(Integer productStatusId) {
        String errorMessage = String.format(ErrorConstants.STATUS_PRODUCTO_BY_ID_NOT_FOUND, productStatusId);
        Optional<ProductStatus> productStatusOptional = productStatusRepository.findById(productStatusId);

        if (productStatusOptional.isPresent()) {
            return productStatusOptional.get();
        }

        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public List<ProductStatusResponseDto> getProductsStatus() {
        List<ProductStatusResponseDto> allstatus = productStatusRepository.findAll().stream()
                .map(status -> modelMapper.map(status, ProductStatusResponseDto.class))
                .collect(Collectors.toList());

        if (!allstatus.isEmpty())
            return allstatus;

        throw new NoDataFoundException();
    }

    @Override
    public void disProductModel(Integer modelId) {
        Optional <ProductModel> modelOptional = productModelRepository.findById(modelId);
        if (!modelOptional.isPresent()) {
            log.warn(ErrorConstants.MODEL_PRODUCT_BY_ID_IS_NULL);
            throw new ResourceNotFoundException(ErrorConstants.SUPPLIER_BY_ID_IS_NULL);
        }

        ProductModel entity = modelOptional.get();
        if (entity.getState().equals(ModelState.DISCONTINUED)){
            String mensaje = String.format(ErrorConstants.STATE_PRODUCT_MODEL_DISCONTINUED, modelId);
            log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }
        entity.setState(ModelState.DISCONTINUED);
        productModelRepository.save(entity);
    }

    @Override
    public void editProductModel(Integer modelId, ProductModelRequestDto request) {
        log.info("editando el modelo del producto");
        Optional <ProductModel> modelOptional = productModelRepository.findById(modelId);
        if (!modelOptional.isPresent()) {
            log.warn(ErrorConstants.MODEL_PRODUCT_BY_ID_IS_NULL);
            throw new ResourceNotFoundException(ErrorConstants.SUPPLIER_BY_ID_IS_NULL);
        }

        ProductModel nuevo = modelMapper.map(request, ProductModel.class);
        nuevo.setState(getProductModelById(modelId).getState());

        nuevo.setId(modelId);
        productModelRepository.save(nuevo);
    }



}
