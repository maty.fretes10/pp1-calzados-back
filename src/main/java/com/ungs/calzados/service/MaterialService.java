package com.ungs.calzados.service;

import com.ungs.calzados.dto.MaterialRequestDto;
import com.ungs.calzados.dto.MaterialResponseDto;
import com.ungs.calzados.entity.Material;

import java.util.List;

public interface MaterialService {

    List<MaterialResponseDto> getMaterials();

    void addMaterial(MaterialRequestDto supplyRequest);

    void inactivateMaterial(Integer materialId);

    Material getMaterialById(Integer id);

    MaterialResponseDto getMaterialByIdResponse(Integer id);

    void updateStockMaterial(Integer materialId, Integer requiredStock);

    void returnStockMaterial(Integer materialId, Integer requiredStock);
}
