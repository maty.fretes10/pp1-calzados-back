package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.StockStoreRequestDto;
import com.ungs.calzados.dto.StockStoreResponseDto;
import com.ungs.calzados.entity.StockStore;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.sale.StockStoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class StockStoreServiceImpl implements StockStoreService {

    private StockStoreRepository stockStoreRepository;
    private StoreService storeService;
    private ProductService productService;

    @Autowired
    public StockStoreServiceImpl(StockStoreRepository stockStoreRepository,
                                 StoreService storeService,
                                 ProductService productService) {
        this.stockStoreRepository = stockStoreRepository;
        this.storeService = storeService;
        this.productService = productService;
    }

    @Override
    public List<StockStore> findByIdInList(Integer storeId, List<Integer> idList) {
        return stockStoreRepository.findByIdInList(storeId, idList);
    }

    @Override
    public StockStore save(StockStore stockStore) {
        return stockStoreRepository.save(stockStore);
    }

    @Override
    public StockStore findStockStoreById(Integer storeId, Integer productId) {
        final Optional<StockStore> optionalStockFactory = this.stockStoreRepository.findByStoreIdAndProductId(storeId, productId);
        if (!optionalStockFactory.isPresent())
            throw new ResourceNotFoundException("Registro de stock factory no encontrado");

        return optionalStockFactory.get();
    }

    @Override
    public void addStockStore(StockStoreRequestDto request) {

        Optional<StockStore> comprobar = stockStoreRepository.findByStoreIdAndProductId(request.getStoreId(), request.getProductId());

        if (comprobar.isPresent()){
            log.info("La sucursal ya cuenta con stock de este producto. Se procederá a actualizar el stock del mismo");
            Integer stockUpdate = request.getTotalStock();
            comprobar.get().setTotalStock(comprobar.get().getTotalStock()+stockUpdate);
            stockStoreRepository.save(comprobar.get());
        }

        else {
            log.info("La sucursal no cuenta con stock de este producto. Se procederá a dar de alta el stock");
            StockStore nuevo = new StockStore();
            nuevo.setStore(storeService.getStoreById(request.getStoreId()));
            nuevo.setProduct(productService.getProductById(request.getProductId()));
            nuevo.setTotalStock(request.getTotalStock());

        stockStoreRepository.save(nuevo);
        }
    }

    @Override
    public void updateStockStore(StockStore stockStore, Integer quantity) {
        stockStore.setTotalStock(stockStore.getTotalStock()+quantity);
        stockStoreRepository.save(stockStore);
    }

    @Override
    public List<StockStoreResponseDto> findByProductId(Integer productId) {
        List<Optional<StockStore>> optional = stockStoreRepository.findByProductId(productId);

        if (optional.isEmpty()){
            log.warn(ErrorConstants.PRODUCT_BY_ID_NOT_FOUND);
            throw new NoDataFoundException(ErrorConstants.PRODUCT_BY_ID_NOT_FOUND);
        }

        List<StockStoreResponseDto> retorno = new ArrayList<>();


        for (int i = 0; i < optional.size(); i++) {
                StockStoreResponseDto response = new StockStoreResponseDto();
                response.setProduct(optional.get(i).get().getProduct());
                response.setStore(optional.get(i).get().getStore());
                response.setStock(optional.get(i).get().getTotalStock());
                retorno.add(response);
        }
        return retorno;
    }

    @Override
    public List<StockStoreResponseDto> findByStoreId(Integer storeId) {
        List<Optional<StockStore>> optional = stockStoreRepository.findByStoreId(storeId);

        if (optional.isEmpty()){
            log.warn(ErrorConstants.STORE_BY_ID_NOT_FOUND);
            throw new NoDataFoundException(ErrorConstants.STORE_BY_ID_NOT_FOUND);
        }

        List<StockStoreResponseDto> retorno = new ArrayList<>();

        for (int i = 0; i < optional.size(); i++) {
            StockStoreResponseDto response = new StockStoreResponseDto();
            response.setProduct(optional.get(i).get().getProduct());
            response.setStore(optional.get(i).get().getStore());
            response.setStock(optional.get(i).get().getTotalStock());
            retorno.add(response);
        }
        return retorno;
    }


}
