package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.State;
import com.ungs.calzados.dto.sale.StoreRequestDto;
import com.ungs.calzados.dto.sale.StoreResponseDto;
import com.ungs.calzados.entity.CashRegister;
import com.ungs.calzados.entity.Locality;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.StoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StoreServiceImpl implements StoreService {

    private final LocationService locationService;
    private final StoreRepository storeRepository;
    private final CashRegisterService cashRegisterService;
    private final ModelMapper modelMapper;


    @Autowired
    public StoreServiceImpl(
            LocationService locationService,
            StoreRepository storeRepository,
            CashRegisterService cashRegisterService,
            ModelMapper modelMapper) {

        this.locationService = locationService;
        this.storeRepository = storeRepository;
        this.cashRegisterService = cashRegisterService;
        this.modelMapper = modelMapper;
    }

    @Override
    public Store getStoreById(Integer storeId) {
        String errorMessage = String.format(ErrorConstants.STORE_BY_ID_NOT_FOUND, storeId);
        Optional<Store> optionalStore = storeRepository.findById(storeId);
        if (optionalStore.isPresent()) {
            log.info("Store with id {} found: {}", storeId, optionalStore.get());
            return optionalStore.get();
        }
        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public void addStore(StoreRequestDto request) {
        checkUniqueFields(request);
        Store storeToSave = modelMapper.map(request, Store.class);
        Locality localityEntity = locationService.getLocalityById(request.getLocalityId());

        storeToSave.setLocality(localityEntity);
        storeToSave.setCuit("30-54808315-6");
        storeToSave.setState(State.ACTIVE);
        storeRepository.save(storeToSave);
        cashRegisterService.addCashRegister(CashRegister.builder().balance(0.0).build(), storeToSave);

    }

    @Override
    public StoreResponseDto getStoreByIdDto(Integer storeId) {
        return modelMapper.map(this.getStoreById(storeId), StoreResponseDto.class);
    }

    @Override
    public List<StoreResponseDto> getStores() {
        List<Store> sucursalesLista = storeRepository.findAll();
        if (!sucursalesLista.isEmpty()){
            return sucursalesLista.stream().parallel().map(store -> modelMapper.map(store,StoreResponseDto.class)).collect(Collectors.toList());

        }
        log.warn(ErrorConstants.EMPTY_STORES_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_STORES_LIST);
    }

    @Override
    public void editStore(StoreRequestDto request, Integer storeId) {
        Store storeToEdit = storeRepository.findById(storeId).get();

        Locality localityEntity = locationService.getLocalityById(request.getLocalityId());
        storeToEdit.setLocality(localityEntity);
        storeToEdit.setStreet(request.getStreet());
        storeToEdit.setStreetNumber(request.getStreetNumber());

        storeToEdit.setName(request.getName());
        storeToEdit.setSalePoint(request.getSalePoint());
        storeToEdit.setEmail(request.getEmail());
        storeToEdit.setTelephone(request.getTelephone());
        
        storeRepository.save(storeToEdit);
    }

    private void checkUniqueFields(StoreRequestDto storeRequest) {
        String errorMessageEmail =
                String.format(ErrorConstants.STORE_BY_EMAIL_ALREADY_EXISTS, storeRequest.getEmail());
        String errorMessageSalePoint =
                String.format(ErrorConstants.STORE_BY_SALE_POINT_ALREADY_EXISTS, storeRequest.getSalePoint());

        if (this.existsByEmail(storeRequest.getEmail())) {
            throw new IllegalArgumentException(errorMessageEmail);
        }
        if (this.existsBySalePoint(storeRequest.getSalePoint())) {
            throw new IllegalArgumentException(errorMessageSalePoint);
        }
    }

    private boolean existsByEmail(String email) {
        return storeRepository.existsByEmailIgnoreCase(email);
    }

    private boolean existsBySalePoint(String salePoint) {
        return storeRepository.existsBySalePoint(salePoint);
    }
}
