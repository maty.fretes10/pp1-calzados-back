package com.ungs.calzados.service;

import com.ungs.calzados.dto.SupplierRequestDto;
import com.ungs.calzados.dto.SupplierResponseDto;
import com.ungs.calzados.entity.CheckingAccount;
import com.ungs.calzados.entity.Locality;
import com.ungs.calzados.entity.Supplier;

import java.util.List;

public interface SupplierService {

    List <SupplierResponseDto> getSuppliers();

    void inactivateSupplier(Integer supplierId);

    void addSupplier(
            SupplierRequestDto supplierRequest,
            CheckingAccount checkingAccountEntitySaved,
            Locality localityEntityFound);

    Supplier getSupplierByID(Integer id);

    void editSupplier(
            Integer supplierId,
            SupplierRequestDto supplierRequest,
            Locality locality);

}
