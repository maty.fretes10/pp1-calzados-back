package com.ungs.calzados.service;

import com.ungs.calzados.entity.CashRegister;
import com.ungs.calzados.entity.Store;

public interface CashRegisterService {

    void addCashRegister(CashRegister cashRegister, Store store);

    CashRegister getCashRegisterByIdSucursal(Integer sucursalId);

}
