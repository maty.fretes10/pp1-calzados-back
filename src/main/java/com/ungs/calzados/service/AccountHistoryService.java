package com.ungs.calzados.service;

import com.ungs.calzados.entity.AccountHistory;
import com.ungs.calzados.entity.CheckingAccount;

public interface AccountHistoryService {
    AccountHistory addAccountHistory(AccountHistory priceHistory, String type, CheckingAccount checkingAccountEntitySaved);
}
