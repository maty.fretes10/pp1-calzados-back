package com.ungs.calzados.service;

import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.MaterialSupplierFavsRequestDto;
import com.ungs.calzados.dto.MaterialSupplierFavsResponseDto;


import java.util.List;

public interface MaterialSupplierFavsService {

    List<MaterialSupplierFavsResponseDto> getMaterialSupplierFavById(Integer id, FavsType favsType);

    void addFavSupplier(MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto);

    void delFavMaterial(Integer materialId, Integer supplierId);

    void replaceFavSupplier (MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto);

}
