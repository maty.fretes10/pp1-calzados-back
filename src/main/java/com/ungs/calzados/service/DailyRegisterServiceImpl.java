package com.ungs.calzados.service;

import com.ungs.calzados.constants.DailyState;
import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.dto.DailyRegisterResponseDto;
import com.ungs.calzados.entity.CashRegister;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.repository.DailyRegisterRepository;
import com.ungs.calzados.repository.StoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DailyRegisterServiceImpl implements DailyRegisterService{


    private final CashRegisterService cashRegisterService;
    private final DailyRegisterRepository dailyRegisterRepository;
    private final StoreRepository storeRepository;
    private final ModelMapper modelMapper;



    public DailyRegisterServiceImpl(CashRegisterService cashRegisterService,
                                    DailyRegisterRepository dailyRegisterRepository, StoreRepository storeRepository, ModelMapper modelMapper){
        this.dailyRegisterRepository = dailyRegisterRepository;
        this.cashRegisterService = cashRegisterService;
        this.storeRepository = storeRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public void addDaily(Integer sucursalId) {
        closeDailyRegisterOld(sucursalId);
        CashRegister caja = cashRegisterService.getCashRegisterByIdSucursal(sucursalId);

        Optional<DailyRegister> cajaFecha = dailyRegisterRepository.existsByDate(LocalDate.now(), caja.getId());

        if (!cajaFecha.isPresent()){
            log.info("No existe una caja diaria con fecha de hoy. Creando una.");
            DailyRegister register = new DailyRegister();
            register.setCashRegister(caja);
            register.setDate(LocalDate.now());
            register.setBalance(0.0);
            register.setState(DailyState.OPEN);
            dailyRegisterRepository.save(register);
            return;
        }

        DailyRegister dailyRegister = cajaFecha.get();

        if (dailyRegister.getState().equals(DailyState.CLOSED)) {
            log.info("Se encontró la caja pero estaba cerrada. Abriendo caja.");
            dailyRegister.setState(DailyState.OPEN);
            dailyRegisterRepository.save(dailyRegister);
            return;
        }

        log.info("La caja ya está abierta");
    }

    @Override
    public List<DailyRegisterResponseDto> listDailies(Integer sucursalId) {

        CashRegister caja = cashRegisterService.getCashRegisterByIdSucursal(sucursalId);


        List<DailyRegister> dailiesLista = dailyRegisterRepository.findByCashRegisterId(caja.getId());

        if (dailiesLista.isEmpty()) {
            log.warn(ErrorConstants.EMPTY_DAILIES_LIST);
            throw new NoDataFoundException(ErrorConstants.EMPTY_DAILIES_LIST);
        }

        return dailiesLista.stream().parallel().map(daily -> modelMapper.map(daily, DailyRegisterResponseDto.class)).collect(Collectors.toList());
    }

    @Override
    public void closeDaily(Integer sucursalId) {

        DailyRegister cajaAbierta = getDailyOpenBySucursal(sucursalId);

        cajaAbierta.setId(cajaAbierta.getId());
        cajaAbierta.setState(DailyState.CLOSED);

        dailyRegisterRepository.save(cajaAbierta);
    }


    @Override
    public DailyRegister getDailyOpenBySucursal(Integer sucursalId){

        CashRegister caja = cashRegisterService.getCashRegisterByIdSucursal(sucursalId);

        Optional<DailyRegister> cajaDiaria = dailyRegisterRepository.existsByDate(LocalDate.now(), caja.getId());

        if (!cajaDiaria.isPresent()){
            log.warn(ErrorConstants.DAILY_REGISTER_NOT_FOUND);
            throw new NoDataFoundException();
        }

        List<DailyRegister> dailiesLista = dailyRegisterRepository.findByCashRegisterId(caja.getId());

        for (int i=0; i<dailiesLista.size(); i++){
            if (dailiesLista.get(i).getState().equals(DailyState.OPEN) &&
                    dailiesLista.get(i).getDate().equals(LocalDate.now())){
                DailyRegister retorno = dailiesLista.get(i);
                return retorno;
            }
        }
        log.warn(ErrorConstants.EMPTY_DAILIES_LIST);
        throw new NoDataFoundException(ErrorConstants.EMPTY_DAILIES_LIST);
    }

    @Override
    public void addBalance(DailyRegister dailyRegister, MovementType movementType, Double amount) {
        Double balanceDaily = dailyRegister.getBalance();
        Double balanceDailyNuevo;
        Double balanceEnCaja = dailyRegister.getCashRegister().getBalance();

        if (movementType.equals(MovementType.INPUT)){
             balanceDailyNuevo=balanceDaily+amount;
             dailyRegister.getCashRegister().setBalance(balanceEnCaja + amount);
             dailyRegister.setBalance(balanceDailyNuevo);
        }

        if (movementType.equals(MovementType.OUTPUT)){
            balanceDailyNuevo=balanceDaily-amount;
            dailyRegister.setBalance(balanceDailyNuevo);
            dailyRegister.getCashRegister().setBalance(balanceEnCaja-amount);
        }
    }

    void closeDailyRegisterOld(Integer sucursalId){
        CashRegister caja = cashRegisterService.getCashRegisterByIdSucursal(sucursalId);
        List<DailyRegister> dailiesLista = dailyRegisterRepository.findByCashRegisterId(caja.getId());
        if (!dailiesLista.isEmpty()){
            for ( DailyRegister dailyRegister: dailiesLista ) {
                if(!dailyRegister.getDate().equals(LocalDate.now()) && dailyRegister.getState().equals(DailyState.OPEN)){
                     dailyRegister.setState(DailyState.CLOSED);
                     dailyRegisterRepository.save(dailyRegister);
                }
            }
        }
    }
}
