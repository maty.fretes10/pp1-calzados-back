package com.ungs.calzados.service;

import com.ungs.calzados.entity.product.PriceHistory;
import com.ungs.calzados.entity.product.Product;
import com.ungs.calzados.repository.PriceHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Slf4j
public class PriceHistoryServiceImpl implements PriceHistoryService {

    private final PriceHistoryRepository priceHistoryRepository;

    @Autowired
    public PriceHistoryServiceImpl(PriceHistoryRepository priceHistoryRepository) {
        this.priceHistoryRepository = priceHistoryRepository;
    }

    @Override
    public PriceHistory addPriceHistory(Product product) {
        PriceHistory priceHistory = new PriceHistory();
        priceHistory.setPrice(product.getPriceSale());
        priceHistory.setDate(LocalDate.now());
        priceHistory.setProduct_id(product);

        return priceHistoryRepository.save(priceHistory);
    }
}
