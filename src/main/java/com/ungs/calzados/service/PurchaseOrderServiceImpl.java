package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.constants.State;
import com.ungs.calzados.dto.*;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.Supplier;
import com.ungs.calzados.entity.buy.PurchaseOrder;
import com.ungs.calzados.entity.buy.PurchaseOrderItem;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.PurchaseOrderItemRepository;
import com.ungs.calzados.repository.PurchaseOrderRepository;
import com.ungs.calzados.service.report.ReportService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final ReportService reportService;
    private final ModelMapper modelMapper;
    private final StoreService storeService;
    private final SupplierService supplierService;
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseOrderItemRepository purchaseOrderItemRepository;
    private final RegisterItemService registerItemService;
    private final DailyRegisterService dailyRegisterService;



    @Autowired
    public PurchaseOrderServiceImpl(
            ReportService reportService,
            ModelMapper modelMapper,
            StoreService storeService,
            SupplierService supplierService,
            PurchaseOrderRepository purchaseOrderRepository,
            PurchaseOrderItemRepository purchaseOrderItemRepository,
            RegisterItemService registerItemService,
            DailyRegisterService dailyRegisterService) {

        this.reportService = reportService;
        this.modelMapper = modelMapper;
        this.storeService = storeService;
        this.supplierService = supplierService;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderItemRepository  = purchaseOrderItemRepository;
        this.registerItemService = registerItemService;
        this.dailyRegisterService = dailyRegisterService;
    }

    @Override
    public void addPurchaseOrder(PurchaseOrderRequestDto orderRequestDto) throws JRException, FileNotFoundException {

        Store store = storeService.getStoreById(orderRequestDto.getStoreId());
        DailyRegister cajaAbierta = dailyRegisterService.getDailyOpenBySucursal(store.getId());
        Supplier supplier = supplierService.getSupplierByID(orderRequestDto.getSupplierId());
        PurchaseOrder order = new PurchaseOrder();

        order.setStoreId(store.getId());
        order.setStoreAddress(store.getStreet() +" " + store.getStreetNumber());
        order.setSupplierName(supplier.getName());
        order.setSupplierAddress(supplier.getStreet() + " " + supplier.getStreetNumber());
        order.setSupplierIdentification(supplier.getCuit());
        order.setState(State.ACTIVE);

        purchaseOrderRepository.save(order);

        order.setItems(defineItems(orderRequestDto, order));
        purchaseOrderRepository.save(order);

        PurchaseRequestReportDto purchaseRequestReportDto = modelMapper.map(order, PurchaseRequestReportDto.class);
        purchaseRequestReportDto.setSalePoint(store.getSalePoint());
        purchaseRequestReportDto.setEmail(store.getEmail());
        purchaseRequestReportDto.setTelephone(store.getTelephone());

        purchaseRequestReportDto.setLocalidad(store.getLocality().getName() + "  <CP: "+ store.getLocality().getZipCode() + "> , " + store.getLocality().getProvince().getName());
        reportService.exportReportPurchase(purchaseRequestReportDto, supplier.getEmail());

        for (int i=0;i<order.getItems().size(); i++){
            PurchaseOrderItem compra = order.getItems().get(i);
            registerItemService.addItem("Compra COD: "+ compra.getId() + " - "+ compra.getDetail(), MovementType.OUTPUT, compra.getTotalPrice(), cajaAbierta);
            }
    }

    private List<PurchaseOrderItem> defineItems(PurchaseOrderRequestDto orderRequest, PurchaseOrder order) {

        double priceTotal = 0;

        List<PurchaseOrderItem> list = new ArrayList<>();
        for (PurchaseOrderItemRequestDto item : orderRequest.getItems()) {
            PurchaseOrderItem entity = modelMapper.map(item, PurchaseOrderItem.class);
            entity.setPurchaseOrder(order);
            entity.setTotalPrice(item.getQuantity() * item.getUnitPrice());
            priceTotal += entity.getTotalPrice();
            list.add(entity);
            purchaseOrderItemRepository.save(entity);
        }
        order.setTotal(priceTotal);
        return list;
    }

    @Override
    public List<PurchaseOrderResponseDto> getPurchaseOrder() {
        List<PurchaseOrder> allPurchaseOrder = (List<PurchaseOrder>) purchaseOrderRepository.findAll();
        List<PurchaseOrder> retorno = new ArrayList<>();
        for (PurchaseOrder purchaseOrder: allPurchaseOrder) {
            if(!purchaseOrder.getState().equals(State.INACTIVE)){
                retorno.add(purchaseOrder);
            }
        }
        if (retorno.isEmpty()) {
            log.warn(ErrorConstants.EMPTY_PURCHASE_ORDER_LIST);
            throw new NoDataFoundException(ErrorConstants.EMPTY_PURCHASE_ORDER_LIST);
        }
        return retorno.stream()
                .parallel()
                .map(order -> modelMapper.map(order, PurchaseOrderResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void inactivatePurchaseOrder(Integer purchaseOrderId) {
        PurchaseOrder purchaseOrder = getPurchaseOrder(purchaseOrderId);
        if( purchaseOrder.getState().equals(State.INACTIVE) ){
            log.warn(ErrorConstants.INACTIVE_PURCHASE_ORDER);
            throw new IllegalArgumentException(ErrorConstants.INACTIVE_PURCHASE_ORDER);
        }
        purchaseOrder.setState(State.INACTIVE);
        purchaseOrderRepository.save(purchaseOrder);

        DailyRegister cajaAbierta = dailyRegisterService.getDailyOpenBySucursal(purchaseOrder.getStoreId());

        for ( PurchaseOrderItem item : purchaseOrder.getItems() ) {
            registerItemService.addItem("Compra cancelada: "+item.getDetail(), MovementType.INPUT, item.getTotalPrice(), cajaAbierta);
        }
    }

    @Override
    public PurchaseOrderResponseDto getPurchaseOrderResponseDto(Integer purchaseOrderId) {
        Optional<Integer> optionalId = Optional.ofNullable(purchaseOrderId);
        if (!optionalId.isPresent()) {
            log.warn(ErrorConstants.PURCHASE_ORDER_BY_ID_NOT_FOUND);
            throw new IllegalStateException(ErrorConstants.PURCHASE_ORDER_BY_ID_NOT_FOUND);
        }
        PurchaseOrder purchaseOrder = getPurchaseOrder(purchaseOrderId);
        log.info("Purchase Order with id {} was found", purchaseOrderId);
        return modelMapper.map(purchaseOrder, PurchaseOrderResponseDto.class);
    }

    @Override
    public PurchaseOrder getPurchaseOrder(Integer purchaseOrderId) {
        final Optional<PurchaseOrder> optionalPurchaseOrder = this.purchaseOrderRepository.findById(purchaseOrderId);
        if (!optionalPurchaseOrder.isPresent()) {
            log.warn(ErrorConstants.PURCHASE_ORDER_BY_ID_NOT_FOUND);
            throw new ResourceNotFoundException(ErrorConstants.PURCHASE_ORDER_BY_ID_NOT_FOUND);
        }
        return optionalPurchaseOrder.get();
    }
}
