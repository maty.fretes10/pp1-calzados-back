package com.ungs.calzados.service;

import com.ungs.calzados.dto.CategoryAfipResponseDto;
import com.ungs.calzados.dto.DocumentTypeResponseDto;
import com.ungs.calzados.entity.CategoryAfip;
import com.ungs.calzados.entity.DocumentType;

import java.util.List;

public interface IdentificationService {
    List<DocumentTypeResponseDto> getDocumentTypes();

    DocumentType getDocumentTypeEntity(Integer documentTypeId);

    CategoryAfip getCategoryAfipById(Integer categoryAfipId);

    List<CategoryAfipResponseDto> getCategoriesAfip();
}
