package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.dto.LocalityResponseDto;
import com.ungs.calzados.dto.ProvinceResponseDto;
import com.ungs.calzados.entity.Locality;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.LocalityRepository;
import com.ungs.calzados.repository.ProvinceRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LocationServiceImpl implements LocationService {

    private final ProvinceRepository provinceRepository;
    private final LocalityRepository localityRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public LocationServiceImpl(
            ProvinceRepository provinceRepository,
            LocalityRepository localityRepository,
            ModelMapper modelMapper) {

        this.provinceRepository = provinceRepository;
        this.localityRepository = localityRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<ProvinceResponseDto> getAllProvinces() {

        List<ProvinceResponseDto> allprovinces = provinceRepository.findAll().stream()
                .map(province -> modelMapper.map(province, ProvinceResponseDto.class))
                .collect(Collectors.toList());

        if (!allprovinces.isEmpty())
            return allprovinces;

        throw new NoDataFoundException();
    }

    public List<LocalityResponseDto> getAllLocalitiesByProvinceId(Integer provinceId) {

        String errorMessage = String.format(ErrorConstants.EMPTY_LOCALITIES_LIST_BY_PROVINCE_ID, provinceId);
        List<LocalityResponseDto> localitiesFound = localityRepository.findAllByProvinceId(provinceId).stream()
                .parallel()
                .map(localityFound -> modelMapper.map(localityFound, LocalityResponseDto.class))
                .collect(Collectors.toList());

        if (!localitiesFound.isEmpty())
            return localitiesFound;

        log.info(errorMessage);
        throw new NoDataFoundException();
    }

    @Override
    public Locality getLocalityById(Integer localityId) {
        Optional<Locality> optionalLocality = localityRepository.findById(localityId);
        String errorMessage = String.format(ErrorConstants.LOCALITY_BY_ID_NOT_FOUND, localityId);

        if (optionalLocality.isPresent()) {
            log.info("Locality with id {} was found", localityId);
            return optionalLocality.get();
        }
        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public LocalityResponseDto getLocalityByIdDto(Integer localityId) {
        Locality localityById = this.getLocalityById(localityId);
        return modelMapper.map(localityById, LocalityResponseDto.class);
    }
}
