package com.ungs.calzados.service.report;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

@Service
public class EmailNotificator {

    @Value("${email.from}")
    private String emailFrom;
    private JavaMailSender javaMailSender;

    @Autowired
    public EmailNotificator(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void execute(String subject, JasperPrint bill, String mailTo, Integer accion) throws MessagingException, JRException {
        MimeMessage msg = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        helper.setFrom(emailFrom);
        helper.setTo(mailTo);
        helper.setSubject(subject);

        if (accion == 1){
            String bodyMsg = "<h1>Gracias por tu compra!</h1>";
            helper.setText(bodyMsg, true);
        }
        else{
            String bodyMsg = "<h1>Orden de compra</h1>";
            helper.setText(bodyMsg, true);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(bill, baos);
        DataSource aAttachment = new ByteArrayDataSource(baos.toByteArray(), "application/pdf");

        helper.addAttachment("ZapApp.pdf", aAttachment);

        javaMailSender.send(msg);
    }
}

