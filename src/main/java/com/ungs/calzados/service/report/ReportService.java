package com.ungs.calzados.service.report;

import com.ungs.calzados.dto.PurchaseRequestReportDto;
import com.ungs.calzados.dto.sale.BillRequestReportDto;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import javax.mail.MessagingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class ReportService {

    private final EmailNotificator emailNotificator;

    @Autowired
    public ReportService(EmailNotificator emailNotificator) {
        this.emailNotificator = emailNotificator;
    }


    public void exportReport(BillRequestReportDto bill, String email) throws FileNotFoundException, JRException {

        log.info("Entrando a generar la factura");
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

        try {
            File file = ResourceUtils.getFile("classpath:invoice.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(bill));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametersMap, dataSource);
            emailNotificator.execute("Factura", jasperPrint, email,1);
            log.info("Se envió correctamente la factura al email " + email);
        } catch (JRException ex) {
            log.error("Ocurrió un error mientras se cargaba la factura", ex);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void exportReportPurchase(PurchaseRequestReportDto purchaseRequestReportDto, String email) throws FileNotFoundException, JRException {

        log.info("Entrando a generar la orden de compra");
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

        try {
            File file = ResourceUtils.getFile("classpath:purchaseOrder.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(purchaseRequestReportDto));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametersMap, dataSource);
            emailNotificator.execute("Orden de compra", jasperPrint, email,2);
            log.info("Se envió correctamente la orden de compra a " + email);
        } catch (JRException ex) {
            log.error("Ocurrió un error mientras se cargaba la orden de compra", ex);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
