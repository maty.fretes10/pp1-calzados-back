package com.ungs.calzados.service;

import com.ungs.calzados.dto.ProductModelRequestDto;
import com.ungs.calzados.dto.product.*;
import com.ungs.calzados.entity.product.*;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    void addProduct(ProductRequestDto productRequestDto,
                    Colour colour,
                    ProductModel productModel,
                    ProductStatus productStatus,
                    Waist waist);

    void addManufacturedProduct(Product product, Integer storeId);

    void updateStockManufacturedProduct(Integer productId, Integer quantity);

    Product getProductById(Integer id);

    Optional<Product> getProductByModelColorWaist(Integer modelId, Integer colorId, Integer waistId);

    List<ProductResponseDto> getProducts();

    /**** Color ****/
    void addColour(ColourRequestDto colourRequestDto);
    Colour getColourById(Integer colourId);
    List<ColourResponseDto> getColors();
    void inactivateColour(Integer colorId);
    void editColour(Integer colorId, ColourRequestDto colourRequestDto);

    /**** Talle ****/
    void addWaist(WaistRequestDto waistRequestDto);
    Waist getWaistById(Integer waistId);
    List<WaistResponseDto> getWaist();
    void inactivateWaist(Integer waistId);
    void editWaist (Integer waistId, WaistRequestDto waistRequestDto);

    /**** Estado ****/
    ProductStatus getProductStatusById(Integer productStatusId);
    List<ProductStatusResponseDto> getProductsStatus();

    /**** Modelo ***/
    void addProductModel(ProductModelRequestDto productModelRequestDto);
    ProductModel getProductModelById(Integer productModelId);
    List<ProductModelResponseDto> getProductsModels();
    void disProductModel(Integer modelId);
    void editProductModel(Integer modelId, ProductModelRequestDto request);

}
