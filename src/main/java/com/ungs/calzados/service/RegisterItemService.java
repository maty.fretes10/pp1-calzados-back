package com.ungs.calzados.service;

import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.dto.RegisterItemResponseDto;
import com.ungs.calzados.entity.DailyRegister;

import java.util.List;

public interface RegisterItemService {

    void addItem(String description, MovementType movementType, Double amout, DailyRegister dailyRegister);

    List<RegisterItemResponseDto> listItems(Integer sucursalId);

}
