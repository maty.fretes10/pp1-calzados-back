package com.ungs.calzados.service;

import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.ProductSupplierFavsRequestDto;
import com.ungs.calzados.dto.ProductSupplierFavsResponseDto;

import java.util.List;

public interface ProductSupplierFavsService {

    List<ProductSupplierFavsResponseDto> getProductSupplierFavById(Integer id, FavsType favsType);

    void addFavSupplier(ProductSupplierFavsRequestDto productSupplierFavsRequestDto);

    void delFavProduct(Integer productoId, Integer proveedorId);

    void replaceFavSupplier (ProductSupplierFavsRequestDto productSupplierFavsRequestDto);


}
