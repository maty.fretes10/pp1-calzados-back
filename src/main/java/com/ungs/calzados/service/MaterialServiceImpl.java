package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MaterialState;
import com.ungs.calzados.dto.MaterialRequestDto;
import com.ungs.calzados.dto.MaterialResponseDto;
import com.ungs.calzados.entity.Material;
import com.ungs.calzados.entity.production.StockFactory;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.MaterialRepository;
import com.ungs.calzados.repository.production.StockFactoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MaterialServiceImpl implements MaterialService {

    private final MaterialRepository materialRepository;
    private final ModelMapper modelMapper;
    private final StoreService storeService;
    private final StockFactoryRepository stockFactoryRepository;

    @Autowired
    public MaterialServiceImpl(MaterialRepository materialRepository,
                               ModelMapper modelMapper,
                               StoreService storeService,
                               StockFactoryRepository stockFactoryRepository
    ) {
        this.materialRepository = materialRepository;
        this.modelMapper = modelMapper;
        this.storeService = storeService;
        this.stockFactoryRepository = stockFactoryRepository;
    }

        @Override
        public void addMaterial (MaterialRequestDto materialRequestDto){
            Material entity = modelMapper.map(materialRequestDto, Material.class);
            entity.setState(MaterialState.ACTIVE);
            entity.setCreationDate(LocalDate.now());
            materialRepository.save(entity);


            StockFactory factory = new StockFactory();
            factory.setMaterial(entity);
            factory.setStore(storeService.getStoreById(1));
            factory.setTotalStock(materialRequestDto.getStock());
            stockFactoryRepository.save(factory);
        }


        @Override
        public List<MaterialResponseDto> getMaterials () {
            List<Material> insumos = materialRepository.findAll();
            if (!insumos.isEmpty()) {
                return insumos.stream().parallel().map(material -> modelMapper.map(material,
                        MaterialResponseDto.class)).collect(Collectors.toList());
            }
            log.warn(ErrorConstants.EMPTY_MATERIALS_LIST);
            throw new NoDataFoundException(ErrorConstants.EMPTY_MATERIALS_LIST);
        }

        @Override
        public Material getMaterialById (Integer id){
            final Optional<Material> optionalMaterial = this.materialRepository.findById(id);
            if (!optionalMaterial.isPresent()) {
                String message = String.format(ErrorConstants.MATERIAL_BY_ID_NOT_FOUND, id);
                log.warn(message);
                throw new ResourceNotFoundException(message);
            }
            return optionalMaterial.get();
        }

        @Override
        public MaterialResponseDto getMaterialByIdResponse (Integer materialId){
            Optional<Integer> optionalId = Optional.ofNullable(materialId);
            if (!optionalId.isPresent()) {
                throw new IllegalStateException(ErrorConstants.MATERIAL_BY_ID_IS_NULL);
            }

            Optional<Material> optionalMaterial = this.materialRepository.findById(materialId);
            if (!optionalMaterial.isPresent()) {
                String message = String.format(ErrorConstants.MATERIAL_BY_ID_NOT_FOUND, materialId);
                log.warn(message);
                throw new ResourceNotFoundException(message);
            }

            Material materialFound = optionalMaterial.get();
            log.info("Client with id {} was found", materialId);
            return modelMapper.map(materialFound, MaterialResponseDto.class);
        }

        @Override
        public void inactivateMaterial (Integer idInsumo){
            Optional<Material> insumo = materialRepository.findById(idInsumo);

            if (!insumo.isPresent()) {
                log.warn(ErrorConstants.MATERIAL_BY_ID_IS_NULL);
                throw new ResourceNotFoundException(ErrorConstants.MATERIAL_BY_ID_IS_NULL);
            }

            Material entity = insumo.get();
            if (entity.getState().equals(MaterialState.INACTIVE)) {

                log.warn(ErrorConstants.MATERIAL_BY_STATE_INACTIVE_EXISTS);
                throw new ResourceNotFoundException(ErrorConstants.MATERIAL_BY_STATE_INACTIVE_EXISTS);
            }
            entity.setState(MaterialState.INACTIVE);
            materialRepository.save(entity);
        }

        @Override
        public void updateStockMaterial (Integer materialId, Integer requiredStock){
            Material foundMaterial = getMaterialById(materialId);
            int updateStock = foundMaterial.getStock() - requiredStock;
            foundMaterial.setStock(updateStock);
            materialRepository.save(foundMaterial);
        }

        @Override
        public void returnStockMaterial (Integer materialId, Integer requiredStock){
            Material foundMaterial = getMaterialById(materialId);
            int updateStock = foundMaterial.getStock() + requiredStock;
            foundMaterial.setStock(updateStock);
            materialRepository.save(foundMaterial);
        }


}