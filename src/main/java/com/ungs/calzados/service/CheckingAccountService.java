package com.ungs.calzados.service;

import com.ungs.calzados.constants.AccountType;
import com.ungs.calzados.dto.AccountHistoryResponseDto;
import com.ungs.calzados.dto.CheckingAccountRequestDto;
import com.ungs.calzados.dto.CheckingAccountResponseDto;
import com.ungs.calzados.entity.CheckingAccount;
import com.ungs.calzados.entity.sale.CheckingAccountTransaction;
import com.ungs.calzados.entity.sale.Transaction;

import java.util.List;

public interface CheckingAccountService {
    CheckingAccount addCheckingAccount(CheckingAccount checkingAccount);

    CheckingAccountTransaction generateSaleTransaction(Transaction transaction, Double amount);

    CheckingAccountResponseDto getBalance(AccountType accountType, Integer clientId);

    List<AccountHistoryResponseDto> getAccountHistory(AccountType accountType, Integer clientId);

    void getDeposit(AccountType accountType, Integer clientId, CheckingAccountRequestDto accountRequestDto);
}
