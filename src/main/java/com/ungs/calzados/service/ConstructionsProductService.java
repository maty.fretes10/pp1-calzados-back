package com.ungs.calzados.service;

import com.ungs.calzados.dto.product.ProductRequestDto;
import com.ungs.calzados.entity.product.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Slf4j
@Service
public class ConstructionsProductService {

    private final ProductService productService;
    private final PriceHistoryService priceHistoryService;

    public ConstructionsProductService(ProductService productService, PriceHistoryService priceHistoryService) {
        this.productService = productService;
        this.priceHistoryService = priceHistoryService;
    }

    @Transactional
    public void constructionsSaveProduct(ProductRequestDto productRequest) {

        Colour colourEntity = productService.getColourById(productRequest.getColourId());
        ProductModel productModel = productService.getProductModelById(productRequest.getProductModelId());
        ProductStatus productStatus = productService.getProductStatusById(productRequest.getProductStatusId());
        Waist waist = productService.getWaistById(productRequest.getWaistId());

        productService.addProduct(
                productRequest,
                colourEntity,
                productModel,
                productStatus,
                waist
        );
    }
}
