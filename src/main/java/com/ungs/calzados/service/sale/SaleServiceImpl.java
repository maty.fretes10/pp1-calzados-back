package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.constants.StateSale;
import com.ungs.calzados.dto.sale.CreateSpecificSaleRequestDto;
import com.ungs.calzados.dto.sale.SaleResponseDto;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.entity.sale.*;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.sale.SaleRepository;
import com.ungs.calzados.repository.sale.SaleStoreRepository;
import com.ungs.calzados.repository.sale.SaleWebRepository;
import com.ungs.calzados.repository.sale.TransactionSaleCompositeRepository;
import com.ungs.calzados.service.DailyRegisterService;
import com.ungs.calzados.service.RegisterItemService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SaleServiceImpl implements SaleService {

    private final SaleRepository saleRepository;
    private final SaleStoreRepository saleStoreRepository;
    private final SaleWebRepository saleWebRepository;
    private final ModelMapper modelMapper;
    private final TransactionSaleCompositeRepository transactionSaleCompositeRepository;
    private final TransactionService transactionService;
    private final DailyRegisterService dailyRegisterService;
    private final RegisterItemService registerItemService;

    @Autowired
    public SaleServiceImpl(
            SaleRepository saleRepository,
            SaleStoreRepository saleStoreRepository,
            SaleWebRepository saleWebRepository,
            ModelMapper modelMapper,
            TransactionSaleCompositeRepository transactionSaleCompositeRepository,
            TransactionService transactionService,
            DailyRegisterService dailyRegisterService,
            RegisterItemService registerItemService) {

        this.saleRepository = saleRepository;
        this.saleStoreRepository = saleStoreRepository;
        this.saleWebRepository = saleWebRepository;
        this.modelMapper = modelMapper;
        this.transactionSaleCompositeRepository = transactionSaleCompositeRepository;
        this.transactionService = transactionService;
        this.dailyRegisterService = dailyRegisterService;
        this.registerItemService = registerItemService;
    }

    @Override
    public Sale createStoreSale(CreateSpecificSaleRequestDto saleRequest) {
        Sale saleBaseEntity = createInitialSaveData(saleRequest);
        SaleStore saleStore = new SaleStore(saleBaseEntity);
        saleStore.setEmployee(saleRequest.getEmployee());
        return saleStoreRepository.save(saleStore);
    }

    @Override
    public Sale createWebSale(CreateSpecificSaleRequestDto saleRequest) {
        Sale saleBaseEntity = createInitialSaveData(saleRequest);
        SaleWeb saleWeb = new SaleWeb(saleBaseEntity);
        return saleWebRepository.save(saleWeb);
    }

    @Override
    public void checkExistsSaleById(Integer saleId) {
        String errorMessage = String.format(ErrorConstants.SALE_BY_ID_NOT_EXISTS, saleId);
        boolean existsById = saleRepository.existsById(saleId);

        if (!existsById)
            throw new IllegalArgumentException(errorMessage);
    }

    @Override
    public List<SaleResponseDto> getSale() {
        List<Sale> allSale = (List<Sale>) saleRepository.findAll();
        List<Sale> retorno = new ArrayList<>();
        for (Sale sale: allSale) {
            if(!sale.getState().equals(StateSale.CANCELLED)){
                retorno.add(sale);
            }
        }

        if (retorno.isEmpty()) {
            log.warn(ErrorConstants.EMPTY_SALE_LIST);
            throw new NoDataFoundException(ErrorConstants.EMPTY_SALE_LIST);
        }

        return retorno.stream()
                .parallel()
                .map(sale -> modelMapper.map(sale, SaleResponseDto.class))
                .collect(Collectors.toList());
    }

    private Sale createInitialSaveData(CreateSpecificSaleRequestDto saleRequest) {
        Sale saleBaseEntity = new Sale();
        saleBaseEntity.setCart(saleRequest.getCart());
        saleBaseEntity.setUpdateDate(new Date());
        saleBaseEntity.setState(StateSale.IN_PROGRESS);
        saleBaseEntity.setStore(saleRequest.getStore());
        saleBaseEntity.setSaleType(saleRequest.getSaleType());
        saleBaseEntity.setTotalAmount(saleRequest.getTotalAmount());
        saleBaseEntity.setDiscount(saleRequest.getDiscount());
        saleBaseEntity.setTotalDiscountAmount(saleRequest.getTotalDiscountAmount());

        return saleBaseEntity;
    }

    @Override
    public SaleResponseDto getSaleByIdDto(Integer saleId) {
        Optional<Integer> optionalId = Optional.ofNullable(saleId);
        if (!optionalId.isPresent()) {
            throw new IllegalStateException(ErrorConstants.SALE_BY_ID_NOT_EXISTS);
        }

        Optional<Sale> optionalSale = this.saleRepository.findById(saleId);
        if (!optionalSale.isPresent()) {
            String message = String.format(ErrorConstants.SALE_BY_ID_NOT_EXISTS, saleId);
            log.warn(message);
            throw new ResourceNotFoundException(message);
        }

        Sale sale = optionalSale.get();
        log.info("Sale with id {} was found", saleId);
        return modelMapper.map(sale, SaleResponseDto.class);
    }

    @Override
    public void inactivateSale(Integer saleId) {
        Optional<Sale> optionalSale = saleRepository.findById(saleId);
        if ( !optionalSale.isPresent() ) {
            log.warn("No existe venta con id " + saleId);
            throw new IllegalArgumentException("No existe venta con id " + saleId);
        }
        Sale sale = optionalSale.get();
        if( sale.getState().equals(StateSale.CANCELLED) ){
            log.warn("La venta ya fue cancelada");
            throw new IllegalArgumentException("La venta ya fue cancelada");
        }

        List<TransactionSaleComposite> saleComposite = transactionSaleCompositeRepository.findBySaleId(saleId);
        for (TransactionSaleComposite composite : saleComposite) {
            transactionService.deleteTransaction(composite.getTransaction().getId());
            transactionSaleCompositeRepository.delete(composite);
        }
        sale.setState(StateSale.CANCELLED);
        saleRepository.save(sale);

        DailyRegister cajaAbierta = dailyRegisterService.getDailyOpenBySucursal(sale.getStore().getId());

        for (CartItem cartItem : sale.getCart().getCartItems() ) {
            registerItemService.addItem("Venta "+sale.getId()+" cancelada: COD: " + cartItem.getProduct().getId() + " - " + cartItem.getProduct().getDescription() , MovementType.OUTPUT, (double) cartItem.getProduct().getPriceSale(), cajaAbierta);
        }
    }
}