package com.ungs.calzados.service.sale;

import com.ungs.calzados.entity.sale.CartItem;

import java.util.List;

public interface CartItemService {
    void addAllCartItem(List<CartItem> cartItemList);
}
