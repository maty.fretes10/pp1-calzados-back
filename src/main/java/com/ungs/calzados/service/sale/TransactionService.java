package com.ungs.calzados.service.sale;

import com.ungs.calzados.dto.sale.CreateTransactionRequestDto;
import com.ungs.calzados.entity.sale.Transaction;

public interface TransactionService {
    Transaction saveTransaction(CreateTransactionRequestDto createTransactionRequest);

    void checkExistsTransactionById(Integer transactionId);

    void deleteTransaction (Integer transactionId);
}
