package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.CartState;
import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.entity.sale.Cart;
import com.ungs.calzados.exception.ResourceNotFoundException;
import com.ungs.calzados.repository.sale.CartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public Cart saveCart(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public Cart getCartById(Integer cartTd) {
        final Optional<Cart> optionalCart = this.cartRepository.findById(cartTd);
        String errorMessage = String.format(ErrorConstants.CART_BY_ID_NOT_FOUND, cartTd);

        if (optionalCart.isPresent()) {
            log.info("Card with id {} was found", cartTd);
            return optionalCart.get();
        }

        log.error(errorMessage);
        throw new ResourceNotFoundException(errorMessage);
    }

    @Override
    public void updateCartToCompleted(Cart cartToSaleById) {
        cartToSaleById.setState(CartState.COMPLETED);
        this.saveCart(cartToSaleById);
    }
}
