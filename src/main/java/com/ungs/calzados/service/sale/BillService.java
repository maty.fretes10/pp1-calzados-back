package com.ungs.calzados.service.sale;

import com.ungs.calzados.dto.sale.CreateBillRequestDto;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;

public interface BillService {
    void addBill(CreateBillRequestDto bill) throws JRException, FileNotFoundException;
}
