package com.ungs.calzados.service.sale;


import com.ungs.calzados.entity.sale.Cart;

public interface CartService {
    Cart saveCart(Cart cart);

    Cart getCartById(Integer id);

    void updateCartToCompleted(Cart cartToSaleById);
}
