package com.ungs.calzados.service.sale;


import com.ungs.calzados.repository.sale.TransactionSaleCompositeRepository;
import com.ungs.calzados.dto.sale.TransactionSaleCompositeRequestDto;
import com.ungs.calzados.entity.sale.TransactionSaleComposite;
import com.ungs.calzados.entity.sale.TransactionSalePk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionSaleCompositeServiceImpl implements TransactionSaleCompositeService {
    private final TransactionService transactionService;
    private final SaleService saleService;
    private final TransactionSaleCompositeRepository transactionSaleCompositeRepository;

    @Autowired
    public TransactionSaleCompositeServiceImpl(
            TransactionService transactionService,
            SaleService saleService,
            TransactionSaleCompositeRepository transactionSaleCompositeRepository) {

        this.transactionService = transactionService;
        this.saleService = saleService;
        this.transactionSaleCompositeRepository = transactionSaleCompositeRepository;
    }

    @Override
    public void save(TransactionSaleCompositeRequestDto requestDto) {

        Integer transactionId = requestDto.getTransactionId();
        Integer saleId = requestDto.getSaleId();

        transactionService.checkExistsTransactionById(transactionId);
        saleService.checkExistsSaleById(saleId);

        TransactionSalePk pk = new TransactionSalePk(transactionId, saleId);
        TransactionSaleComposite transactionSaleComposite = new TransactionSaleComposite(pk);

        transactionSaleCompositeRepository.save(transactionSaleComposite);
    }

    @Override
    public List<TransactionSaleComposite> getTransactionSaleById(Integer saleId) {
        List<TransactionSaleComposite> optionalSale = transactionSaleCompositeRepository.findBySaleId(saleId);
        if(optionalSale.isEmpty()){
            throw new IllegalArgumentException("No existe transacciones con esa venta");
        }
        return optionalSale;
    }
}
