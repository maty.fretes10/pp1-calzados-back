package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.CartState;
import com.ungs.calzados.dto.PurchaseOrderItemRequestDto;
import com.ungs.calzados.dto.PurchaseOrderRequestDto;
import com.ungs.calzados.dto.product.ProductStockRequestDto;
import com.ungs.calzados.dto.sale.CartRequestDto;
import com.ungs.calzados.dto.sale.CartSavedResponseDto;
import com.ungs.calzados.entity.ProductSupplierFavs;
import com.ungs.calzados.entity.StockStore;
import com.ungs.calzados.entity.product.Product;
import com.ungs.calzados.entity.sale.Cart;
import com.ungs.calzados.entity.sale.CartItem;
import com.ungs.calzados.exception.NotEnoughStockException;
import com.ungs.calzados.exception.StockErrorObject;
import com.ungs.calzados.exception.StockSingleErrorObject;
import com.ungs.calzados.repository.ProductSupplierFavsRepository;
import com.ungs.calzados.service.ProductService;
import com.ungs.calzados.service.PurchaseOrderService;
import com.ungs.calzados.service.StockStoreService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ConstructionsCartService {

    private final StockStoreService stockStoreService;
    private final CartService cartService;
    private final CartItemService cartItemService;
    private static final CartState DEFAULT_CART_STATE = CartState.ACTIVE;
    private final ProductService productService;
    private final ProductSupplierFavsRepository productSupplierFavsRepository;
    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public ConstructionsCartService(
            StockStoreService stockStoreService,
            CartService cartService,
            CartItemService cartItemService,
            ProductService productService,
            ProductSupplierFavsRepository productSupplierFavsRepository,
            PurchaseOrderService purchaseOrderService) {

        this.stockStoreService = stockStoreService;
        this.cartService = cartService;
        this.cartItemService = cartItemService;
        this.productService = productService;
        this.productSupplierFavsRepository = productSupplierFavsRepository;
        this.purchaseOrderService = purchaseOrderService;
    }

    @Transactional
    public CartSavedResponseDto addCart(CartRequestDto cartRequest) {

        List<StockSingleErrorObject> errorList = new ArrayList<>();
        List<CartItem> cartItemList = new ArrayList<>();
        Cart cartToSave = Cart.builder().state(DEFAULT_CART_STATE).build();

        List<Integer> productsRequiredIdList = cartRequest.getProducts()
                .stream()
                .parallel()
                .map(ProductStockRequestDto::getProductId)
                .collect(Collectors.toList());
        List<StockStore> availableProductsInStock =
                stockStoreService.findByIdInList(cartRequest.getStoreId(), productsRequiredIdList);

        availableProductsInStock.stream().forEach(availableProductInStock -> {
            ProductStockRequestDto productStockRequest =
                    cartRequest.getProducts().stream()
                            .filter(prod -> prod.getProductId()
                                    .equals(availableProductInStock.getId()))
                            .findFirst().get();

            if (productStockRequest.getRequiredStock() <= availableProductInStock.getTotalStock()) {
                try {
                    updateStockInStore(availableProductInStock, productStockRequest);
                } catch (JRException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                fillCartItemList(cartItemList, cartToSave, availableProductInStock, productStockRequest);

            } else {
                errorList.add(StockSingleErrorObject.builder()
                        .productId(availableProductInStock.getId())
                        .availableStock(availableProductInStock.getTotalStock())
                        .requiredStock(productStockRequest.getRequiredStock())
                        .build());
            }
        });

        if (!errorList.isEmpty())
            throw new NotEnoughStockException(StockErrorObject.builder().errors(errorList).build());

        Cart cartSaved = cartService.saveCart(cartToSave);
        cartItemService.addAllCartItem(cartItemList);
        return CartSavedResponseDto.builder().cartId(cartSaved.getId()).build();
    }


    private void updateStockInStore(StockStore availableProductInStock, ProductStockRequestDto productStockRequest) throws JRException, FileNotFoundException {
        int newStockValue = availableProductInStock.getTotalStock() - productStockRequest.getRequiredStock();
        availableProductInStock.setTotalStock(newStockValue);

        Product product = productService.getProductById(productStockRequest.getProductId());
        if(newStockValue <= product.getStockMinimum() ){
            Optional<ProductSupplierFavs> productSupplierFavs = productSupplierFavsRepository.findByProductIdOpcional(product.getId());
            if(productSupplierFavs.isPresent()){
                PurchaseOrderRequestDto orderRequestDto = new PurchaseOrderRequestDto();
                orderRequestDto.setStoreId(availableProductInStock.getStore().getId());
                orderRequestDto.setSupplierId(productSupplierFavs.get().getSupplier().getId());

                PurchaseOrderItemRequestDto itemRequestDto = new PurchaseOrderItemRequestDto();
                itemRequestDto.setDetail(product.getDescription());
                itemRequestDto.setQuantity(product.getStockSecurity());
                itemRequestDto.setUnitPrice(product.getPriceBuy());
                List<PurchaseOrderItemRequestDto> list = new ArrayList<>();
                list.add(itemRequestDto);
                orderRequestDto.setItems(list);
                purchaseOrderService.addPurchaseOrder(orderRequestDto);
            }
        }

        stockStoreService.save(availableProductInStock);
    }

    private void fillCartItemList(List<CartItem> cartItemList, Cart cart, StockStore availableProductInStock, ProductStockRequestDto productStockRequest) {
        cartItemList.add(
                CartItem.builder()
                        .cart(cart)
                        .itemCount(productStockRequest.getRequiredStock())
                        .product(availableProductInStock.getProduct())
                        .subTotal((double) (productStockRequest.getRequiredStock() * availableProductInStock.getProduct().getPriceSale())).build());
    }
}
