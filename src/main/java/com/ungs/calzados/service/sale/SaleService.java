package com.ungs.calzados.service.sale;

import com.ungs.calzados.dto.sale.CreateSpecificSaleRequestDto;
import com.ungs.calzados.dto.sale.SaleResponseDto;
import com.ungs.calzados.entity.sale.Sale;

import java.util.List;


public interface SaleService {
    Sale createStoreSale(CreateSpecificSaleRequestDto saleRequest);

    Sale createWebSale(CreateSpecificSaleRequestDto saleRequest);

    void checkExistsSaleById(Integer saleId);

    List<SaleResponseDto> getSale();

    void inactivateSale(Integer saleId);

    SaleResponseDto getSaleByIdDto(Integer saleId);
}
