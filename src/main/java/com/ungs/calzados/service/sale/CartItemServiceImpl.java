package com.ungs.calzados.service.sale;

import com.ungs.calzados.entity.sale.CartItem;
import com.ungs.calzados.repository.sale.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository cartItemRepository;

    @Autowired
    public CartItemServiceImpl(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }


    @Override
    public void addAllCartItem(List<CartItem> cartItemList) {
        cartItemRepository.saveAll(cartItemList);
    }
}
