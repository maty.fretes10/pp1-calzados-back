package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.TransactionType;
import com.ungs.calzados.dto.sale.BillRequestReportDto;
import com.ungs.calzados.dto.sale.CreateBillRequestDto;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.sale.*;
import com.ungs.calzados.repository.sale.BillItemRepository;
import com.ungs.calzados.repository.sale.BillPayRepository;
import com.ungs.calzados.repository.sale.BillRepository;
import com.ungs.calzados.repository.sale.CartItemRepository;
import com.ungs.calzados.service.report.ReportService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class BillServiceImpl implements BillService {

    private final ReportService reportService;
    private final BillRepository billRepository;
    private final BillItemRepository billItemRepository;
    private final BillPayRepository billPayRepository;
    private final ModelMapper modelMapper;
    private static final Character TYPE_BILL_A = 'A';
    private static final Character TYPE_BILL_B = 'B';
    private final CartItemRepository cartItemRepository;

    @Autowired
    public BillServiceImpl(
            ReportService reportService,
            BillRepository billRepository,
            BillItemRepository billItemRepository,
            BillPayRepository billPayRepository,
            ModelMapper modelMapper,
            CartItemRepository cartItemRepository) {

        this.reportService = reportService;
        this.billRepository = billRepository;
        this.billItemRepository = billItemRepository;
        this.billPayRepository = billPayRepository;
        this.modelMapper = modelMapper;
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public void addBill(CreateBillRequestDto bill) throws JRException, FileNotFoundException {
        Bill entity = new Bill();
        defineTypeofBill(entity, bill.getClient());
        entity.setClientId(bill.getClient().getId());
        entity.setClientName(bill.getClient().getSurname() + ", " + bill.getClient().getName());
        entity.setClientAddress(bill.getClient().getStreet() + " " + bill.getClient().getStreetNumber() + ", " + bill.getClient().getLocality().getName());
        entity.setClientIdentification(bill.getClient().getDocumentNumber());
        entity.setClientCategoryAfip(bill.getClient().getCategoryAfip().getName());
        entity.setStoreId(bill.getSale().getStore().getId());
        entity.setStoreAddress(bill.getSale().getStore().getStreet() + " " + bill.getSale().getStore().getStreetNumber());
        entity.setDiscountName(defineDiscountName(bill.getSale().getDiscount()));
        billRepository.save(entity);
        entity.setItems(defineItems(bill, entity));
        entity.setDiscount(((entity.getSubtotal() + entity.getIva()) * bill.getSale().getDiscount()) / 100);
        entity.setPayments(definePayments(bill, entity));
        entity.setTotal(entity.getSubtotal() + entity.getIva() - entity.getDiscount());
        billRepository.save(entity);
        BillRequestReportDto billRequestReportDto = modelMapper.map(entity, BillRequestReportDto.class);
        reportService.exportReport(billRequestReportDto, bill.getClient().getEmail());
    }

    private void defineTypeofBill(Bill entity, Client client) {
        if (client.getCategoryAfip().getId() == 1) {
            entity.setTypeBill(TYPE_BILL_A);
            entity.setIvaPercentage(0.21);
        } else {
            entity.setTypeBill(TYPE_BILL_B);
            entity.setIvaPercentage(0);
        }
    }

    private List<BillItem> defineItems(CreateBillRequestDto bill, Bill billById) {
        List<BillItem> listBillItems = new ArrayList<>();
        List<CartItem> cartItems = cartItemRepository.findByCartId(bill.getSale().getCart().getId());
        double discountIva = 0;
        double subtotal = 0;

        for (CartItem cartItem : cartItems) {
            BillItem billItem = new BillItem();
            billItem.setBill(billById);
            billItem.setProductId(cartItem.getProduct().getId());
            billItem.setProductId(cartItem.getProduct().getId());
            billItem.setDetail(cartItem.getProduct().getDescription() +" "+ cartItem.getProduct().getProductModel().getName() + " "+ cartItem.getProduct().getProductModel().getMark());
            billItem.setUnitPrice(cartItem.getProduct().getPriceSale() - (cartItem.getProduct().getPriceSale() * billById.getIvaPercentage()));
            billItem.setQuantity(cartItem.getItemCount());
            billItem.setTotalPrice(cartItem.getSubTotal() - (cartItem.getSubTotal() * billById.getIvaPercentage()));
            discountIva += (cartItem.getSubTotal() * billById.getIvaPercentage());
            subtotal += (billItem.getTotalPrice());
            listBillItems.add(billItem);
            billItemRepository.save(billItem);
        }
        billById.setIva(discountIva);
        billById.setSubtotal(subtotal);
        return listBillItems;
    }

    private List<BillPay> definePayments(CreateBillRequestDto bill, Bill billById) {
        List<BillPay> listBillPay = new ArrayList<>();
        List<Transaction> transaction = bill.getTransaction();

        for (Transaction transaction1 : transaction) {
            BillPay billPay = new BillPay();
            billPay.setBill(billById);
            billPay.setPaymentMethod(defineTransaction(transaction1.getTransactionType()));
            log.info("Medio de pago: " + defineTransaction(transaction1.getTransactionType()));
            billPay.setAmount(transaction1.getAmount());
            listBillPay.add(billPay);
            billPayRepository.save(billPay);
        }
        return listBillPay;
    }

    private String defineTransaction(TransactionType transactionType) {
        if (transactionType.equals(TransactionType.CREDIT_CARD)) {
            return "Tarjeta de credito ";
        } else if (transactionType.equals(TransactionType.ACCOUNT)) {
            return "Cuenta Corriente";
        } else {
            return "Efectivo";
        }
    }

    private String defineDiscountName(Double discount) {
        if (discount > 0) {
            return "Descuento del " + discount + "% aplicado";
        }
        return "Sin descuento aplicado";
    }

}
