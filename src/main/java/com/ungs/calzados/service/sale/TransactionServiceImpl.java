package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.AccountType;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.constants.TransactionState;
import com.ungs.calzados.constants.TransactionType;
import com.ungs.calzados.dto.CheckingAccountRequestDto;
import com.ungs.calzados.dto.sale.CreateTransactionRequestDto;
import com.ungs.calzados.dto.sale.CreditCardRequestDto;
import com.ungs.calzados.entity.sale.CashTransaction;
import com.ungs.calzados.entity.sale.CheckingAccountTransaction;
import com.ungs.calzados.entity.sale.CreditCardTransaction;
import com.ungs.calzados.entity.sale.Transaction;
import com.ungs.calzados.exception.TransactionException;
import com.ungs.calzados.repository.sale.CashTransactionRepository;
import com.ungs.calzados.repository.sale.CreditCardTransactionRepository;
import com.ungs.calzados.repository.sale.TransactionRepository;
import com.ungs.calzados.service.CheckingAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    private final CheckingAccountService checkingAccountService;
    private final CreditCardTransactionRepository creditCardTransactionRepository;
    private final CashTransactionRepository cashTransactionRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(
            CheckingAccountService checkingAccountService,
            CreditCardTransactionRepository creditCardTransactionRepository,
            CashTransactionRepository cashTransactionRepository,
            TransactionRepository transactionRepository) {

        this.checkingAccountService = checkingAccountService;
        this.creditCardTransactionRepository = creditCardTransactionRepository;
        this.cashTransactionRepository = cashTransactionRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction saveTransaction(CreateTransactionRequestDto createTransactionRequest) {
        TransactionType transactionType = createTransactionRequest.getTransactionType();
        Transaction transactionToSave = new Transaction();
        Transaction savedTransaction = null;

        transactionToSave.setClient(createTransactionRequest.getClient());
        transactionToSave.setAmount(createTransactionRequest.getSpecificRequestDetails().getAmount());
        transactionToSave.setCreationDate(new Date());
        transactionToSave.setTransactionType(transactionType);
        transactionToSave.setTransactionState(TransactionState.APPROVED);

        if (transactionType.equals(TransactionType.CREDIT_CARD)) {
            CreditCardTransaction creditCardTransaction = new CreditCardTransaction(transactionToSave);
            CreditCardRequestDto creditCard = createTransactionRequest.getSpecificRequestDetails().getCreditCard();

            creditCardTransaction.setCardNumber(creditCard.getCardNumber());
            creditCardTransaction.setCardType(creditCard.getCardType());
            savedTransaction = creditCardTransactionRepository.save(creditCardTransaction);

        } else if (transactionType.equals(TransactionType.ACCOUNT)) {
            CheckingAccountTransaction checkingAccountTransaction = new CheckingAccountTransaction(transactionToSave);
            checkingAccountTransaction.setMovementType(MovementType.OUTPUT);
            savedTransaction = checkingAccountService.generateSaleTransaction(
                    checkingAccountTransaction,
                    createTransactionRequest.getSpecificRequestDetails().getAmount());

        } else {
            CashTransaction cashTransaction = new CashTransaction(transactionToSave);
            cashTransaction.setDescription(createTransactionRequest.getSpecificRequestDetails().getDescription());
            savedTransaction = cashTransactionRepository.save(cashTransaction);
        }

        return savedTransaction;
    }

    @Override
    public void checkExistsTransactionById(Integer transactionId) {
        String errorMessage = String.format("Transacción con id %s no existe", transactionId);
        boolean existsById = transactionRepository.existsById(transactionId);
        if (!existsById)
            throw new TransactionException(errorMessage);
    }

    @Override
    public void deleteTransaction(Integer transactionId) {
        Optional<Transaction> transactionOptional = transactionRepository.findById(transactionId);
        if(!transactionOptional.isPresent()){
            throw new IllegalArgumentException("La transaccion que se busca no fue encontrada");
        }
        Transaction transaction = transactionOptional.get();
        log.info("Por rechazar la transaccion " + transaction.getId());

        if(transaction.getTransactionType().equals(TransactionType.ACCOUNT)){
            CheckingAccountRequestDto requestDto = new CheckingAccountRequestDto();
            requestDto.setBalance(transaction.getAmount());
            checkingAccountService.getDeposit(AccountType.CLIENT, transaction.getClient().getId(),requestDto);
        }

        transaction.setTransactionState(TransactionState.REJECTED);
        transactionRepository.save(transaction);
    }
}
