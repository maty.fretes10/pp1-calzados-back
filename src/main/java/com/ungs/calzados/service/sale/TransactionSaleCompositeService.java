package com.ungs.calzados.service.sale;

import com.ungs.calzados.dto.sale.TransactionSaleCompositeRequestDto;
import com.ungs.calzados.entity.sale.TransactionSaleComposite;

import java.util.List;

public interface TransactionSaleCompositeService {
    void save(TransactionSaleCompositeRequestDto requestDto);

    List<TransactionSaleComposite> getTransactionSaleById(Integer saleId);
}
