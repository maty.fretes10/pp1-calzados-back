package com.ungs.calzados.service.sale;

import com.ungs.calzados.constants.CartState;
import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.constants.SaleType;
import com.ungs.calzados.dto.product.ProductStockRequestDto;
import com.ungs.calzados.dto.sale.*;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.DailyRegister;
import com.ungs.calzados.entity.StockStore;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Product;
import com.ungs.calzados.entity.sale.Cart;
import com.ungs.calzados.entity.sale.CartItem;
import com.ungs.calzados.entity.sale.Sale;
import com.ungs.calzados.entity.sale.Transaction;
import com.ungs.calzados.exception.SaleException;
//import com.ungs.calzados.login.Usuario;
//import com.ungs.calzados.login.ignore.UserService;
import com.ungs.calzados.service.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
@Slf4j
public class ConstructionsSaleService {


    private final ConstructionsCartService orchestratedCartTasksService;
    private final CartService cartService;
    private final StoreService storeService;
    private final ClientService clientService;
    private final TransactionService transactionService;
    private final SaleService saleService;
    private final TransactionSaleCompositeService transactionSaleCompositeService;
    private final ConstructionsBillService constructionsBillService;
    private static final Double INITIAL_DISCOUNT = 0d;
    private static final Double TOTAL_INITIAL_DISCOUNT_AMOUNT = 0d;
    private static final String SALE_CREATED_OK_MESSAGE = "Se creó correctamente la venta de tipo con id {} de tipo {}";
    private final RegisterItemService registerItemService;
    private final DailyRegisterService dailyRegisterService;
    private final ProductService productService;

    //    private final UserService userService;

    @Autowired
    public ConstructionsSaleService(
            ConstructionsCartService orchestratedCartTasksService,
            CartService cartService,
            StoreService storeService,
            ClientService clientService,
            TransactionSaleCompositeService transactionSaleCompositeService,
            TransactionService transactionService,
            ConstructionsBillService constructionsBillService,
            SaleService saleService,
            RegisterItemService registerItemService,
            DailyRegisterService dailyRegisterService,
            ProductService productService) {

        this.orchestratedCartTasksService = orchestratedCartTasksService;
        this.cartService = cartService;
        this.storeService = storeService;
        this.clientService = clientService;
        this.transactionService = transactionService;
        this.saleService = saleService;
        this.transactionSaleCompositeService = transactionSaleCompositeService;
        this.constructionsBillService = constructionsBillService;
        //this.userService = userService;
        this.registerItemService = registerItemService;
        this.dailyRegisterService = dailyRegisterService;
        this.productService = productService;
    }

    @Transactional
    public CreateSaleResponseDto createStoreSale(CreateStoreSaleRequestDto createSaleRequest) throws Exception {
        log.info("Create store sale orchestrated service");

        SaleType saleType = SaleType.STORE;
        Client clientFoundById = clientService.getClientByID(createSaleRequest.getClientId());
        Store storeById = storeService.getStoreById(createSaleRequest.getStoreId());
        Cart cartToSaleById = cartService.getCartById(createSaleRequest.getCartId());
        Double totalAmountWithDiscount = createSaleRequest.getTotalAmount() - createSaleRequest.getTotalDiscountAmount();

        this.checkActiveCart(cartToSaleById);
        this.checkTotalAmountMatchTransactionsTotal(totalAmountWithDiscount, createSaleRequest.getTransactions());

        Sale saleSaved = this.saleService.createStoreSale(
                this.createSaleStoreToSaveRequest(
                        saleType,
                        createSaleRequest.getEmployeeId(),
                        clientFoundById,
                        cartToSaleById,
                        storeById,
                        totalAmountWithDiscount,
                        createSaleRequest.getDiscount(),
                        createSaleRequest.getTotalDiscountAmount()));

        List<Transaction> transactionsSaved =
                this.saveTransactions(clientFoundById, cartToSaleById, storeById, saleSaved, createSaleRequest.getTransactions());

        updateCartToCompleted(cartToSaleById);

        log.info(SALE_CREATED_OK_MESSAGE, saleSaved.getId(), saleType);

        constructionsBillService.createBill(saleSaved, clientFoundById, transactionsSaved);

        Integer storeId = createSaleRequest.getStoreId();

        Cart carrito = cartService.getCartById(createSaleRequest.getCartId());

        DailyRegister cajaAbierta = dailyRegisterService.getDailyOpenBySucursal(storeId);

        for (int i=0;i<carrito.getCartItems().size(); i++){
            CartItem item = carrito.getCartItems().get(i);
            Integer itemsVendidos = item.getItemCount();

            item.getProduct().setStockConsolidated(item.getProduct().getStockConsolidated()-itemsVendidos);



            registerItemService.addItem("Venta Local - COD: "+item.getProduct().getId() + " / "+ item.getProduct().getProductModel().getMark()+ " - " + item.getProduct().getProductModel().getName() + " / " + item.getProduct().getDescription(), MovementType.INPUT, (double) item.getProduct().getPriceSale(), cajaAbierta);
        }


        return CreateSaleResponseDto.builder().saleId(saleSaved.getId()).build();
    }

    @Transactional
    public CreateSaleResponseDto createWebSale(CreateWebSaleRequestDto createSaleRequest) throws FileNotFoundException, JRException {
        log.info("Create store sale orchestrated service");

        Double totalAmount = createSaleRequest.getTotalAmount();
        SaleType saleType = SaleType.WEB;
        Client clientFoundById = clientService.getClientByID(createSaleRequest.getClientId());
        Store storeById = storeService.getStoreById(createSaleRequest.getStoreId());
        CartSavedResponseDto cartSavedResponse = orchestratedCartTasksService.addCart(
                CartRequestDto.builder()
                        .storeId(createSaleRequest.getStoreId())
                        .products(createSaleRequest.getProducts())
                        .build());
        Cart cartById = cartService.getCartById(cartSavedResponse.getCartId());

        this.checkTotalAmountMatchTransactionsTotal(totalAmount, createSaleRequest.getTransactions());

        Sale saleSaved = this.saleService.createWebSale(this.createSpecificSaleRequestDto(
                saleType,
                clientFoundById,
                cartById,
                storeById,
                createSaleRequest.getTotalAmount(),
                INITIAL_DISCOUNT,
                TOTAL_INITIAL_DISCOUNT_AMOUNT));

        List<Transaction> transactionsSaved = this.saveTransactions(clientFoundById, cartById, storeById, saleSaved, createSaleRequest.getTransactions());

        updateCartToCompleted(cartById);

        log.info(SALE_CREATED_OK_MESSAGE, saleSaved.getId(), saleType);

        Integer storeId = createSaleRequest.getStoreId();

        List<ProductStockRequestDto> products = createSaleRequest.getProducts();

        DailyRegister cajaAbierta = dailyRegisterService.getDailyOpenBySucursal(storeId);

        for (int i=0;i<products.size(); i++){
            Integer productId = products.get(i).getProductId();
            Product product = productService.getProductById(productId);
            registerItemService.addItem("Venta Web - COD: "+product.getId() + " / "+ product.getProductModel().getMark()+ " - " + product.getProductModel().getName() + " / " + product.getDescription(), MovementType.INPUT, (double) product.getPriceSale(), cajaAbierta);
        }

        constructionsBillService.createBill(saleSaved, clientFoundById, transactionsSaved);

        return CreateSaleResponseDto.builder().saleId(saleSaved.getId()).build();
    }

    private void checkActiveCart(Cart cartToSaleById) {

        String errorMessage = String.format(ErrorConstants.CART_BY_ID_NOT_ACTIVE, cartToSaleById.getId());

        if (!CartState.ACTIVE.equals(cartToSaleById.getState())) {
            log.error(errorMessage);
            throw new SaleException(errorMessage);
        }
    }

    private void updateCartToCompleted(Cart cartToSaleById) {
        cartService.updateCartToCompleted(cartToSaleById);
    }

    private void checkTotalAmountMatchTransactionsTotal(
            Double totalAmount,
            List<SpecificTransactionRequestDetailsRequestDto> transactionRequests) {

        Double transactionsAmountTotal =
                transactionRequests.stream()
                        .map(SpecificTransactionRequestDetailsRequestDto::getAmount)
                        .reduce(0d, Double::sum);

        if (transactionsAmountTotal.compareTo(totalAmount) != 0) {
            log.error(ErrorConstants.TOTAL_AMOUNT_NOT_MATCH);
            throw new SaleException(ErrorConstants.TOTAL_AMOUNT_NOT_MATCH);
        }
    }

    private List<Transaction> saveTransactions(
            Client client,
            Cart cart,
            Store store,
            Sale sale,
            List<SpecificTransactionRequestDetailsRequestDto> transactions) {

        List<Transaction> transactionReturn = new ArrayList<>();

        CreateTransactionRequestDto transactionRequest = CreateTransactionRequestDto.builder()
                .client(client)
                .cart(cart)
                .store(store)
                .sale(sale)
                .build();

        transactions.stream().map(singleTransactionRequest -> {
            transactionRequest.setSpecificRequestDetails(singleTransactionRequest);
            transactionRequest.setTransactionType(singleTransactionRequest.getTransactionType());

            return transactionRequest;
        }).forEach(mapTransactionRequest -> {
            Transaction transaction = transactionService.saveTransaction(mapTransactionRequest);
            transactionReturn.add(transaction);
            saveTransactionAndRelationWithSale(sale, transaction);
        });

        return transactionReturn;
    }

    private void saveTransactionAndRelationWithSale(Sale sale, Transaction transaction) {
        TransactionSaleCompositeRequestDto request = TransactionSaleCompositeRequestDto.builder().
                transactionId(transaction.getId()).
                saleId(sale.getId()).
                build();

        transactionSaleCompositeService.save(request);
    }

    private CreateSpecificSaleRequestDto createSaleStoreToSaveRequest(
            SaleType saleType,
            Integer employeeId,
            Client client,
            Cart cart,
            Store store,
            Double totalAmount,
            Double discount, Double totalDiscountAmount) throws Exception {

        CreateSpecificSaleRequestDto request =
                createSpecificSaleRequestDto(saleType, client, cart, store, totalAmount, discount, totalDiscountAmount);

        Long idUser = Long.valueOf(employeeId);
//        Usuario employeeFoundById = userService.getUserById(idUser);
//        request.setEmployee(employeeFoundById.getUsername());

        return request;
    }

    private CreateSpecificSaleRequestDto createSpecificSaleRequestDto(
            SaleType saleType,
            Client client,
            Cart cart,
            Store store,
            Double totalAmount,
            Double discount,
            Double totalDiscountAmount
    ) {

        return CreateSpecificSaleRequestDto.builder()
                .saleType(saleType)
                .client(client)
                .cart(cart)
                .store(store)
                .totalAmount(totalAmount)
                .discount(discount)
                .totalDiscountAmount(totalDiscountAmount)
                .build();
    }
}
