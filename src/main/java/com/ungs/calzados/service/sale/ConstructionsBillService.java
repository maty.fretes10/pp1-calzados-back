package com.ungs.calzados.service.sale;

import com.ungs.calzados.dto.sale.CreateBillRequestDto;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.sale.Sale;
import com.ungs.calzados.entity.sale.Transaction;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.util.List;

@Service
@Slf4j
public class ConstructionsBillService {

    private final BillService billService;

    @Autowired
    public ConstructionsBillService(BillService billService) {
        this.billService = billService;
    }

    @Transactional
    public void createBill(Sale sale, Client client, List<Transaction> transaction) throws JRException, FileNotFoundException {
        log.info("creando factura para la venta " + sale.getId());
        billService.addBill(this.createBillRequest(sale, client, transaction));
    }

    private CreateBillRequestDto createBillRequest(Sale sale, Client client, List<Transaction> transaction) {
        return CreateBillRequestDto.builder().sale(sale).client(client).transaction(transaction).build();
    }
}
