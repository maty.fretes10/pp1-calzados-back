package com.ungs.calzados.service;

import com.ungs.calzados.constants.ErrorConstants;
import com.ungs.calzados.entity.CashRegister;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.repository.CashRegisterRepository;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CashRegisterServiceImpl implements CashRegisterService {

    private final CashRegisterRepository cashRegisterRepository;

    @Autowired
    public CashRegisterServiceImpl(CashRegisterRepository cashRegisterRepository) {
        this.cashRegisterRepository = cashRegisterRepository;
    }

    @Override
    public void addCashRegister(CashRegister cashRegister, Store store) {
        cashRegister.setStore(store);
        cashRegisterRepository.save(cashRegister);
    }

    @Override
    public CashRegister getCashRegisterByIdSucursal(Integer sucursalId) {
        Optional<CashRegister> caja = cashRegisterRepository.findByStore(sucursalId);

        if(!caja.isPresent()){
            String mensaje = String.format(ErrorConstants.STORE_BY_ID_NOT_FOUND, sucursalId);
            Log.warn(mensaje);
            throw new IllegalStateException(mensaje);
        }

        return caja.get();
    }

}
