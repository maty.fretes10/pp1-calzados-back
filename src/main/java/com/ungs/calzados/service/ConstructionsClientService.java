package com.ungs.calzados.service;

import com.ungs.calzados.dto.ClientRequestDto;
import com.ungs.calzados.entity.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ConstructionsClientService {

    private final ClientService clientService;
    private final CheckingAccountService checkingAccountService;
    private final AccountHistoryService accountHistoryService;
    private final IdentificationService identificationService;
    private final LocationService locationService;
    private static final Double DEFAULT_EMPTY_BALANCE = 0d;

    public ConstructionsClientService(
            ClientService clientService,
            CheckingAccountService checkingAccountService,
            AccountHistoryService accountHistoryService,
            IdentificationService identificationService,
            LocationService locationService
    ) {

        this.clientService = clientService;
        this.checkingAccountService = checkingAccountService;
        this.accountHistoryService = accountHistoryService;
        this.identificationService = identificationService;
        this.locationService = locationService;
    }

    @Transactional
    public void constructionsSaveClient(ClientRequestDto clientRequest) {

        CheckingAccount checkingAccountEntitySaved = checkingAccountService
                .addCheckingAccount(CheckingAccount.builder().balance(DEFAULT_EMPTY_BALANCE).build());

        accountHistoryService.addAccountHistory(AccountHistory.builder().money(0).build(), "INPUT", checkingAccountEntitySaved);

        DocumentType documentTypeEntityChosen =
                identificationService.getDocumentTypeEntity(clientRequest.getDocumentTypeId());
        CategoryAfip categoryAfipEntity =
                identificationService.getCategoryAfipById(clientRequest.getCategoryAfipId());
        Locality localityEntityFound =
                locationService.getLocalityById(clientRequest.getLocalityId());

        clientService.addClient(
                clientRequest,
                checkingAccountEntitySaved,
                documentTypeEntityChosen,
                categoryAfipEntity,
                localityEntityFound
        );
    }

    public void constructionsEditClient(Integer clientId,ClientRequestDto request) {
        DocumentType documentTypeEntityChosen =
                identificationService.getDocumentTypeEntity(request.getDocumentTypeId());
        CategoryAfip categoryAfipEntity =
                identificationService.getCategoryAfipById(request.getCategoryAfipId());
        Locality localityEntityFound =
                locationService.getLocalityById(request.getLocalityId());

        clientService.editClient(
                clientId,
                request,
                documentTypeEntityChosen,
                categoryAfipEntity,
                localityEntityFound
        );
    }
}





