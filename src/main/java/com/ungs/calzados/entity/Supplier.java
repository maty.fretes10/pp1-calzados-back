package com.ungs.calzados.entity;


import com.ungs.calzados.constants.SupplierState;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "suppliers")
@Builder

public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String email;
    @Enumerated(EnumType.STRING)
    private SupplierState state;
    private String street;
    @Column (name="street_number")
    private Integer streetNumber;
    private String telephone;
    private String cuit;
    @ManyToOne
    @JoinColumn(name="locality_Id", referencedColumnName = "id")
    private Locality locality;
    @OneToOne
    @JoinColumn(name="checking_account_id",referencedColumnName = "id")
    private CheckingAccount checkingAccount;


}
