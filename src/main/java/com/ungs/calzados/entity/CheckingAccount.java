package com.ungs.calzados.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "checking_accounts")
public class CheckingAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double balance;

    @OneToOne(mappedBy = "checkingAccount")
    private Client client;

    @OneToOne(mappedBy = "checkingAccount")
    private Supplier supplier;


}
