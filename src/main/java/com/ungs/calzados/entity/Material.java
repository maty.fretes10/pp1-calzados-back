package com.ungs.calzados.entity;

import com.ungs.calzados.constants.MaterialState;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "materials")
@Builder

public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    @Column (name="creation_date", nullable = false, updatable = false)
    private LocalDate creationDate;
    private Integer stock;
    @Column (name="stock_min")
    private Integer stockMin;
    @Enumerated (EnumType.STRING)
    private MaterialState state;
    @Column (name="price_buy")
    private float priceBuy;

}
