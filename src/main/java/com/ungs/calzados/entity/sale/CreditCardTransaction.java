package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Entity
@Table(name = "credit_card_transaction")
@PrimaryKeyJoinColumn(name = "transaction_id")
public class CreditCardTransaction extends Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "card_number")
    private String cardNumber;
    @Column(name = "card_type")
    private String cardType;

    public CreditCardTransaction(Transaction tr) {
        setId(tr.getId());
        setTransactionType(tr.getTransactionType());
        setClient(tr.getClient());
        setTransactionState(tr.getTransactionState());
        setCreationDate(tr.getCreationDate());
        setAmount(tr.getAmount());
    }
}
