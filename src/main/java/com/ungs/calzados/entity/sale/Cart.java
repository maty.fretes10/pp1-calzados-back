package com.ungs.calzados.entity.sale;

import com.ungs.calzados.constants.CartState;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "carts")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToMany(mappedBy = "cart")
    private List<CartItem> cartItems;
    @Enumerated(EnumType.STRING)
    private CartState state;
}
