package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bill_item")
public class BillItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "bill_id", referencedColumnName = "id")
    private Bill bill;
    @Column(name = "product_id")
    private Integer productId;
    private String detail;
    @Column(name = "unit_price")
    private double unitPrice;
    private Integer quantity;
    @Column(name = "total_price")
    private double totalPrice;
}
