package com.ungs.calzados.entity.sale;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.ungs.calzados.entity.product.Product;
import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cart_items")
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product product;
    @Column(name = "item_count")
    private Integer itemCount;
    @Column(name = "subtotal")
    private Double subTotal;
    @JsonIgnore

    @ManyToOne
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private Cart cart;
}
