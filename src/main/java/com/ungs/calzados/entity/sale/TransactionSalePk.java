package com.ungs.calzados.entity.sale;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class TransactionSalePk implements Serializable {
    @Column(name = "transaction_id")
    private Integer idTransaction;
    @Column(name = "sale_id")
    private Integer idSale;
}
