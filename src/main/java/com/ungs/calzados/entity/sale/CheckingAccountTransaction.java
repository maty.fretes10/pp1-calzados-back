package com.ungs.calzados.entity.sale;

import com.ungs.calzados.constants.MovementType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@Entity
@Table(name = "checking_account_transactions")
@PrimaryKeyJoinColumn(name = "transaction_id")
public class CheckingAccountTransaction extends Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private MovementType movementType;

    public CheckingAccountTransaction(Transaction tr) {
        setId(tr.getId());
        setTransactionType(tr.getTransactionType());
        setClient(tr.getClient());
        setTransactionState(tr.getTransactionState());
        setCreationDate(tr.getCreationDate());
        setAmount(tr.getAmount());
    }
}
