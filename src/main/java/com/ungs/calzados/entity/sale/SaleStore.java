package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "store_sale")
@PrimaryKeyJoinColumn(name = "sale_id")
public class SaleStore extends Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String employee;

    public SaleStore(Sale saleBaseEntity) {
        setId(saleBaseEntity.getId());
        setSaleType(saleBaseEntity.getSaleType());
        setState(saleBaseEntity.getState());
        setUpdateDate(new Date());
        setStore(saleBaseEntity.getStore());
        setCart(saleBaseEntity.getCart());
        setTotalAmount(saleBaseEntity.getTotalAmount());
        setDiscount(saleBaseEntity.getDiscount());
        setTotalDiscountAmount(saleBaseEntity.getTotalDiscountAmount());
    }
}
