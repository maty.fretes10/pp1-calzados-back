package com.ungs.calzados.entity.sale;

import com.ungs.calzados.constants.SaleType;
import com.ungs.calzados.constants.StateSale;
import com.ungs.calzados.entity.Store;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sales")
@Inheritance(strategy = InheritanceType.JOINED)
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private Cart cart;
    @Column(name = "update_date", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Enumerated(EnumType.STRING)
    private StateSale state;
    @ManyToOne
    @JoinColumn(name = "store_id", referencedColumnName = "id")
    private Store store;
    @Column(name = "total_amount")
    private Double totalAmount;
    @Column(name = "total_discount_amount")
    private Double totalDiscountAmount = 0d;
    private Double discount = 0d;
    @Enumerated(EnumType.STRING)
    private SaleType saleType;
}
