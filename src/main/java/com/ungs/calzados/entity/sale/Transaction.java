package com.ungs.calzados.entity.sale;

import com.ungs.calzados.constants.TransactionState;
import com.ungs.calzados.constants.TransactionType;
import com.ungs.calzados.entity.Client;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sale_transaction")
@Inheritance(strategy = InheritanceType.JOINED)
@Builder
@ToString
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "client", referencedColumnName = "id")
    private Client client;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;
    @Column
    private Date creationDate;
    @Enumerated(EnumType.STRING)
    private TransactionState transactionState;
}
