package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "type_bill")
    private Character typeBill;

    //client
    @Column(name = "client_id")
    private Integer clientId;
    @Column(name = "client_name")
    private String clientName;
    @Column(name = "client_address")
    private String clientAddress;
    @Column(name = "client_identification")
    private String clientIdentification;
    @Column(name = "client_category_afip")
    private String clientCategoryAfip;

    //Store
    @Column(name = "store_id")
    private Integer storeId;
    @Column(name = "store_address")
    private String storeAddress;

    //Sale
    @OneToMany(mappedBy = "bill")
    private List<BillItem> items;
    private double subtotal;
    private double iva;
    @Column(name = "iva_percentage")
    private double ivaPercentage;
    private double discount;
    @Column(name = "discount_name")
    private String discountName;
    private double total;

    //Payments
    @OneToMany(mappedBy = "bill")
    private List<BillPay> payments;
}
