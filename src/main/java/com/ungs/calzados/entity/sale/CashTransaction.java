package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cash_transactions")
@PrimaryKeyJoinColumn(name = "transaction_id")
public class CashTransaction extends Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "description")
    private String description;

    public CashTransaction(Transaction tr) {
        setId(tr.getId());
        setTransactionType(tr.getTransactionType());
        setClient(tr.getClient());
        setTransactionState(tr.getTransactionState());
        setCreationDate(tr.getCreationDate());
        setAmount(tr.getAmount());
    }
}
