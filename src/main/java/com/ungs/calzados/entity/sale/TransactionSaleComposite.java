package com.ungs.calzados.entity.sale;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transactions_sales")
public class TransactionSaleComposite implements Serializable {

    @EmbeddedId
    private TransactionSalePk id;

    @ManyToOne
    @MapsId("transaction_id")
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    private Transaction transaction;

    @ManyToOne
    @MapsId("sale_id")
    @JoinColumn(name = "sale_id", referencedColumnName = "id")
    private Sale sale;

    public TransactionSaleComposite(TransactionSalePk id) {
        this.id = id;
    }
}
