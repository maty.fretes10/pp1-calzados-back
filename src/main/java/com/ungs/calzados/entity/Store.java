package com.ungs.calzados.entity;

import com.ungs.calzados.constants.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stores")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(unique = true)
    private String email;
    private String telephone;
    private String cuit;
    @Column(name = "sale_point", unique = true)
    private String salePoint;
    private String street;
    @Column(name = "street_number")
    private String streetNumber;
    @ManyToOne
    @JoinColumn(name = "locality_id", referencedColumnName = "id")
    private Locality locality;
    @Enumerated(EnumType.STRING)
    private State state;
}
