package com.ungs.calzados.entity;

import com.ungs.calzados.entity.product.Product;
import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "prod_suppliers_favs")
@Builder

public class ProductSupplierFavs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name="id_product", referencedColumnName = "id")
    private Product product;
    @OneToOne
    @JoinColumn(name="id_supplier",referencedColumnName = "id")
    private Supplier supplier;

}