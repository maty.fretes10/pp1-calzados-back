package com.ungs.calzados.entity;

import com.ungs.calzados.entity.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stock_stores")
public class StockStore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;
    @Column(name = "total_stock")
    private Integer totalStock;
    @ManyToOne
    @JoinColumn(name="store_id", referencedColumnName = "id")
    private Store store;


}
