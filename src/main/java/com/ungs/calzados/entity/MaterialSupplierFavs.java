package com.ungs.calzados.entity;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mats_suppliers_favs")
@Builder

public class MaterialSupplierFavs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name="id_material", referencedColumnName = "id")
    private Material material;
    @OneToOne
    @JoinColumn(name="id_proveedor",referencedColumnName = "id")
    private Supplier supplier;

}


