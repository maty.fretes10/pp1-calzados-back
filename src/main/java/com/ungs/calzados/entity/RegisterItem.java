package com.ungs.calzados.entity;

import com.ungs.calzados.constants.DailyState;
import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.constants.TransactionType;
import com.ungs.calzados.entity.sale.Transaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "register_items")

public class RegisterItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    @Column(name="movement_type")
    @Enumerated(EnumType.STRING)
    private MovementType movementType;
    private Double amount;
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "daily_register_id", referencedColumnName = "id")
    private DailyRegister dailyRegister;
}
