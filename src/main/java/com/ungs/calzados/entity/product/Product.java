package com.ungs.calzados.entity.product;

import com.ungs.calzados.constants.State;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    @Column(name = "creation_date", nullable = false, updatable = false)
    private LocalDate creationDate;
    @Column(name = "year_fabrication")
    private Integer yearFabrication;

    @Column(name = "price_sale")
    private float priceSale;
    @Column(name = "price_buy")
    private float priceBuy;

    @ManyToOne
    @JoinColumn(name = "waist_product_id", referencedColumnName = "id")
    private Waist waist;
    @ManyToOne
    @JoinColumn(name = "colour_product_id", referencedColumnName = "id")
    private Colour colour;
    @ManyToOne
    @JoinColumn(name = "product_status_id", referencedColumnName = "id")
    private ProductStatus productStatus;
    @ManyToOne
    @JoinColumn(name = "product_model_id", referencedColumnName = "id")
    private ProductModel productModel;

    @Column(name = "stock_security")
    private Integer stockSecurity;
    @Column(name = "stock_minimum")
    private Integer stockMinimum;
    @Column(name = "stock_consolidated")
    private Integer stockConsolidated;

    @Enumerated(EnumType.STRING)
    private State state;
}
