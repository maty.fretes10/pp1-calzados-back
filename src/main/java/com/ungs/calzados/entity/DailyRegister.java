package com.ungs.calzados.entity;

import com.ungs.calzados.constants.DailyState;
import com.ungs.calzados.constants.MovementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "daily_register")

public class DailyRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double balance;
    @Enumerated(EnumType.STRING)
    private DailyState state;
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "cash_register_id", referencedColumnName = "id")
    private CashRegister cashRegister;

}
