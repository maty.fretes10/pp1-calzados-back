package com.ungs.calzados.entity;

import com.ungs.calzados.constants.MovementType;
import com.ungs.calzados.entity.product.Product;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account_history")
@Builder
public class AccountHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "movement_date", nullable = false, updatable = false)
    private LocalDate Date;
    private float money;
    @Enumerated(EnumType.STRING)
    private MovementType type;

    @ManyToOne
    @JoinColumn(name = "checking_account_id", referencedColumnName = "id")
    private CheckingAccount checkingAccountId;

}
