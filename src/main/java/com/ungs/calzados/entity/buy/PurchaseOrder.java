package com.ungs.calzados.entity.buy;

import com.ungs.calzados.constants.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "purchase_order")
public class PurchaseOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private State state;
    @Column(name = "supplier_name")
    private String supplierName;
    @Column(name = "supplier_address")
    private String supplierAddress;
    @Column(name = "supplier_identification")
    private String supplierIdentification;
    @Column(name = "store_id")
    private Integer storeId;
    @Column(name = "store_address")
    private String storeAddress;
    @OneToMany(mappedBy = "purchaseOrder")
    private List<PurchaseOrderItem> items;
    private double total;

}
