package com.ungs.calzados.entity.production;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_order_state")
@Builder
public class ProductionOrderState {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;

    @ManyToOne
    @JoinColumn(name = "production_order_id", referencedColumnName = "id")
    private ProductionOrder productionOrderId;
}
