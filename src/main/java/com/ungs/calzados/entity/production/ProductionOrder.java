package com.ungs.calzados.entity.production;

import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.Waist;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_order")
@Builder
public class ProductionOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "modification_date", nullable = false, updatable = false)
    private LocalDate modificationDate;
    @ManyToOne
    @JoinColumn(name = "waist_production_id", referencedColumnName = "id")
    private Waist waistProduction;
    @ManyToOne
    @JoinColumn(name = "colour_production_id", referencedColumnName = "id")
    private Colour colourProduction;
    @ManyToOne
    @JoinColumn(name = "production_model_id", referencedColumnName = "id")
    private ProductionModel modelProduction;
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "store_id", referencedColumnName = "id")
    private Store store;



}
