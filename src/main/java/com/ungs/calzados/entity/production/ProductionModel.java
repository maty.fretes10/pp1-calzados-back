package com.ungs.calzados.entity.production;

import com.ungs.calzados.constants.ModelState;
import com.ungs.calzados.entity.product.ProductModel;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_model")
@Builder
public class ProductionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "product_model_id", referencedColumnName = "id")
    private ProductModel modelProduct;

    @Enumerated(EnumType.STRING)
    private ModelState state;

    @OneToMany(mappedBy = "productionModel")
    private List<ItemProduction> materials;
}
