package com.ungs.calzados.entity.production;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_order_history")
@Builder
public class ProductionOrderHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "modification_date", nullable = false, updatable = false)
    private LocalDate modificationDate;
    private String description;

    @ManyToOne
    @JoinColumn(name = "production_order_id", referencedColumnName = "id")
    private ProductionOrder productionOrderId;
}
