package com.ungs.calzados.entity.production;

import com.ungs.calzados.entity.Material;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stock_factory")
public class StockFactory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "material_id", referencedColumnName = "id")
    private Material material;
    @Column(name = "total_stock")
    private Integer totalStock;
    @ManyToOne
    @JoinColumn(name="store_id", referencedColumnName = "id")
    private Store store;


}
