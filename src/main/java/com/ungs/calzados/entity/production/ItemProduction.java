package com.ungs.calzados.entity.production;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ungs.calzados.constants.ProductionPhase;
import com.ungs.calzados.entity.Material;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "item_production")
public class ItemProduction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "id_material",referencedColumnName = "id")
    private Material material;
    private Integer quantity;
    @Enumerated(EnumType.STRING)
    private ProductionPhase phase;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "production_model_id", referencedColumnName = "id")
    private ProductionModel productionModel;

}
