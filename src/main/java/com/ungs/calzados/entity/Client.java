package com.ungs.calzados.entity;

import com.ungs.calzados.constants.ClientState;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "clients")
@Builder
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String surname;
    @Column(unique = true)
    private String email;
    private String telephone;
    @ManyToOne
    @JoinColumn(name = "document_type_id", referencedColumnName = "id")
    private DocumentType documentType;
    @Column(name = "document_number", unique = true)
    private String documentNumber;
    @ManyToOne
    @JoinColumn(name = "category_afip_id", referencedColumnName = "id")
    private CategoryAfip categoryAfip;
    private String street;
    @Column(name = "street_number")
    private String streetNumber;
    @ManyToOne
    @JoinColumn(name = "locality_id", referencedColumnName = "id")
    private Locality locality;
    @Column(name = "creation_date", nullable = false, updatable = false)
    private LocalDate creationDate;
    @Column(name = "credit_limit")
    private Double creditLimit;
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "checking_account_id", referencedColumnName = "id")
    private CheckingAccount checkingAccount;
    @Enumerated(EnumType.STRING)
    private ClientState state;
}
