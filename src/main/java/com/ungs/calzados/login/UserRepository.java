package com.ungs.calzados.login;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<Usuario, Long> {

	Optional<Usuario> findByUsername(String username);
 }
