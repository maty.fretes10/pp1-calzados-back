package com.ungs.calzados.login.UserStore;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserStoreRequestDto {
    @Positive(message = "Please provide a user id")
    private Integer userId;
    @Positive(message = "Please provide a store id")
    private Integer storeId;
}
