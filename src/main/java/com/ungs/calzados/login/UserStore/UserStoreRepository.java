package com.ungs.calzados.login.UserStore;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserStoreRepository extends CrudRepository<UserStore, Integer> {
    @Query("select l from UserStore l where l.usuario.id=:userId")
    Optional<UserStore> findUserStoreByUserId(Integer userId);

    @Query("select l from UserStore l where l.store.id=:storeId")
    List<UserStore> findAllUserStoreByStoreId(Integer storeId);
}
