package com.ungs.calzados.login.ignore;
import com.ungs.calzados.login.UserStore.UserStoreRequestDto;
import com.ungs.calzados.login.UserStore.UserStoreResponseDto;

import java.util.List;

public interface UserStoreService {
    void addUserStore(UserStoreRequestDto userStoreRequestDto) throws Exception;

    void deleteUserStore(Integer userId) throws Exception;

    List<UserStoreResponseDto> listOfUsersByStore(Integer storeId);

    UserStoreResponseDto storeByUserId (Integer userId);

    List<UserStoreResponseDto> listOfUserStore();
}
