package com.ungs.calzados.login.ignore;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class BasicPasswordComponent {
    public String encryptPassword(String password) {
        return DigestUtils.md5Hex(password).toUpperCase();
    }
}
