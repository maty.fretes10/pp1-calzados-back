package com.ungs.calzados.login.ignore;

import com.ungs.calzados.login.UserResponseDto;
import com.ungs.calzados.login.UserStore.UserStoreRequestDto;
import com.ungs.calzados.login.UserStore.UserStoreResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class UserStoreController {
    private final UserStoreService userStoreService;
    private final UserService userService;

    @Autowired
    public UserStoreController(UserStoreService userStoreService, UserService userService) {
        this.userStoreService = userStoreService;
        this.userService = userService;
    }

    @PostMapping("/userstore")
    public void addUserStore(@RequestBody @Valid UserStoreRequestDto userStoreRequestDto) throws Exception {
        userStoreService.addUserStore(userStoreRequestDto);
    }

    @GetMapping("/userstore/list")
    public List<UserStoreResponseDto> getUserStore() {
        return userStoreService.listOfUserStore();
    }

    @GetMapping("/userstore/store/list")
    public List<UserStoreResponseDto> getUserStoreByStoreId(@RequestParam(required = false) Integer storeId) {
        return userStoreService.listOfUsersByStore(storeId);
    }

    @GetMapping("/userstore/user")
    public UserStoreResponseDto getUserStoreByUserId(@RequestParam(required = false) Integer userId) {
        return userStoreService.storeByUserId(userId);
    }

    @DeleteMapping("/userstore/user/delete")
    public void deleteUserStore(@RequestParam(required = false) Integer userId) throws Exception {
        userStoreService.deleteUserStore(userId);
    }

    @GetMapping("/users")
    public List<UserResponseDto> getUsers(){
        return userService.getUsers();
    }
}
