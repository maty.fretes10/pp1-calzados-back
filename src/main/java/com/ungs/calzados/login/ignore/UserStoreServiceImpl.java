package com.ungs.calzados.login.ignore;

import com.ungs.calzados.entity.Store;
import com.ungs.calzados.exception.NoDataFoundException;
import com.ungs.calzados.login.UserStore.UserStore;
import com.ungs.calzados.login.UserStore.UserStoreRepository;
import com.ungs.calzados.login.UserStore.UserStoreRequestDto;
import com.ungs.calzados.login.UserStore.UserStoreResponseDto;
import com.ungs.calzados.login.Usuario;
import com.ungs.calzados.service.StoreService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserStoreServiceImpl implements UserStoreService {

    private final UserStoreRepository userStoreRepository;
    private final StoreService storeService;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserStoreServiceImpl(UserStoreRepository userStoreRepository,
                                StoreService storeService,
                                UserService userService,
                                ModelMapper modelMapper){
        this.userStoreRepository = userStoreRepository;
        this.storeService = storeService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @Override
    public void addUserStore(UserStoreRequestDto userStoreRequestDto) throws Exception {
        Store store = storeService.getStoreById(userStoreRequestDto.getStoreId());
        Usuario usuario = userService.getUserById(userStoreRequestDto.getUserId().longValue());
        UserStore userStore = new UserStore();
        userStore.setStore(store);
        userStore.setUsuario(usuario);

        userStoreRepository.save(userStore);
    }

    @Override
    public void deleteUserStore(Integer userId) {
        UserStore userStore = getUserStoreByUserId(userId);
        userStoreRepository.delete(userStore);
    }

    @Override
    public List<UserStoreResponseDto> listOfUsersByStore(Integer storeId) {
        List<UserStore> allUserStore = userStoreRepository.findAllUserStoreByStoreId(storeId);
        if (allUserStore.isEmpty()) {
            throw new NoDataFoundException();
        }
        return allUserStore.stream()
                .parallel()
                .map(userStore -> modelMapper.map(userStore, UserStoreResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserStoreResponseDto storeByUserId(Integer userId) {
        UserStore userStore = getUserStoreByUserId(userId);
        return modelMapper.map(userStore, UserStoreResponseDto.class);
    }

    @Override
    public List<UserStoreResponseDto> listOfUserStore() {
        List<UserStore> allUserStore = (List<UserStore>) userStoreRepository.findAll();
        if (allUserStore.isEmpty()) {
            throw new NoDataFoundException();
        }
        return allUserStore.stream()
                .parallel()
                .map(userStore -> modelMapper.map(userStore, UserStoreResponseDto.class))
                .collect(Collectors.toList());
    }

    private UserStore getUserStoreByUserId(Integer userId) {
        Optional<UserStore> optionalUserStore = userStoreRepository.findUserStoreByUserId(userId);
        if(!optionalUserStore.isPresent()){
            throw new IllegalArgumentException("No se encontro una relacion de Usuario y Sucursal");
        }
        UserStore userStore = optionalUserStore.get();
        return userStore;
    }
}
