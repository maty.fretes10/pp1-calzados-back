package com.ungs.calzados.login.ignore;


import com.ungs.calzados.dto.ChangePasswordForm;
import com.ungs.calzados.exception.UsernameOrIdNotFound;
import com.ungs.calzados.login.UserResponseDto;
import com.ungs.calzados.login.Usuario;

import java.util.List;

public interface UserService {

	Iterable<Usuario> getAllUsers();

	Usuario createUser(Usuario user) throws Exception;

	Usuario getUserById(Long id) throws Exception;
	
	Usuario updateUser(Usuario user) throws Exception;
	
	void deleteUser(Long id) throws UsernameOrIdNotFound;
	
	Usuario changePassword(ChangePasswordForm form) throws Exception;

	List<UserResponseDto> getUsers();
}
