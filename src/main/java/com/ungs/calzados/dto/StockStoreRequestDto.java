package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)

public class StockStoreRequestDto {

    @NotNull(message = "Please provide a product id")
    private Integer productId;
    @NotNull(message = "Please provide a store id")
    private Integer storeId;
    @NotNull(message = "Please provide a stock number")
    private Integer totalStock;

}
