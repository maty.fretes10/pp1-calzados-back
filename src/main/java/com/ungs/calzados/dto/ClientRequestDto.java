package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClientRequestDto {
    @NotEmpty(message = "Please provide a name")
    private String name;
    @NotEmpty(message = "Please provide a surname")
    private String surname;
    @NotEmpty(message = "Please provide an email")
    private String email;
    @NotEmpty(message = "Please provide a telephone number")
    private String telephone;
    @NotNull(message = "Please provide a document number type id")
    private Integer documentTypeId;
    @NotEmpty(message = "Please provide a document number")
    private String documentNumber;
    @NotNull(message = "Please provide a category AFIP id")
    private Integer categoryAfipId;
    @NotEmpty(message = "Please provide a street name")
    private String street;
    @NotEmpty(message = "Please provide a street number")
    private String streetNumber;
    @Positive(message = "Please provide a locality id")
    private Integer localityId;
    @NotNull(message = "Please provide a credit limit")
    private Double creditLimit;
}
