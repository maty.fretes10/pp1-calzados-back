package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)

public class MaterialRequestDto {
    @NotEmpty(message = "Please provide a supply name")
    private String name;
    @NotEmpty(message = "Please provide a supply description")
    private String description;
    @NotNull(message = "Please provide a stock")
    private Integer stock;
    @NotNull(message = "Please provide a price")
    private float priceBuy;
    @NotNull(message = "Please provide a minimum stock")
    private Integer stockMin;

}
