package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.SupplierState;
import com.ungs.calzados.entity.Locality;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)

public class SupplierResponseDto {
    private Integer id;
    private String name;
    private String email;
    private String street;
    private Integer streetNumber;
    private SupplierState state;
    private String telephone;
    private String cuit;
    private Locality locality;


}
