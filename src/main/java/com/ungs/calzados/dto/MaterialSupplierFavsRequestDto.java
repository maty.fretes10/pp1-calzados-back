package com.ungs.calzados.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)


public class MaterialSupplierFavsRequestDto {
    @NotNull(message = "Please provide a material id")
    private Integer idMaterial;
    @NotNull(message = "Please provide a supplier id")
    private Integer idSupplier;
}
