package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.dto.product.ProductStockRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PurchaseOrderRequestDto implements Serializable {
    @NotNull(message = "Please provide a store id")
    private Integer storeId;
    @NotNull(message = "Please provide a store id")
    private Integer supplierId;

    @NotNull(message = "Please provide a list of products")
    private List<PurchaseOrderItemRequestDto> items;

}
