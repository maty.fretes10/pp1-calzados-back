package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.ProductModel;
import com.ungs.calzados.entity.product.Waist;
import com.ungs.calzados.entity.production.ProductionOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class POStateResponseDto {
    private ProductionOrder productionOrderId;
    private String description;
}
