package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ItemProductionRequestDto implements Serializable {
    @Positive(message = "Please provide a material id greater than 0")
    private Integer materialId;
    @Positive(message = "Please provide a required stock greater than 0")
    private Integer requiredStock;
    @NotNull(message = "Please provide a phase")
    private String phase;
}
