package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductionModelRequestDto implements Serializable {
    @NotNull(message = "Please provide a store id")
    private Integer modelId;
    @NotNull(message = "Please provide a list of material to save in production")
    private List<ItemProductionRequestDto> materials;
}
