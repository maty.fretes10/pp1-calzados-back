package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.entity.product.Colour;
import com.ungs.calzados.entity.product.Waist;
import com.ungs.calzados.entity.production.ProductionModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductionOrderResponseDto {
    private Integer id;
    private Waist waistProduction;
    private Colour colourProduction;
    private LocalDate modificationDate;
    private ProductionModel modelProduction;
    private Integer quantity;
}
