package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;


@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class POStateRequestDto {
    @NotEmpty(message = "Please provide a description")
    private String description;
    @Positive(message = "Please provide a production order id greater than 0")
    private Integer productionOrderId;
}
