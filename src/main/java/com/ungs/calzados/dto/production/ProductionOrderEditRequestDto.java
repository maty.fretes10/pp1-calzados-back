package com.ungs.calzados.dto.production;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductionOrderEditRequestDto {

    @NotNull(message = "Please provide a production order id")
    private Integer productionOrderId;
    @Positive(message = "Please provide a waist Id")
    private Integer waistProductionId;
    @Positive(message = "Please provide a colour Id")
    private Integer colourProductionId;
    @NotNull(message = "Please provide a quantity")
    private Integer quantity;
}
