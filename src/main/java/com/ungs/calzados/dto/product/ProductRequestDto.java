package com.ungs.calzados.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductRequestDto {
    @NotEmpty(message = "Please provide a description")
    private String description;
    @NotNull(message = "Please provide a year fabrication")
    private Integer yearFabrication;

    @NotNull(message = "Please provide a price Sale ")
    private float priceSale;
    @NotNull(message = "Please provide a price Buy")
    private float priceBuy;

    @Positive(message = "Please provide a product waist Id")
    private Integer waistId;
    @Positive(message = "Please provide a product colour Id")
    private Integer colourId;
    @Positive(message = "Please provide a product Status Id")
    private Integer productStatusId;
    @Positive(message = "Please provide a product Model Id")
    private Integer productModelId;

    @NotNull(message = "Please provide a stock Security")
    private Integer stockSecurity;

    @NotNull(message = "Please provide a stock Minimum")
    private Integer stockMinimum;

}
