package com.ungs.calzados.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.entity.product.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductResponseDto {
    private Integer id;
    private String description;
    private Integer stock;
    private LocalDate creationDate;
    private Integer yearFabrication;
    private float priceSale;
    private float priceBuy;
    private Waist waist;
    private Colour colour;
    private ProductStatus productStatus;
    private ProductModel productModel;
    private Integer stockSecurity;
    private Integer stockMinimum;
    private PriceHistoryResponseDto priceHistory;
}