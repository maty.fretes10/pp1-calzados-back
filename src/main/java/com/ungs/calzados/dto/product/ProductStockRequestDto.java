package com.ungs.calzados.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.validation.constraints.Positive;
import java.io.Serializable;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductStockRequestDto implements Serializable {
    @Positive(message = "Please provide a product id greater than 0")
    private Integer productId;
    @Positive(message = "Please provide a required stock greater than 0")
    private Integer requiredStock;
}
