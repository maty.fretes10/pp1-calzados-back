package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.State;
import com.ungs.calzados.entity.buy.PurchaseOrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)

public class PurchaseOrderResponseDto {
    private Integer id;
    private String supplierName;
    private String supplierAddress;
    private String supplierIdentification;
    private Integer storeId;
    private String storeAddress;
    private List<PurchaseOrderItem> items;
    private double total;
    private State state;
}
