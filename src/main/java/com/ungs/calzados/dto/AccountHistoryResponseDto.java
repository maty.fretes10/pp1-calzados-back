package com.ungs.calzados.dto;

import com.ungs.calzados.constants.MovementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountHistoryResponseDto {
    private Integer id;
    private LocalDate Date;
    private float money;
    private MovementType type;
}
