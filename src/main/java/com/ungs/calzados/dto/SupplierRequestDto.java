package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)


public class SupplierRequestDto {
    @NotEmpty(message = "Please provide a name")
    private String name;
    @NotEmpty(message = "Please provide an email")
    private String email;
    @NotEmpty(message = "Please provide a street name")
    private String street;
    @NotNull(message = "Please provide a street number")
    private Integer streetNumber;
    @NotEmpty(message = "Please provide a telephone")
    private String telephone;
    @NotEmpty(message = "Please provide a CUIT number")
    private String cuit;
    @NotNull(message = "Please provide a locality Id")
    private Integer localityId;


}
