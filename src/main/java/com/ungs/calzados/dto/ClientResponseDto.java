package com.ungs.calzados.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.ClientState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClientResponseDto {
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String telephone;
    private DocumentTypeResponseDto documentType;
    private String documentNumber;
    private CategoryAfipResponseDto categoryAfip;
    private String street;
    private String streetNumber;
    private LocalityResponseDto locality;
    private LocalDate creationDate;
    private Double creditLimit;
    private CheckingAccountResponseDto checkingAccount;
    private ClientState state;
}