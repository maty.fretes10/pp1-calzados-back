package com.ungs.calzados.dto.sale;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionSaleCompositeRequestDto {
    private Integer transactionId;
    private Integer saleId;
}
