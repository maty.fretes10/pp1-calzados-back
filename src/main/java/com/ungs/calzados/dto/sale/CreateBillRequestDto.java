package com.ungs.calzados.dto.sale;

import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.sale.Sale;
import com.ungs.calzados.entity.sale.Transaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateBillRequestDto {
    private Sale sale;
    private Client client;
    private List<Transaction> transaction;
}
