package com.ungs.calzados.dto.sale;

import com.ungs.calzados.constants.SaleType;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.sale.Cart;
import com.ungs.calzados.entity.sale.Sale;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateSpecificSaleRequestDto {
    private Sale sale;
    private Cart cart;
    private Store store;
    private Client client;
    private String employee;
    private SaleType saleType;
    private Double totalAmount;
    private Double discount;
    private Double totalDiscountAmount;
}
