package com.ungs.calzados.dto.sale;

import com.ungs.calzados.entity.Locality;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StoreResponseDto {
    private Integer id;
    private String name;

    private String email;
    private String telephone;
    private String cuit;
    private String salePoint;

    private String street;
    private String streetNumber;
    private Locality locality;

}
