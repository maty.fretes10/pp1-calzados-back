package com.ungs.calzados.dto.sale;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SpecificTransactionRequestDetailsRequestDto {
    @NotNull(message = "Please provide the transaction type")
    private TransactionType transactionType;
    @Positive(message = "Please provide an transaction amount greater tha 0")
    private Double amount;
    private CreditCardRequestDto creditCard;
    private String description;
}
