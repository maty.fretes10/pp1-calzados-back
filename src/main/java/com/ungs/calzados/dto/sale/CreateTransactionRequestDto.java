package com.ungs.calzados.dto.sale;

import com.ungs.calzados.constants.TransactionType;
import com.ungs.calzados.entity.Client;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.sale.Cart;
import com.ungs.calzados.entity.sale.Sale;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateTransactionRequestDto {
    private Client client;
    private String employee;
    private Cart cart;
    private Store store;
    private Sale sale;
    private TransactionType transactionType;
    private SpecificTransactionRequestDetailsRequestDto specificRequestDetails;
}
