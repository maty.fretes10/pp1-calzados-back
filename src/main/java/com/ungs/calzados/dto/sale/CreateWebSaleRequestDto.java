package com.ungs.calzados.dto.sale;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.dto.product.ProductStockRequestDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateWebSaleRequestDto {
    @Positive(message = "Please provide a store id greater than 0")
    private Integer storeId;
    @Positive(message = "Please provide a client id greater than 0")
    private Integer clientId;
    @NotNull(message = "Please provide al least one transaction request to pay the sale")
    private List<SpecificTransactionRequestDetailsRequestDto> transactions;
    @NotNull(message = "please provide the total amount of the sale")
    private Double totalAmount;
    @NotNull(message = "Please provide a list of products to buy")
    private List<ProductStockRequestDto> products;
}