package com.ungs.calzados.dto.sale;

import com.ungs.calzados.entity.sale.BillItem;
import com.ungs.calzados.entity.sale.BillPay;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BillRequestReportDto extends JRDefaultScriptlet {
    private Integer id;
    private Character typeBill;
    private Integer clientId;
    private String clientName;
    private String clientAddress;
    private String clientIdentification;
    private String clientCategoryAfip;
    private Integer storeId;
    private String storeAddress;
    private List<BillItem> items;
    private double subtotal;
    private double iva;
    private double discount;
    private String discountName;
    private double total;
    private List<BillPay> payments;
    private JRBeanCollectionDataSource itemsdataSource;
    private JRBeanCollectionDataSource paymentsdataSource;

    public JRBeanCollectionDataSource getItemsdataSource() {
        return new JRBeanCollectionDataSource(items, false);
    }
    public JRBeanCollectionDataSource getPaymentsdataSource() { return new JRBeanCollectionDataSource(payments, false); }

}
