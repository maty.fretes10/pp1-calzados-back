package com.ungs.calzados.dto.sale;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.SaleType;
import com.ungs.calzados.entity.Store;
import com.ungs.calzados.entity.sale.Cart;

import com.ungs.calzados.constants.StateSale;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaleResponseDto {
    private Integer id;
    private Cart cart;
    private Date updateDate;

    private StateSale state;

    private Store store;
    private Double totalAmount;
    private Double totalDiscountAmount;
    private Double discount;
    private SaleType saleType;
}
