package com.ungs.calzados.dto.sale;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class StoreRequestDto {
        @NotEmpty(message = "Please provide a name")
        private String name;
        @NotEmpty(message = "Please provide a sale Point")
        private String salePoint;
        @NotEmpty(message = "Please provide an email")
        private String email;
        @NotEmpty(message = "Please provide a telephone number")
        private String telephone;
        @NotEmpty(message = "Please provide a street name")
        private String street;
        @NotEmpty(message = "Please provide a street number")
        private String streetNumber;
        @Positive(message = "Please provide a locality id")
        private Integer localityId;
    }


