package com.ungs.calzados.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ungs.calzados.constants.DailyState;
import com.ungs.calzados.entity.CashRegister;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DailyRegisterResponseDto {

    private Integer id;
    private Double balance;
    private DailyState state;
    private LocalDate date;
    private CashRegister cashRegister;
}
