package com.ungs.calzados.dto;

import com.ungs.calzados.entity.buy.PurchaseOrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PurchaseRequestReportDto extends JRDefaultScriptlet {
    private Integer id;
    private String supplierName;
    private String supplierAddress;
    private String supplierIdentification;
    private String salePoint;
    private String storeAddress;
    private String email;
    private String telephone;
    private String localidad;
    private List<PurchaseOrderItem> items;
    private double total;
    private JRBeanCollectionDataSource itemsdataSource;

    public JRBeanCollectionDataSource getItemsdataSource() {
        return new JRBeanCollectionDataSource(items, false);
    }

}
