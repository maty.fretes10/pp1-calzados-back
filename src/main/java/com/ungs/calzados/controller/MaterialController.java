package com.ungs.calzados.controller;

import com.ungs.calzados.dto.MaterialRequestDto;
import com.ungs.calzados.dto.MaterialResponseDto;
import com.ungs.calzados.service.MaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class MaterialController {

    private final MaterialService materialService;

    @Autowired
    public MaterialController (MaterialService materialService){
        this.materialService = materialService;
    }

    @PostMapping("/material")
    public void addMaterial(@Valid @RequestBody MaterialRequestDto materialRequest){
        log.info("Accediendo a crear un insumo");
        materialService.addMaterial(materialRequest);
    }

    @GetMapping("/materials")
    public List<MaterialResponseDto> getMaterials (){
        log.info("Accediendo a la lista de insumos");
        return materialService.getMaterials();
    }

    @GetMapping("/material")
    public MaterialResponseDto getMaterialById(@RequestParam Integer materialId){
        log.info("accediendo al material con id "+materialId);
        return materialService.getMaterialByIdResponse(materialId);
    }


    @DeleteMapping("/materials/inactive")
    public void inactivateMaterial(@RequestParam Integer materialId){
        log.info("Inactivando el material con id "+materialId);
        materialService.inactivateMaterial(materialId);
    }


}