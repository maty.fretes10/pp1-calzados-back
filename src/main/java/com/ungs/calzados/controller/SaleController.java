package com.ungs.calzados.controller;

import com.ungs.calzados.dto.sale.CreateSaleResponseDto;
import com.ungs.calzados.dto.sale.CreateStoreSaleRequestDto;
import com.ungs.calzados.dto.sale.CreateWebSaleRequestDto;
import com.ungs.calzados.dto.sale.SaleResponseDto;
import com.ungs.calzados.service.sale.ConstructionsSaleService;
import com.ungs.calzados.service.sale.SaleService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
@Slf4j
public class SaleController {

    private final ConstructionsSaleService orchestratedSaleTasksService;
    private final SaleService saleService;

    @Autowired
    public SaleController(ConstructionsSaleService orchestratedSaleTasksService, SaleService saleService) {
        this.orchestratedSaleTasksService = orchestratedSaleTasksService;
        this.saleService = saleService;
    }

    @PostMapping("/sale/store")
    public CreateSaleResponseDto makeStoreSale(@RequestBody @Valid CreateStoreSaleRequestDto createStoreSaleRequest) throws Exception {
        log.info("Accessing create store sale endpoint");
        return orchestratedSaleTasksService.createStoreSale(createStoreSaleRequest);
    }

    @PostMapping("/sale/web")
    public CreateSaleResponseDto makeWebSale(@RequestBody @Valid CreateWebSaleRequestDto createWebSaleRequest) throws JRException, FileNotFoundException {
        log.info("Accessing create web sale endpoint");
        return orchestratedSaleTasksService.createWebSale(createWebSaleRequest);
    }

    @GetMapping("/sale")
    public List<SaleResponseDto> getSale() {
        log.info("Accsessing sale  ");
        return saleService.getSale();
    }

    @GetMapping("/sale/id")
    public SaleResponseDto getSaleById(@RequestParam(required = false) Integer saleId) {
        log.info("Accsessing sale by id  " + saleId);
        return saleService.getSaleByIdDto(saleId);
    }

    @DeleteMapping("/sale/inactive")
    public void inactivateSale(@RequestParam Integer saleId){
        log.info("Inactivando venta con id "+saleId);
        saleService.inactivateSale(saleId);
    }
}
