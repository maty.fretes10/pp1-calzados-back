package com.ungs.calzados.controller;


import com.ungs.calzados.dto.SupplierRequestDto;
import com.ungs.calzados.dto.SupplierResponseDto;
import com.ungs.calzados.service.ConstrucctionSupplierService;
import com.ungs.calzados.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class SupplierController {

    private final SupplierService supplierService;
    private final ConstrucctionSupplierService construcctionSupplierService;

 @Autowired
    public SupplierController(SupplierService supplierService, ConstrucctionSupplierService construcctionSupplierService){
        this.supplierService = supplierService;
        this.construcctionSupplierService = construcctionSupplierService;
    }

    @PostMapping("/supplier")
    public void addSupplier(@Valid @RequestBody SupplierRequestDto supplierRequestDto){
        log.info("Accediendo a crear un proveedor");
        construcctionSupplierService.constructionsSaveSupplier(supplierRequestDto);
    }

    @GetMapping("/suppliers")
    public List <SupplierResponseDto> getSuppliers (){
        log.info("Accediendo a la lista de clientes");
        return supplierService.getSuppliers();
    }

    @DeleteMapping("/supplier/inactive")
    public void inactivateSupplier(@RequestParam Integer supplierId){
        log.info("Inactivando al proveedor número "+supplierId);
        supplierService.inactivateSupplier(supplierId);
    }

    @PostMapping("/supplier/edit")
    public void editSupplier(
            @RequestParam Integer supplierId,
            @Valid @RequestBody SupplierRequestDto request){
        log.info("Editando el proveedor con id " + supplierId);
        construcctionSupplierService.constructionsEditSupplier(supplierId, request);
    }
}
