package com.ungs.calzados.controller;

import com.ungs.calzados.dto.production.*;
import com.ungs.calzados.service.production.ConstructionsPOService;
import com.ungs.calzados.service.production.POHistoryService;
import com.ungs.calzados.service.production.POStateService;
import com.ungs.calzados.service.production.ProductionOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class OrderProductionController {

    private final ConstructionsPOService constructionsProductionOrderService;
    private final ProductionOrderService productionOrderService;
    private final POStateService poStateService;
    private final POHistoryService historyService;

    @Autowired
    public OrderProductionController (ConstructionsPOService constructionsProductionOrderService,
                                      ProductionOrderService productionOrderService,
                                      POStateService poStateService,
                                      POHistoryService historyService){
        this.constructionsProductionOrderService = constructionsProductionOrderService;
        this.productionOrderService = productionOrderService;
        this.poStateService = poStateService;
        this.historyService = historyService;
    }

    @PostMapping("production/order")
    public void addProductionOrder(@Valid @RequestBody ProductionOrderRequestDto productionOrderRequestDto){
        log.info("Accediendo a crear orden de produccion");
        constructionsProductionOrderService.addProductionOrder(productionOrderRequestDto);
    }

    @PostMapping("production/order/edit")
    public void editProductionOrder(@Valid @RequestBody ProductionOrderEditRequestDto productionOrderRequestDto){
        log.info("Accediendo a editar la orden de produccion");
        constructionsProductionOrderService.editProductionOrder(productionOrderRequestDto);
    }

    @GetMapping("production/order/list")
    public List<ProductionOrderResponseDto> getProductionOrder(){
        log.info("Accediendo a la lista de ordenes de produccion");
        return productionOrderService.getOrderResponse();
    }

    //ESTADOS DE ORDEN DE PRODUCCION
    @PostMapping("po/state/edit")
    public void editStatePO (@Valid @RequestBody POStateRequestDto poStateRequestDto){
        log.info("Accediendo a editar el estado de una orden de produccion");
        poStateService.editPOState(poStateRequestDto);
    }

    @GetMapping("po/state/list")
    public List<POStateResponseDto> getProductionOrderState(){
        log.info("Accediendo a la lista de ordenes de produccion con estados");
        return poStateService.getPOstate();
    }

    @GetMapping("po/history/list/{orderId}")
    public List<POHistoryResponseDto> getProductionOrderState(@PathVariable Integer orderId){
        log.info("Accediendo a al historial de la orden de produccion");
        return historyService.getPOHystoryByOrder(orderId);
    }
}
