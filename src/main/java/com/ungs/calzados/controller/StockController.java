package com.ungs.calzados.controller;

import com.ungs.calzados.dto.StockStoreRequestDto;
import com.ungs.calzados.dto.StockStoreResponseDto;
import com.ungs.calzados.service.StockStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class StockController {
    private final StockStoreService stockService;


@Autowired
public  StockController(StockStoreService stockService){
    this.stockService=stockService;
}

    @PostMapping("/stock")
    public void addStock(@Valid @RequestBody StockStoreRequestDto request){
        log.info("Accediendo a cargar stocks");
        stockService.addStockStore(request);
    }

    @GetMapping("/stock/product")
    public List<StockStoreResponseDto> getStocks(@RequestParam Integer productId){
        log.info("Accediendo consultar el stock del producto con id: "+productId);
        return stockService.findByProductId(productId);
    }

    @GetMapping("stock/store")
    public List<StockStoreResponseDto> getStockStore(@RequestParam Integer storeId){
        log.info("Accediendo a consultar el stock de la sucursal con id: "+storeId);
        return stockService.findByStoreId(storeId);
    }



}