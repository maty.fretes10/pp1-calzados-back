package com.ungs.calzados.controller;

import com.ungs.calzados.dto.PurchaseOrderRequestDto;
import com.ungs.calzados.dto.PurchaseOrderResponseDto;
import com.ungs.calzados.service.PurchaseOrderService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
@Slf4j
public class PurchaseOrderController {

   private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public PurchaseOrderController(PurchaseOrderService purchaseOrderService){
        this.purchaseOrderService = purchaseOrderService;
    }

    @PostMapping("/purchase/order")
    public void addPurchaseOrder(@RequestBody @Valid PurchaseOrderRequestDto purchaseOrderRequestDto) throws JRException, FileNotFoundException {
        log.info("Accediendo a crear orden de compra");
        purchaseOrderService.addPurchaseOrder(purchaseOrderRequestDto);
    }

    @GetMapping("/purchase/order/list")
    public List<PurchaseOrderResponseDto> getPurchaseOrder() {
        log.info("Accediendo a la lista de ordenes de compra  ");
        return purchaseOrderService.getPurchaseOrder();
    }

    @DeleteMapping("/purchase/order/inactive")
    public void deletePurchaseOrder(@RequestParam Integer purchaseOrderId) {
        log.info("Accsessing purchase order by id to inactivate  " + purchaseOrderId);
        purchaseOrderService.inactivatePurchaseOrder(purchaseOrderId);
    }

    @GetMapping("/purchase/order")
    public PurchaseOrderResponseDto getPurchaseOrderById(@RequestParam(required = false) Integer purchaseOrderId) {
        log.info("Accsessing purchase order by id  " + purchaseOrderId);
        return purchaseOrderService.getPurchaseOrderResponseDto(purchaseOrderId);
    }
}
