package com.ungs.calzados.controller;

import com.ungs.calzados.dto.production.*;
import com.ungs.calzados.service.production.ConstructionsModelProductionService;
import com.ungs.calzados.service.production.ProductionModelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class ModelProductionController {

    ConstructionsModelProductionService constructionsModelProductionService;
    ProductionModelService productionModelService;

    @Autowired
    public ModelProductionController(ConstructionsModelProductionService constructionsModelProductionService,
                                     ProductionModelService productionModelService){
        this.constructionsModelProductionService = constructionsModelProductionService;
        this.productionModelService = productionModelService;
    }

    @PostMapping("/production/model")
    public void addProductionModel(@Valid @RequestBody ProductionModelRequestDto productionModelRequestDto){
        log.info("Accediendo a crear un modelo de produccion");
        constructionsModelProductionService.constructionsSaveProductionModel(productionModelRequestDto);
    }

    @GetMapping("/production/model/list")
    public List<ProductionModelResponseDto> getClients() {
        log.info("Accsessing a la lista de modelos de produccion  ");
        return productionModelService.getProductModels();
    }

    @PostMapping("/production/model/edit")
    public void editProductionModel(@Valid @RequestBody ProductionModelRequestDto productionModelRequestDto, @RequestParam Integer modelId){
        log.info("Accediendo a editar un modelo de produccion");
        constructionsModelProductionService.constructionsEditProductionModel(productionModelRequestDto, modelId);
    }

    @DeleteMapping("/production/model/inactivate")
    public void inactivateProductionModel(@RequestParam Integer modelId){
        productionModelService.inactivateProductionModel(modelId);
    }
}
