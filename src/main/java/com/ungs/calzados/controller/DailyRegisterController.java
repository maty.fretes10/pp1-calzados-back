
package com.ungs.calzados.controller;

import com.ungs.calzados.dto.DailyRegisterResponseDto;
import com.ungs.calzados.dto.RegisterItemResponseDto;
import com.ungs.calzados.service.DailyRegisterService;
import com.ungs.calzados.service.RegisterItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class DailyRegisterController {

    private final DailyRegisterService dailyRegisterService;
    private final RegisterItemService registerItemService;

    @Autowired
    public DailyRegisterController(DailyRegisterService dailyRegisterService,
                                   RegisterItemService registerItemService){
        this.dailyRegisterService = dailyRegisterService;
        this.registerItemService = registerItemService;
    }

    @PostMapping("/store/daily")
    public void addDaily(@RequestParam Integer sucursalId){
        log.info("Accediendo a crear una caja diaria para la sucursal: "+ sucursalId);
        dailyRegisterService.addDaily(sucursalId);
    }

    @GetMapping("/store/dailies")
    public List<DailyRegisterResponseDto> listDailies(@RequestParam Integer sucursalId){
        log.info("Accediendo a la lista de cajas diarias de la sucursal: "+sucursalId );
        return dailyRegisterService.listDailies(sucursalId);
    }


    @DeleteMapping("/store/daily/close")
    public void closeDaily(@RequestParam Integer sucursalId){
        log.info("Accediendo a cerrar la caja diaria abierta del día");
        dailyRegisterService.closeDaily(sucursalId);
    }

    @GetMapping("/store/daily/items")
    public List<RegisterItemResponseDto> listItems(@RequestParam Integer sucursalId){
        log.info("Accediendo a la lista de items de la caja diaria abierta de la sucursal: "+sucursalId);
        return registerItemService.listItems(sucursalId);
    }



}
