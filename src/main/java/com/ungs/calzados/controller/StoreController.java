package com.ungs.calzados.controller;


import com.ungs.calzados.dto.SupplierResponseDto;
import com.ungs.calzados.dto.sale.StoreRequestDto;
import com.ungs.calzados.dto.sale.StoreResponseDto;
import com.ungs.calzados.service.StoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class StoreController {

    private final StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService){
        this.storeService = storeService;
    }

    @PostMapping("/store")
    public void addStore(@Valid @RequestBody StoreRequestDto request){
        log.info("Accediendo a crear una sucursal");
        storeService.addStore(request);
    }

    @GetMapping("/stores")
    public List<StoreResponseDto> listStores(){
        log.info("Accediendo a la lista de sucursales");
        return storeService.getStores();
    }

    @PostMapping("/store/edit")
    public void editStore(@Valid@RequestBody StoreRequestDto request, @RequestParam Integer sucursalId){
        log.info("Accediendo a editar la sucursal: "+sucursalId);
        storeService.editStore(request, sucursalId);
    }



}
