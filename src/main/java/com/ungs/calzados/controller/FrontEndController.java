package com.ungs.calzados.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class FrontEndController {


	/*
	 * IMPORTANTE!:
	 *
	 * Controlador de vistas de Front!
	 * Desarrolladores Backend no usen esta vista
	 * Consultar antes
	 * */

	@GetMapping("/productos")
	public String listaProducto() {
		return "productos";
	}


	/*@GetMapping("")
	public String index() {
		return "login";
	}*/

	@GetMapping("/clientes")
	public String listaCientes() {
		return "clientes";
	}

	@GetMapping("/carrito-compras")
	public String carrito_compras() {
		return "carrito-compras";
	}

//	@GetMapping("/checkout")
//	public String checkout() {
//		return "checkout";
//	}

	/*@GetMapping("/forgot-password")
	public String forgotPassword() {
		return "forgot-password";
	}*/

	/*@GetMapping("/login")
	public String login() {
		return "login";
	}*/

	/*@GetMapping("/register")
	public String register() {
		return "register";
	}*/

//	@GetMapping("/pagos")
//	public String payment() {
//		return "pagos";
//	}

		/*@GetMapping("/index")
	public String mainPage() {
		return "clientes";
	}*/

	@GetMapping("/ventas-realizadas")
	public String ventasRealizadas() {
		return "ventas-realizadas";
	}

	@GetMapping("/ventas-web")
	public String ventasWeb() {
		return "ventas-web";
	}

	/*
	@GetMapping("/clientes/{id}")
	public String getClient(Model model,@PathVariable int id) {
		model.addAttribute("id", id);
		return "detail";
	}
	*/




	@GetMapping("/proveedores")
	public String moduloProveedores() {
		return "proveedores";
	}

	@GetMapping("/sucursales")
	public String moduloSucursales() {
		return "sucursales";
	}

	@GetMapping("/gestion-caja")
	public String gestionCaja() {
		return "gestion-caja";
	}

	@GetMapping("/insumos")
	public String moduloInsumos() {
		return "insumos";
	}

	@GetMapping("/ordenes-produccion")
	public String ordenesProduccion() {
		return "ordenes-produccion";
	}

	@GetMapping("/modelo-productos")
	public String ModeloProductos() {
		return "modelo-productos";
	}

	@GetMapping("/modelo-ordenes-produccion")
	public String moduloModelosOrdenesProd() {
		return "modelo-unidad-produccion";
	}













	@GetMapping("/compra-proveedores")
	public String moduloCompraProveedores() {
		return "compra-proveedores";
	}
}