package com.ungs.calzados.controller;

import com.ungs.calzados.dto.CategoryAfipResponseDto;
import com.ungs.calzados.dto.DocumentTypeResponseDto;
import com.ungs.calzados.service.IdentificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class IdentificationController {
    private final IdentificationService identificationService;

    @Autowired
    public IdentificationController (IdentificationService identificationService){
        this.identificationService = identificationService;
    }

    @GetMapping("/identification/types")
    public List<DocumentTypeResponseDto> getIdentifications(){
        return identificationService.getDocumentTypes();
    }

    @GetMapping("/categories/afip")
    public List<CategoryAfipResponseDto> getCategoriesAfip(){
        return identificationService.getCategoriesAfip();
    }
}
