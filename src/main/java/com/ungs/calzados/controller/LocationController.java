package com.ungs.calzados.controller;


import com.ungs.calzados.dto.LocalityResponseDto;
import com.ungs.calzados.dto.ProvinceResponseDto;
import com.ungs.calzados.service.LocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class LocationController {

    private LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/provinces")
    public List<ProvinceResponseDto> getAllProvinces() {
        log.info("Accessing all provinces endpoint");
        return locationService.getAllProvinces();
    }

    @GetMapping("/localities/{provinceId}")
    public List<LocalityResponseDto> getLocalitiesByProvinceId(@PathVariable Integer provinceId) {
        log.info("Accessing all localities by province id endpoint");
        return locationService.getAllLocalitiesByProvinceId(provinceId);
    }
}
