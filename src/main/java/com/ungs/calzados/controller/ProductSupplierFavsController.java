package com.ungs.calzados.controller;

import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.MaterialSupplierFavsRequestDto;
import com.ungs.calzados.dto.ProductSupplierFavsRequestDto;
import com.ungs.calzados.dto.ProductSupplierFavsResponseDto;
import com.ungs.calzados.service.ProductSupplierFavsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")

public class ProductSupplierFavsController {

    private final ProductSupplierFavsService productSupplierFavsService;

    @Autowired
    public ProductSupplierFavsController (ProductSupplierFavsService productSupplierFavsService){
        this.productSupplierFavsService = productSupplierFavsService;
    }

    @PostMapping("/favs/supplier/product")
    public void addProduct(@Valid @RequestBody ProductSupplierFavsRequestDto productSupplierFavsRequestDto){
        log.info("Seteando un proveedor favorito a un producto");
        productSupplierFavsService.addFavSupplier(productSupplierFavsRequestDto);
    }

    @GetMapping("/favs/product/supplier/id")
    public List<ProductSupplierFavsResponseDto> getFavByIdSupplier(@Valid @RequestParam Integer id){
        log.info("Accediendo a la lista de favoritos por proveedor");
        return productSupplierFavsService.getProductSupplierFavById(id, FavsType.SUPPLIER);
    }

    @GetMapping("/favs/supplier/product/id")
    public List<ProductSupplierFavsResponseDto> getFavByIdProduct(@Valid @RequestParam Integer id){
        log.info("Accediendo a la lista de favoritos por supplier");
        return productSupplierFavsService.getProductSupplierFavById(id, FavsType.PRODUCT);
    }

    @DeleteMapping("/favs/supplier/product/inactivate")
    public void delFavProduct(@RequestParam Integer productId, @RequestParam Integer supplierId){
        log.info("Intentando quitar de la lista de favoritos el producto: " + productId +" y el proveedor: " +supplierId);
        productSupplierFavsService.delFavProduct(productId,supplierId);
    }

    @PostMapping("/favs/supplier/product/edit")
    public void replaceFavSupplier(@Valid @RequestBody ProductSupplierFavsRequestDto productSupplierFavsRequestDto) {
        log.info("Cambiando el proveedor favorito de un material");
        productSupplierFavsService.replaceFavSupplier(productSupplierFavsRequestDto);
    }







}
