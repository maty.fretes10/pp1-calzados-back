package com.ungs.calzados.controller;

import com.ungs.calzados.dto.ClientRequestDto;
import com.ungs.calzados.dto.ClientResponseDto;
import com.ungs.calzados.service.ClientService;
import com.ungs.calzados.service.ConstructionsClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class ClientController {
    private final ClientService clientService;
    private final ConstructionsClientService constructionsClientService;


    @Autowired
    public ClientController(ClientService clientService, ConstructionsClientService constructionsClientService) {
        this.clientService = clientService;
        this.constructionsClientService = constructionsClientService;
    }

    @GetMapping("/clients")
    public List<ClientResponseDto> getClients() {
        log.info("Accsessing clients  ");
        return clientService.getClients();
    }
    
    @GetMapping("/client")
    public ClientResponseDto getClientById(@RequestParam(required = false) Integer clientId) {
        log.info("Accsessing client by id  " + clientId);
        return clientService.getClientByIdResponse(clientId);
    }

    @PostMapping("/client")
    @ResponseStatus(HttpStatus.CREATED)
    public void addClient(@Valid @RequestBody ClientRequestDto clientRequest) {
        log.info("Accsessing add clients endpoint.");
        log.info("Client to add {}", clientRequest);
        constructionsClientService.constructionsSaveClient(clientRequest);
    }

    @DeleteMapping("/client/inactive")
    public void deleteClient(@RequestParam Integer clientId) {
        log.info("Accsessing client by id to inactivate  " + clientId);
        clientService.inactivateClient(clientId);
    }

    @PostMapping("/client/edit")
    public void editClient(
            @RequestParam Integer clientId,
            @Valid @RequestBody ClientRequestDto request){
        log.info("Editando el cliente con id " + clientId);
        constructionsClientService.constructionsEditClient(clientId, request);
    }

}
