package com.ungs.calzados.controller;

import com.ungs.calzados.dto.ProductModelRequestDto;
import com.ungs.calzados.dto.SupplierRequestDto;
import com.ungs.calzados.dto.product.*;
import com.ungs.calzados.service.ConstructionsProductService;
import com.ungs.calzados.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class ProductController {
    private final ProductService productService;
    private final ConstructionsProductService constructionsProductService;

    @Autowired
    public ProductController(ProductService productService, ConstructionsProductService constructionsProductService){
        this.productService = productService;
        this.constructionsProductService = constructionsProductService;
    }

    @GetMapping("/products")
    public List<ProductResponseDto> getProducts(){
        return productService.getProducts();
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public void addProduct(@Valid @RequestBody ProductRequestDto productRequest) {
        log.info("Accsessing add products endpoint.");
        log.info("Product to add {}", productRequest);
        constructionsProductService.constructionsSaveProduct(productRequest);
    }

    @PostMapping("/product/model")
    @ResponseStatus(HttpStatus.CREATED)
    public void addProductModel(@Valid @RequestBody ProductModelRequestDto productModelRequestDto) {
        log.info("Product model to add {}", productModelRequestDto);
        productService.addProductModel(productModelRequestDto);
    }

    @GetMapping("/products/colors")
    public List<ColourResponseDto> getColors(){
        log.info("Accsessing colors  ");
        return productService.getColors();
    }

    @GetMapping("/products/waist")
    public List<WaistResponseDto> getWaist(){
        log.info("Accsessing waist  ");
        return productService.getWaist();
    }

    @GetMapping("/products/model")
    public List<ProductModelResponseDto> getProductsModels(){
        log.info("Accsessing model  ");
        return productService.getProductsModels();
    }

    @GetMapping("/products/status")
    public List<ProductStatusResponseDto> getProductsStatus(){
        log.info("Accsessing status  ");
        return productService.getProductsStatus();
    }

    @DeleteMapping("/products/model/dis")
    public void disModelProduct(Integer modelId){
        log.info("Accessing product discontinuation");
        productService.disProductModel(modelId);
    }

    @PostMapping("/products/model/edit")
    public void editModel(@RequestParam Integer modelId, @Valid @RequestBody ProductModelRequestDto request){
        log.info("Editando el modelo con id " + modelId);
        productService.editProductModel(modelId, request);
    }

    @PostMapping("/color/add")
    public void addColor( @Valid @RequestBody ColourRequestDto request){
        log.info("Creando un nuevo color ");
        productService.addColour(request);
    }

    @DeleteMapping("/color/inactivate")
    public void inactivateColor(Integer colorId){
        log.info("Inactivando el color con id "  + colorId);
        productService.inactivateColour(colorId);
    }

    @PostMapping("/color/edit")
    public void editColor(@RequestParam Integer colorId, @Valid @RequestBody ColourRequestDto request){
        log.info("Editando el color con id " + colorId);
        productService.editColour(colorId, request);
    }

    @PostMapping("/waist/add")
    public void addWaist( @Valid @RequestBody WaistRequestDto request){
        log.info("Creando un nuevo talle ");
        productService.addWaist(request);
    }

    @DeleteMapping("/waist/inactivate")
    public void inactivateWaist(Integer waistId){
        log.info("Inactivando el talle con id "  + waistId);
        productService.inactivateWaist(waistId);
    }

    @PostMapping("/waist/edit")
    public void editWaist(@RequestParam Integer waistId, @Valid @RequestBody WaistRequestDto request){
        log.info("Editando el talle con id " + waistId);
        productService.editWaist(waistId, request);
    }

}
