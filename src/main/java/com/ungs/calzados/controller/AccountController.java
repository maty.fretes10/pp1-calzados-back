package com.ungs.calzados.controller;

import com.ungs.calzados.constants.AccountType;
import com.ungs.calzados.dto.AccountHistoryResponseDto;
import com.ungs.calzados.dto.CheckingAccountRequestDto;
import com.ungs.calzados.dto.CheckingAccountResponseDto;
import com.ungs.calzados.service.CheckingAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
@Slf4j
public class AccountController {
    private final CheckingAccountService checkingAccountService;


    @Autowired
    public AccountController(CheckingAccountService checkingAccountService) {
        this.checkingAccountService = checkingAccountService;
    }

    @GetMapping("/client/balance/{clientId}")
    public CheckingAccountResponseDto getBalanceClient(@PathVariable Integer clientId) {
        log.info("Accessing al balance  ");
        return checkingAccountService.getBalance(AccountType.CLIENT, clientId);
    }

    @GetMapping("/client/account/history/{clientId}")
    public List<AccountHistoryResponseDto> getAccountHistory(@PathVariable Integer clientId) {
        log.info("Accessing al Account History  ");
        return checkingAccountService.getAccountHistory(AccountType.CLIENT, clientId);
    }

    @PostMapping("/client/account/deposit/{clientId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addDeposit(@PathVariable Integer clientId, @Valid @RequestBody CheckingAccountRequestDto accountRequestDto) {
        checkingAccountService.getDeposit(AccountType.CLIENT, clientId, accountRequestDto);
    }

    @GetMapping("/supplier/balance/{supplierId}")
    public CheckingAccountResponseDto getBalanceSupplier(@PathVariable Integer supplierId) {
        log.info("Accessing al balance  ");
        return checkingAccountService.getBalance(AccountType.SUPPLIER, supplierId);
    }

    @GetMapping("/supplier/account/history/{supplierId}")
    public List<AccountHistoryResponseDto> getAccountHistorySupplier(@PathVariable Integer supplierId) {
        log.info("Accessing al Account History  ");
        return checkingAccountService.getAccountHistory(AccountType.SUPPLIER, supplierId);
    }

    @PostMapping("/supplier/account/deposit/{supplierId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addDepositSupplier(@PathVariable Integer supplierId, @Valid @RequestBody CheckingAccountRequestDto accountRequestDto) {
        checkingAccountService.getDeposit(AccountType.SUPPLIER, supplierId, accountRequestDto);
    }


}
