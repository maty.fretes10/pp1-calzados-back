package com.ungs.calzados.controller;

import com.ungs.calzados.constants.FavsType;
import com.ungs.calzados.dto.MaterialSupplierFavsRequestDto;
import com.ungs.calzados.dto.MaterialSupplierFavsResponseDto;
import com.ungs.calzados.service.MaterialSupplierFavsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")


public class MaterialSupplierFavsController {

    private final MaterialSupplierFavsService materialSupplierFavsService;

    @Autowired
    public MaterialSupplierFavsController (MaterialSupplierFavsService materialSupplierFavsService){
        this.materialSupplierFavsService = materialSupplierFavsService;
    }

    @PostMapping("/favs/supplier/material")
    public void addMaterial(@Valid @RequestBody MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto){
        log.info("Seteando un proveedor favorito a un material");
        materialSupplierFavsService.addFavSupplier(materialSupplierFavsRequestDto);
    }

    @GetMapping("/favs/material/supplier/id")
    public List<MaterialSupplierFavsResponseDto> getFavByIdSupplier(@Valid @RequestParam Integer id){
        log.info("Accediendo a la lista de favoritos");
        return materialSupplierFavsService.getMaterialSupplierFavById(id, FavsType.SUPPLIER);
    }

    @GetMapping("/favs/supplier/material/id")
    public List<MaterialSupplierFavsResponseDto> getFavByIdMaterial(@Valid @RequestParam Integer id){
        log.info("Accediendo a la lista de favoritos");
        return materialSupplierFavsService.getMaterialSupplierFavById(id, FavsType.MATERIAL);
    }

    @DeleteMapping("/favs/supplier/material/inactivate")
    public void delFavMaterial(@RequestParam Integer materialId, @RequestParam Integer supplierId){
        log.info("Intentando quitar de la lista de favoritos el material: " + materialId +" y el proveedor: " +supplierId);
        materialSupplierFavsService.delFavMaterial(materialId, supplierId);
    }

    @PostMapping("/favs/supplier/material/edit")
    public void replaceFavSupplier(@Valid @RequestBody MaterialSupplierFavsRequestDto materialSupplierFavsRequestDto) {
        log.info("Cambiando el proveedor favorito de un material");
        materialSupplierFavsService.replaceFavSupplier(materialSupplierFavsRequestDto);
    }

}
