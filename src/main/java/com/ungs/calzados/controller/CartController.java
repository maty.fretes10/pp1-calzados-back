package com.ungs.calzados.controller;

import com.ungs.calzados.dto.sale.CartRequestDto;
import com.ungs.calzados.dto.sale.CartSavedResponseDto;
import com.ungs.calzados.service.sale.ConstructionsCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
@Slf4j
public class CartController {

    private final ConstructionsCartService orchestratedCartTasksService;

    @Autowired
    public CartController(ConstructionsCartService orchestratedCartTasksService) {
        this.orchestratedCartTasksService = orchestratedCartTasksService;
    }

    @PostMapping("/cart")
    public CartSavedResponseDto addCart(@RequestBody @Valid CartRequestDto cartRequest) {
        log.info("Accsessing save cart endpoint.");
        return orchestratedCartTasksService.addCart(cartRequest);
    }
}
